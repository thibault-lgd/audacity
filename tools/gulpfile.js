// var gulp = require('gulp');
// // Requires the gulp-sass plugin
// var sass = require('gulp-sass');

//   gulp.task('sass', function(){
//     return gulp.src('../app/scss/**/*.scss') 
//     .pipe(sass())
//       .pipe(gulp.dest('../app/css'))
      
//   });

  
  
//   gulp.task('watch', function(){
//     gulp.watch('../app/scss/*.scss', ['sass']); 
//   })  

  var gulp = require('gulp'),
  argv = require('yargs').argv,
  sass = require('gulp-sass'),
  copy = require('gulp-contrib-copy'),
  autoprefixer = require('gulp-autoprefixer'),
  minifycss = require('gulp-minify-css'),
  concat = require('gulp-concat'),
  notify = require('gulp-notify'),
  gulpif = require('gulp-if'),
  rename = require('gulp-rename'),
  uglify = require('gulp-uglify'),
  clean = require('gulp-clean'),
  fs = require('fs'),
  runSequence = require('run-sequence'),
  htmlmin = require('gulp-htmlmin'),
  imagemin = require('gulp-imagemin'),
  pngquant = require('imagemin-pngquant');

// VERSION - get the version from the package json and append to static assets
var version = JSON.parse(fs.readFileSync('./package.json')).version;

// CONSTANTS
var config = {
  dir: {
      root: "../app/wp-content/themes/audacity/",
      src: '../app/wp-content/themes/audacity/',
      dist: '../app/wp-content/themes/audacity/'
  }
};

// Production add --production to "gulp build"
var production = argv.production;

// SCSS
gulp.task('styles', function() {
  return gulp.src(config.dir.src +  'style/style.scss')
      .pipe(sass({
          includePaths: [
              'node_modules/breakpoint-sass/stylesheets',
              'node_modules/susy/sass'
          ]
      }).on('error', sass.logError))
      .pipe(autoprefixer('last 3 version'))
      .pipe(gulpif(production, minifycss()))
      .pipe(gulpif(production, gulp.dest(config.dir.dist + 'style/')))
      .pipe(gulpif(production, rename('style.css')))
      .pipe(gulpif(production, gulp.dest(config.dir.dist + 'style/')))
      .pipe(gulpif(!production, gulp.dest(config.dir.dist + 'style/')))
      .pipe(gulpif(!production, notify({ message: 'SCSS Compiled' })));
});

    // return gulp.src('../app/scss/**/*.scss') 
    // .pipe(sass())
    //   .pipe(gulp.dest('../app/css'))

// BUILD - Production & Development (Gulp build --production || Gulp build --development)
gulp.task('build', function() {
  return runSequence(
      ['styles']
  );
});

// WATCHstyle
gulp.task('watch', function() {
  gulp.watch(config.dir.src + 'style/**/*.scss', ['styles']);
});