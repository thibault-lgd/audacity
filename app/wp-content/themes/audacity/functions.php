<?php

register_nav_menus( array(
	'menu-1' => esc_html__( 'Primary', 'legranddigital' ),
) );


/**
 * Enqueue scripts and styles.
 */
function legranddigital_scripts() {

    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/style/vendor/bootstrap.min.css' );

    wp_enqueue_style( 'custom', get_template_directory_uri() . '/style/style.css' );
    // wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/style/vendor/font-awesome.min.css' );
    wp_enqueue_style( 'fullpage', get_template_directory_uri() . '/style/vendor/jquery.fullpage.css' );
    wp_enqueue_style( 'slick-theme', get_template_directory_uri() . '/style/vendor/slick-theme.css' );
    wp_enqueue_style( 'slick-slider', get_template_directory_uri() . '/style/vendor/slick.css' );
		wp_enqueue_style( 'jquery-modal-css', get_template_directory_uri() . '/assets/jquery-modal/jquery.modal.min.css' );

    wp_enqueue_style( 'legranddigital-style', get_stylesheet_uri() );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'legranddigital_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if(!function_exists('load_my_script')){
    function load_my_script() {
        global $post;
        $deps = array('jquery');
        $version= '1.0';
        $in_footer = true;
        wp_enqueue_script('jquery', get_stylesheet_directory_uri() . '/js/jquery.min.js', $deps, $version, $in_footer);
				wp_enqueue_script('jscookie', get_stylesheet_directory_uri() . '/js/js.cookie.js', $deps, $version, $in_footer);
        wp_enqueue_script('my-script', get_stylesheet_directory_uri() . '/js/custom.js', $deps, $version, $in_footer);
        wp_enqueue_script('utils', get_stylesheet_directory_uri() . '/js/vendor/utils.js', $deps, $version, $in_footer);
        wp_enqueue_script('tether', get_stylesheet_directory_uri() . '/js/vendor/tether.min.js', $deps, $version, $in_footer);
        wp_enqueue_script('bootstrap', get_stylesheet_directory_uri() . '/js/vendor/bootstrap.min.js', $deps, $version, $in_footer);
        // wp_enqueue_script('scrolloverflow', get_stylesheet_directory_uri() . '/js/vendor/scrolloverflow.min.js', $deps, $version, $in_footer);
        wp_enqueue_script('parallax', get_stylesheet_directory_uri() . '/js/vendor/jquery.parallax-1.1.3.js', $deps, $version, $in_footer);
        wp_enqueue_script('slick-slider', get_stylesheet_directory_uri() . '/js/vendor/slick.min.js', $deps, $version, $in_footer);
        wp_enqueue_script('equal-height', get_stylesheet_directory_uri() . '/js/vendor/jquery.matchHeight-min.js', $deps, $version, $in_footer);
        wp_enqueue_script('scrollify', get_stylesheet_directory_uri() . '/js/vendor/jquery.scrollify.js', $deps, $version, $in_footer);
        wp_enqueue_script( 'helliumparallax', get_stylesheet_directory_uri() . '/js/vendor/helium.parallax.js', array(), '3.0.0', true );
        wp_enqueue_script( 'scrollreveal-script', get_stylesheet_directory_uri() . '/js/vendor/scrollreveal.min.js', array(), '3.0.0', true );
        wp_enqueue_script('jquery-modal', get_stylesheet_directory_uri() . '/assets/jquery-modal/jquery.modal.min.js', $deps, $version, $in_footer);
				wp_enqueue_script('jquery-paroller', get_stylesheet_directory_uri() . '/assets/paroller/dist/jquery.paroller.min.js', $deps, $version, $in_footer);
        wp_localize_script( 'my-script', 'menuToggleText', array(
            'open'   => esc_html__( 'Open child menu' ),
            'close'  => esc_html__( 'Close child menu' ),
            // 'icon'   => cover2_get_svg( array( 'icon' => 'icon_bg_angle-down', 'fallback' => true ) ),
        )

        // wp_localize_script('my-script', 'my_script_vars', array(
        //         'postID' => $post->ID
        //     )
        );
    }
}
add_action('wp_enqueue_scripts', 'load_my_script');

function wpb_list_child_pages() {

   global $post;

   if ( is_page() && $post->post_parent )

       $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
   else
       $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );

   if ( $childpages ) {

       $string = '<ul>' . $childpages . '</ul>';
   }

   return $string;

   }

   add_shortcode('wpb_childpages', 'wpb_list_child_pages');
   add_theme_support( 'post-thumbnails' );

   add_action('init', 'wpshout_register_taxonomy');
   function wpshout_register_taxonomy() {
       $args = array(
           'hierarchical' => true,
           'label' => 'Feature post',
       );
       register_taxonomy( 'featured', array( 'post' ), $args );
       session_start();//MD:POSTGROUP
   }


   function tax_cat_active( $output, $args ) {

      if(is_single()){
        global $post;

        $terms = get_the_terms( $post->ID, $args['taxonomy'] );
        foreach( $terms as $term )
            if ( preg_match( '#cat-item-' . $term ->term_id . '#', $output ) )
                $output = str_replace('cat-item-'.$term ->term_id, 'cat-item-'.$term ->term_id . ' current-cat', $output);
      }

      return $output;
    }
    add_filter( 'wp_list_categories', 'tax_cat_active', 10, 2 );
    function wp_list_categories_remove_title_attributes($output) {
        $output = preg_replace('` title="(.+)"`', '', $output);
        return $output;
    }

    add_filter( 'gform_field_value_white_paper', 'gf_filter_white_paper' );
    function gf_filter_white_paper() {
        return esc_attr( get_field( 'white_paper' ) );
    }

    // add_filter( 'gform_field_value_your_parameter', 'my_custom_population_function' );
    // function my_custom_population_function( $value ) {
    //     global $post;
    //     return function_exists( 'get_field' ) ? get_field( 'white_paper', $post->ID ) : false;
    // }

    if( function_exists('acf_add_options_page') ) {

        acf_add_options_page();

    }

    function customBodyClass( $classes ) {
  global $current_blog;
  $classes[] = 'website-'.$current_blog->blog_id;
  return $classes;
}
add_filter( 'body_class', 'customBodyClass' );
