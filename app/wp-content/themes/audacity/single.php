<?php /* Template Name: Blog */

get_header();

?>


	<section class="title">


		<div class="row blue">


			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			<h1>
                <?php the_title(); ?>
			</h1>


			<?php endwhile; else : ?>

			<p>
				<?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?>
			</p>

			<?php endif; ?>

		</div>

	</section>


	<section class="heading container-fluid">

		<div class="row">
			<table>
				<tr>
					<td>
						<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner">
						<div class="carousel-item active">
						<?php if($postslist){
							$decor_picture_url = wp_get_attachment_url($replaceMeta["decor_picture"][0]);
							?>
							<img src="<?php echo $decor_picture_url ?>">
						<?php }else{ ?>
							<img src="<?php the_field( "decor_picture" );  ?>">
						<?php } ?>

</div>

							<div class="carousel-item ">
						<?php
   	if ( has_post_thumbnail() ) {
   		the_post_thumbnail('full');
   	}
	?>
	</div>


</div>
<a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
					</td>
					<td id="parallax-id-1">
						<div class="inner-center">

							<h2 class="black"> Laminate reinvented to amaze</h2>
							      <p> <?php the_field('cta_description','option'); ?>

    </p>

                <a class="cat_desc_cta" target="_blank" href="<?php the_field('cta_link','option'); ?>">   <?php the_field('cta_text','option'); ?>
</a>


    <!--                         <?php if($postslist){
                                
                                    echo $postslist[0]->post_content;
                                }
                            ?>
                            <?php
                                    $allowedCategoryDescription = apply_filters( 'get_allowed_category_description',array("description_type"=>0,"category_id"=>$currentCatID) );
                                    $isHidden = false;
                                    $catDes = "";
                                    if(count($allowedCategoryDescription)>0){
                                        if($allowedCategoryDescription[0]->is_hidden == 1){
                                            $isHidden = true;
                                        }
                                        $catDes = str_replace( ']]>', ']]&gt;', $allowedCategoryDescription[0]->category_description );
                                    }else{
                                        $catDes = category_description( $category );
                                    }
                                    if(!$isHidden & !empty($catDes)){
                                    ?> -->
                                    <p>

 
<section id="cat_desc">
                                        <div class="row">
                                           
                                            <div class="col-md-12">
                                             <!--    <p>
                                                    <?php echo $catDes;?>
                                                </p>
                                                <?php if(count($allowedCategoryDescription)>0){
                                                        if(!empty($allowedCategoryDescription[0]->category_description_link)){
                                                            $link = $allowedCategoryDescription[0]->category_description_link;
                                                            if(!empty($allowedCategoryDescription[0]->category_description_link_title)){
                                                                echo '<a class="cat_desc_cta" target="_blank" href="'.$link.'">'.$allowedCategoryDescription[0]->category_description_link_title.'</a>';
                                                            }else{
                                                                echo '<a class="cat_desc_cta" target="_blank" href="'.$link.'">shop audacity</a>';
                                                            }
                                                        }
                                                    }
                                                ?> -->


                                            </div>
                                           
                                        </div>
                                    </section>
                                <?php 
                                    }
                                ?>
						<!-- 	<p>
								<br/> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
								<br/> Proin consectetur velit in neque placerat sollicitudin.
								<br/> Pellentesque sed malesuada massa, ut bibendum ante.
							</p> -->
						</div>
					</td>
				</tr>
			</table>
		</div>

	</section>

	<section class="product-details container">

		<table class="details-product desktop">
			<tr>
				<td>
					Decor Name
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["decor_name"][0];
                    }else{
                        the_field( "decor_name" );
                    }?>
				</td>
				<td>
					Edge Sealer
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["edge_sealer"][0];
                    }else{
                        the_field( "edge_sealer" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Collection
				</td>
				<td>
				<?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
				</td>
				<td>
					Ultra Stable Core
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["ultra-stable_core"][0];
                    }else{
                        the_field( "ultra-stable_core" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Product Number
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["product_number"][0];
                    }else{
                        the_field( "product_number" );
                    }?>
				</td>
				<td>
					Installation
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["installation"][0];
                    }else{
                        the_field( "installation" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Size </td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["sizes__w_x_l"][0];
                    }else{
                        the_field( "sizes__w_x_l" );
                    }?>
				</td>
				<td>
					Click System
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["click_system"][0];
                    }else{
                        the_field( "click_system" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Overall Thickness
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["overall_thickness_"][0];
                    }else{
                        the_field( "overall_thickness_" );
                    }?>
				</td>
				<td>
					Surface Texture
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["surface_texture"][0];
                    }else{
                        the_field( "surface_texture" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Backing
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["backing"][0];
                    }else{
                        the_field( "backing" );
                    }?>
				</td>
				<td>
					Water Resistant
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["water_resistent_"][0];
                    }else{
                        the_field( "water_resistent_" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Wear Resistence
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["wear_resistence"][0];
                    }else{
                        the_field( "wear_resistence" );
                    }?>
				</td>
				<td>
					Box Content
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["box_content"][0];
                    }else{
                        the_field( "box_content" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Carb2 Certified
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["carb2_certified"][0];
                    }else{
                        the_field( "carb2_certified" );
                    }?>
				</td>
				<td>
					Box Weight
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["box_weight"][0];
                    }else{
                        the_field( "box_weight" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Floorscore Certified
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["floorscore_certificate"][0];
                    }else{
                        the_field( "floorscore_certificate" );
                    }?>
				</td>
				<td>
					Residential Warranty
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["residential_warranty"][0];
                    }else{
                        the_field( "residential_warranty" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Edge Style
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["edge_style"][0];
                    }else{
                        the_field( "edge_style" );
                    }?>
				</td>
				<td>
					Commercial Warranty
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["commercial_warranty"][0];
                    }else{
                        the_field( "commercial_warranty" );
                    }?>
				</td>
			</tr>
		</table>

		<table class="details-product mobile">
			<tr>
				<td>
					Decor Name
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["decor_name"][0];
                    }else{
                        the_field( "decor_name" );
                    }?>
				</td>
			</tr>
			<tr>
			<td>
					Collection
				</td>
				<td>
				<?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
				</td>
				
			</tr>
			<tr>
			<td>
					Product Number
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["decor_name"][0];
                    }else{
                        the_field( "decor_name" );
                    }?>
				</td>
				
			</tr>
			<tr>
			<td>
					Size </td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["sizes__w_x_l"][0];
                    }else{
                        the_field( "sizes__w_x_l" );
                    }?>
				</td>
				
			</tr>
			<tr>
			<td>
					Overall Thickness
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["overall_thickness_"][0];
                    }else{
                        the_field( "overall_thickness_" );
                    }?>
				</td>
				
			</tr>
			<tr>
			<td>
					Backing
				</td>
				<td>
                <?php if($postslist){
                        echo $replaceMeta["backing"][0];
                    }else{
                        the_field( "backing" );
                    }?>
				</td>
				
			</tr>
			<tr>
			<td>
					Water Resistant
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["water_resistent_"][0];
                    }else{
                        the_field( "water_resistent_" );
                    }?>
				</td>
</tr>
			<tr>
			<td>
					Wear Resistence
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["wear_resistence"][0];
                    }else{
                        the_field( "wear_resistence" );
                    }?>
				</td>
			
			</tr>
			<tr>
			<td>
					Carb2 Certified
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["carb2_certified"][0];
                    }else{
                        the_field( "carb2_certified" );
                    }?>
				</td>
				
			</tr>
			<tr>
			<td>
					Floorscore Certified
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["floorscore_certificate"][0];
                    }else{
                        the_field( "floorscore_certificate" );
                    }?>
				</td>
			
			</tr>
			<tr>
			<td>
					Edge Style
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["edge_style"][0];
                    }else{
                        the_field( "edge_style" );
                    }?>
				</td>
			
			</tr>

			<tr>
			<td>
					Edge Sealer
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["edge_sealer"][0];
                    }else{
                        the_field( "edge_sealer" );
                    }?>
				</td>
</tr>
<tr>
<td>
					Ultra Stable Core
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["edge_sealer"][0];
                    }else{
                        the_field( "edge_sealer" );
                    }?>
				</td>

</tr>
<tr>
<td>
					Installation
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["installation"][0];
                    }else{
                        the_field( "installation" );
                    }?>
				</td>

</tr>
<tr>
<td>
					Click System
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["click_system"][0];
                    }else{
                        the_field( "click_system" );
                    }?>
				</td>
</tr>
<tr>
<td>
					Surface Texture
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["surface_texture"][0];
                    }else{
                        the_field( "surface_texture" );
                    }?>
				</td>


</tr>
<tr>
<td>
					Box Content
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["box_content"][0];
                    }else{
                        the_field( "box_content" );
                    }?>
				</td>

</tr>

<tr>

<td>
					Box Weight
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["box_weight"][0];
                    }else{
                        the_field( "box_weight" );
                    }?>
				</td>

</tr>
<tr>
<td>
					Residential Warranty
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["residential_warranty"][0];
                    }else{
                        the_field( "residential_warranty" );
                    }?>
				</td>
</tr>
<tr>
<td>
					Commercial Warranty
				</td>
				<td>
                    <?php if($postslist){
                        echo $replaceMeta["commercial_warranty"][0];
                    }else{
                        the_field( "commercial_warranty" );
                    }?>
				</td>
</tr>
		</table>

	</section>

	<section class="tab-info">

		<div class="container-fluid">

			<main>

				<input id="tab1" type="radio" name="tabs" checked>
				<label for="tab1">From the same collection</label>

				<!-- <input id="tab2" type="radio" name="tabs">
				<label for="tab2">Similar tones</label> -->



				<section id="content1">
					<div class="row">
						<?php 
	  $my_query = new WP_Query(array('posts_per_page=-1','cat' => $currentCatID ));
	  $count=0;
	  while ($my_query->have_posts()) : $my_query->the_post();
		  $do_not_duplicate = $post->ID;
		  if($currentId == $post->ID){
			  $currentIndex = $count;
		  }
  ?>
						<div class="col-lg-3 col-md-4">

							<div class="item">
								<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" style="text-decoration:none">

									<img src="<?php the_post_thumbnail_url(); ?>" />
									<h2>
										<?php echo the_title(); ?>
									</h2>

								</a>
							</div>
						</div>
						<?php $count++;
		endwhile; wp_reset_query(); ?>
					</div>
				</section>

				<section id="content2">
				<div class="row">
				<?php 
$my_query = new WP_Query(array('posts_per_page=-1','cat' => $currentCatID ));
$count=0;
while ($my_query->have_posts()) : $my_query->the_post();
  $do_not_duplicate = $post->ID;
  if($currentId == $post->ID){
	  $currentIndex = $count;
  }
?>
						<div class="col-lg-3 col-md-4">

					<div class="item">
						<a href="<?php echo esc_url( get_permalink( $post->ID ) ); ?>" style="text-decoration:none">

							<img src="<?php the_post_thumbnail_url(); ?>" />
							<h2>
								<?php echo the_title(); ?>
							</h2>

						</a>
					</div>
				</div>
				<?php $count++;
endwhile; wp_reset_query(); ?>
			</div>
				</section>

			</main>
		</div>
	</section>
<!--     <?php echo do_shortcode('[get_link_section]') ?>
 --><div id="new_contact" tyle="padding:60px 0px" class="row">
<style>
form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}

body.single-post .gform_heading{
	display: none !important
}

#cat_desc{
	margin-top:25px;
	display:block;
	position: relative;
}

#cat_desc .col-md-6.col-sm-12{
	padding:25px;
	border:1px solid #33257c;
}

a.cat_desc_cta{
	background: #43bbbe;
	color: white;
	padding:10px 20px;
	    margin: 0 auto;
    position: relative;
    display: block;
    text-align: center;
    width: fit-content;
}

#parallax-id-1 #cat_desc .col-md-12{
	border:none;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:60px 0px 60px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER<br/>
	 <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.!</h2> -->
   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>

            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>


    <?php get_footer('collections'); ?>
