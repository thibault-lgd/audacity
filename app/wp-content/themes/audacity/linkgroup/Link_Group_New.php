<?php

/*
Plugin Name: WP_List_Table Class Example
Plugin URI: http://sitepoint.com
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Agbonghama Collins
Author URI:  http://w3guy.com
*/
class LinkSectionGroupNew_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'New Link Section',
			'New Link Section',
			'manage_options',
			'link_section_group_form',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		$this->process_post_submit();
        $link_section_detail = $this->get_linksectiondetail();
        wp_enqueue_media();
		?>
		 <style>
		 	.label-info {
			    background-color: #5bc0de;
			}
		 </style>
		<div class="wrap">
			<?php if(isset($_GET["action"])){
				if($_GET["action"]=="edit"){
					echo '<h2>Edit Link Section</h2>';
				}else{
					echo '<h2>Edit Link Section</h2>';
				}
			}else{
				echo '<h2>New Link Section</h2>';
			}
			?>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-1">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<input type="hidden" name="link_section_id" value="<?php echo $link_section_detail["ID"];?>" />
								<input type="hidden" name="issubmit" value="1" />
								<div class="form-field term-description-wrap">
									<label for="link_section_title">Link Section Title</label><br />
                                    <input name="link_section_title" id="link_section_title" type="text" value="<?php echo $link_section_detail["link_section_title"];?>" size="40" aria-required="true">
								</div>
                                <div class="form-field term-description-wrap">
									<label for="link_section_button_title">Link Section Button Title</label><br />
                                    <input name="link_section_button_title" id="link_section_button_title" type="text" value="<?php echo $link_section_detail["link_section_button_title"];?>" size="40" aria-required="true">
								</div>
                                <?php $postcode_group = apply_filters( 'taxonomy_get_postcode_group','taxonomy_get_postcode_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Postcode Group</label>  <br />                                  			
                                    <select name="taxonomy_postcode_group[]" multiple style="width:100%">
                                        <?php 
                                        $postcode = unserialize($link_section_detail["postcode_group"]);
                                        if(count($postcode_group)>0){
                                            foreach ( $postcode_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $postcode as $selecteditem ) {
                                                    if($selecteditem==$item->ID){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item->ID.'" '.$isSelected.'>'.$item->postcode_group_name.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>                                                                            
                                </div>    
	                            <?php $country_group =  apply_filters( 'taxonomy_get_country_group','taxonomy_get_country_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Country</label>
                                    <select name="taxonomy_country_group[]" multiple style="width:100%">
                                    <?php
                                        $country = unserialize($link_section_detail["country_group"]);
                                        if(count($country_group)>0){
                                            foreach ( $country_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $country as $selecteditem ) {
                                                    if($selecteditem==$item["code"]){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item["code"].'" '.$isSelected.'>'.$item["name"].'</option>';						
                                            }
                                        }
                                    ?>
                                    </select>                                      
                                </div>
								<p class="submit">
									<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Link Section">
								</p>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>		
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Link Section List',
			'default' => 5,
			'option'  => 'ads_per_page'
		];

		add_screen_option( $option, $args );

		
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	public function get_linksectiondetail() {
		$link_section_detail = array();
		$link_section_detail["ID"]=0;
		$link_section_detail["link_section_title"]="";
        $link_section_detail["link_section_button_title"]="";
        $link_section_detail["postcode_group"]="";
        $link_section_detail["country_group"]="";

		if(isset($_GET["action"])){
			if($_GET["action"]=="edit"){
				global $wpdb;
				$query = "select * from wp_link_section_group where ID =".$_GET["ID"];
				$result = $wpdb->get_results($query);
				$lcount = 0;
				if(count($result)>0){
					foreach ( $result as $item ) {
						$link_section_detail["ID"] = $item->ID;
                        $link_section_detail["link_section_title"] = $item->link_section_title;
                        $link_section_detail["link_section_button_title"] = $item->link_section_button_title;
                        $link_section_detail["postcode_group"] = $item->postcode_group;
                        $link_section_detail["country_group"] = $item->country_group;
						break;
					}
				}
				
			}
		}
		return $link_section_detail;
	}
	public function process_post_submit() {
		if(isset($_POST["issubmit"])){
			$link_section_title = $_POST["link_section_title"];
            $link_section_button_title = $_POST["link_section_button_title"];
            $postcode_group = serialize($_POST["taxonomy_postcode_group"]);
            $country_group = serialize($_POST["taxonomy_country_group"]);
            $link_section_id = $_POST["link_section_id"];
            $description_type = $_POST["description_type"];
			global $wpdb;
			$query = "";
			if($link_section_id == 0){
				$query = "INSERT INTO wp_link_section_group(link_section_title, link_section_button_title, postcode_group, country_group, is_active) VALUES('".$link_section_title."','".$link_section_button_title."','".$postcode_group."','".$country_group."',1)";				
			}else{
				$query = "update wp_link_section_group set link_section_title='".$link_section_title."', link_section_button_title='".$link_section_button_title."', postcode_group='".$postcode_group."', country_group='".$country_group."' where ID =".$link_section_id;
			}
            $wpdb->get_results($query);
			echo '<script type="text/javascript">';
			echo 'window.location = "?page=link_section_group_listing";';
			echo '</script>';
			exit;
		}
	}

}
