<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legranddigital
 */

?>
	<!DOCTYPE html>
	<html <?php language_attributes(); ?>>

	<head>

<!-- Global site tag (gtag.js) - Google Ads: 763242815 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-763242815"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-763242815');
</script>

<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"25042674"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script>
		<meta http-equiv="x-ua-compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<!-- <link href="/style/vendor/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
		    crossorigin="anonymous"> -->
		<!-- <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"> -->
		<!-- <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
		<!-- <script src="wp-content/themes/audacity/js/vendor/scrolloverflow.min.js"></script> -->

		<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
		<?php wp_head(); ?>



<script type="text/javascript">



//     var isCalled = false;
//     jQuery(document).ready(function() {
//         findLocation();
//     });
//     function findLocation(){
// 	 if (navigator && navigator.geolocation) {
//             navigator.geolocation.getCurrentPosition(successCallback, errorCallback);
// 	 }else {
// 		getLocation(true);
// 	}
// }
// function errorCallback(e) {
// 	getLocation(true);
// }

// function successCallback(position) {
// 	var isChange=false;
// 	var UserLatitude = localStorage.getItem('UserLatitude');
// 	var UserLongitude = localStorage.getItem('UserLongitude');
// 	var UserPostcode = localStorage.getItem('UserPostcode');
// 	localStorage.setItem('UserLatitude', position.coords.latitude.toString());
// 	localStorage.setItem('UserLongitude', position.coords.longitude.toString());
// 	if(UserLatitude!=position.coords.latitude.toString() && UserLongitude!=position.coords.longitude.toString()){
// 		UserLatitude = position.coords.latitude;
// 		UserLongitude = position.coords.longitude;
// 		isChange=true;
// 	}

// 	getLocation(isChange);
// }
// function getLocation(isChange){
// 	if(!isCalled){

// 		if(localStorage.getItem('UserPostcode')=="null"){
// 			localStorage.setItem('UserPostcode', "");
// 		}
// 		if(localStorage.getItem('UserCountry')=="null" || localStorage.getItem('UserCountry')==null){
// 			localStorage.setItem('UserCountry', "");
// 		}
// 		var postCode = localStorage.getItem('UserPostcode');
// 		var country = localStorage.getItem('UserCountry');
// 		if(isChange){
// 			postCode = "";
// 		}
// 		if(isChange){
// 			country = "";
// 		}
// 		jQuery.ajax({
// 			type:'POST',
// 			url:'<?php echo get_template_directory_uri();?>/getLocation.php',
// 			data:'latitude='+localStorage.getItem('UserLatitude')+'&longitude='+localStorage.getItem('UserLongitude')+'&postcode='+postCode +'&country='+country,
// 			success:function(msg){
// 				var response = JSON.parse(msg);
// 				var pc = response.UserPostcode;
// 				var con = response.UserCountry;

// 				var oldPostCode = localStorage.getItem('UserPostcode');
// 				localStorage.setItem('UserPostcode', pc);
// 				if(pc=="null"){
// 					pc="";
// 				}
// 				localStorage.setItem('UserPostcode', pc);

// 				var oldCountry = localStorage.getItem('UserCountry');
// 				localStorage.setItem('UserCountry', con);
// 				if(con=="null"){
// 					con="";
// 				}
// 				localStorage.setItem('UserCountry', con);
// 				var IsCheckedOnce = localStorage.getItem('IsCheckedOnce');
//                 localStorage.setItem('IsCheckedOnce', "1");
// 				if(!oldPostCode  && msg !=""){
// 					if(!IsCheckedOnce){
//                         window.location.reload();
//                     }
// 				}else if(!oldCountry  && msg !=""){
//                     if(!IsCheckedOnce){
// 					    window.location.reload();
//                     }
// 				}
// 			}
// 		});
// 	}
// 	isCalled=true;
// }
</script>
	</head>
	<style>


.gf_browser_chrome.gform_wrapper{
    display: block !important;

}

#new_form .gform_wrapper{
    display: block !important;
}

  #new_form h2,
  .section.support span.contact h2{
    color: black !important;
    margin-bottom: 0px;
  }

  #new_form p a,
  .section.support p a {
   background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto;
    font-size: 32px ;
  }
	.multi_dropdown {
		float:left;
		margin-right:20px;
		top:5px;
		position:relative;
	}

		.dms-container{float:left;

	}

	.outer_dropdown{
		float:right;
	}
	#country_selector_modal.fade{
		opacity:1;
	}

	.jquery-modal.blocker.current{
		z-index: 999 !important
	}

	.dms-container select, .dms-container select option{
		border-radius: 0px !important;
		background: #43bbbe;
		color: white !important;
/*		border-width:0px !important;
*/		border: 1px solid white;
		border-color:none !important;
		border-radius: 0px !important;
		border-top-left-radius:0px !important;
		border-bottom-left-radius:0px !important;
		border-top-right-radius:0px !important;
		border-bottom-left-radius:0px !important;
	}



	.wordpress-country-selector-modal-header {
	    border: none;
	}

	.country_selector_modal .wordpress-country-selector-modal-content {
	    box-shadow: none;
	    border: none;
	    text-align: center;
	}

	.wordpress-country-selector-modal.fade .wordpress-country-selector-modal-dialog {
	    -webkit-transform: none;
	    -ms-transform: none;
	    -o-transform: none;
	    transform: none;
	}

	.country_selector_modal .wordpress-country-selector-modal-footer {
	    /* border: none; */
	    display: none;
	}

	.country_selector_modal_close {
	    /* border: none; */
	    display: none;
	}

	#country_selector_modal.fade {
	    border-radius: 0;
	    border: 15px solid #49acb1;
	    max-width:60%;
	}

	.country_selector_modal .wordpress-country-selector-modal-dialog {
	    text-align: center;
	}

	body.page-template-page-collections #country_selector_modal p {
	    text-align: center;
	    /* margin-bottom: 0; */
	}

	.country_selector_modal_text {
	    font-size: 30px;
	    font-weight: normal;
	    margin-bottom: 2.5rem;
	}


	/** Custom CSS 09/14/2018 **/

	.wordpress-country-selector-modal-header {
    border: none;
}

.country_selector_modal .wordpress-country-selector-modal-content {
    box-shadow: none;
    border: none;
    text-align: center;
}

.wordpress-country-selector-modal.fade .wordpress-country-selector-modal-dialog {
    -webkit-transform: none;
    -ms-transform: none;
    -o-transform: none;
    transform: none;
}

.country_selector_modal .wordpress-country-selector-modal-footer {
    /* border: none; */
    display: none;
}

.country_selector_modal_close {
    /* border: none; */
    display: none;
}

#country_selector_modal.fade {
    border-radius: 0;
    border: 15px solid #49acb1;
        background: url(https://thibaultrolando.com/audacity/us/wp-content/themes/audacity/assets/images/bg-art.png) no-repeat left bottom / contain;
    background-size: auto;
    background-color: white;
}

.country_selector_modal .wordpress-country-selector-modal-dialog {
    text-align: center;
}

body.page-template-page-collections #country_selector_modal p {
    text-align: center;
    /* margin-bottom: 0; */
}

.country_selector_modal_text {
    font-size: 30px;
    font-weight: normal;
    margin-bottom: 2.5rem;
}

.country_selector_modal_buttons > p {
	display: none;
}


select#country-dropdown {
     -webkit-appearance: none;
     -moz-appearance: none;
     appearance: none;
     padding: 5px 35px 5px 10px;
     width: 100%;
     border: 1px solid #57AEB0;
		 border-radius: 0px !important;
     color: #57AEB0;
     font-size: 24px;
     height: auto;

}

#country-dropdown-wrap {
    position: relative;
    display: inline-block;
    max-width: 500px;
     width: 100%;
}

.country_selector_seems_text {
	display: none;
}

#country-dropdown-wrap:after {
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: 20px 10px 0 10px;
    border-color: #57AEB0 transparent transparent transparent;
    display: block;
    position: absolute;
    right: 15px;
    top: 15px;
}

.stay {
    font-size: 20px;
    margin-top: 1rem;
    /* color: #57AEB0; */
    border-left: 10px solid;
    display: inline-block;
    border-right: 10px solid;
    padding-left: 10px;
    padding-right: 10px;
    margin-bottom: 4rem;
}

.stay > a {
    color: #57AEB0;
}

.reason {
margin: 0 auto;
		color: rgb(46,38,115);
    font-size: 18px;
}

.country_selector_modal .wordpress-country-selector-modal-dialog {
    width: inherit;
    margin-bottom: 10px;
}

.stay {
    font-size: 20px;
    margin-top: 1rem;
    /* color: #57AEB0; */
    border-left: 10px solid;
    display: inline-block;
    border-right: 10px solid;
    padding-left: 10px;
    padding-right: 10px;
    margin-bottom: 4rem;
}

.stay > a {
    color: #57AEB0;
}

.reason {
margin: 0 auto;
    margin-bottom: 0px;
    color: #2e2674;
}

.country_selector_modal .wordpress-country-selector-modal-dialog {
    width: inherit;
    margin-bottom: 10px;
}

.wordpress-country-selector-modal-content {
    position: relative;
}

.wordpress-country-selector-modal-content:after {
    content: '';
    display: block;
    width: 100%;
    max-width: 300px;
    height: 200px;
    position: absolute;
    bottom: -20px;
    left: -10px;
/*    background: url('<?php echo get_stylesheet_directory_uri(); ?>/assets/images/bg-art.png') no-repeat left bottom / contain;
*/    z-index: -1
}

.wordpress-country-selector-modal-content {
    background: transparent !important;
}

.modal a.close-modal {
    background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/assets/images/close-icon.png);
    width: 60px;
    height: 60px;
    right: -32px;
    top: -32px;
}

@media only screen and (min-device-width : 375px) and (max-device-width : 667px) {

.mobiletohide{
    display: none;
}

.multi_dropdown {
    float: none;
    margin-right: 20px;
    top: 5px;
    position: relative;
    padding: 10px 0px;
    display: block;
    text-align: center;
}

#country_selector_modal.fade{
    max-width: 100%;
}

.dms-container{
    padding-bottom: 20px;
}

}

.right-side {
    position: fixed;
    width: 100%;
    height: 100%;
    background: #fff;
    height: 1;
    z-index: 1000;
    display: flex;
    align-items: center;
    justify-content: center;
}

.redirecting {
	display: flex;
}

	</style>

<?php //echo do_shortcode('[dms]'); ?>

	<body <?php body_class(); ?>>


<div class="right-side"></div>

		<div id="page" class="site">

			<div class="container-fluid" style="background:#43bbbe; padding:5px 0px">
				<div class="row">
					<div class="container">
					<div class="col-md-12 right">
							<div class="outer_dropdown topbar-dropdown topbar-choice">
						<span class="multi_dropdown" style="color: white">Select your country</span>
						<?php echo do_shortcode('[dms]'); ?>
					</div>
			</div>
			</div>
</div>
			</div>

			<header id="masthead" class="site-header" role="banner">
				<div class="container">
					<div class="row">
						<div class="col-lg-5 col-md-5 col-sm-6">
							<div class="logo">
								<a href="<?php echo get_home_url(); ?>">
									<img title="<?php bloginfo('name'); ?>" width="200px" class="desktop" src="<?php bloginfo('template_directory'); ?>/assets/logo.png" alt=" logo"
									/>
								</a>
								<!-- <a href="<?php //echo get_home_url(); ?>">
									<img width="200px" class="desktop" src="<?php//bloginfo('template_directory'); ?>/assets/img/armstrong-logo.jpg" alt=" logo"
									/>
								</a> -->
							</div>
						</div>


						<div class="col-lg-4 col-md-3 col-sm-1 empty">


						</div>
						<div class="col-lg-3 col-md-4 col-sm-5 col-xsm-3 social">
						<div class="social nothome">
                <div class="row">
                    <a
                        target="_blank"
                        href="https://www.facebook.com/Audacity-Flooring-2018669681713086/">
                        <i class="fa  fa-facebook-square"></i>
                    </a>

                    <a target="_blank" href="https://www.linkedin.com/company/audacity-flooring/">
                        <i class="fa fa-linkedin-square"></i>

                        <a
                            target="_blank"
                            href="https://www.youtube.com/channel/UC-7SIn8xQflUAkrdZTzbtOQ">
                            <i class="fa fa-youtube-square"></i>
                        </a>

                        <i class="fa fa-bars"></i>

                    </div>
                    <div class="row">

                        <a target="_blank" href="https://www.pinterest.co.uk/audacityflooring/pins/">
                            <i class="fa  fa-pinterest-square"></i>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/audacityflooring/">
                            <i class="fa fa-instagram"></i>
                        </a>

                        <a
                            target="_blank"
                            href="https://www.houzz.co.uk/pro/sherry-devera/audacity-flooring?irs=US">
                            <i class="fa  fa-houzz"></i>
                        </a>
                    </div>
                </div>

						</div>
					</div>

</div>
<nav id="site-navigation" class="primary-navigation">
<i style="color: white" class="fa fa-times"></i>

		 <div class="container-menu-social">
		 <a
                    target="_blank"
                    href="https://www.facebook.com/Audacity-Flooring-2018669681713086/">
                    <i class="fa  fa-facebook-square"></i>
                </a>

                <a target="_blank" href="https://www.linkedin.com/company/audacity-flooring/">
                    <i class="fa fa-linkedin-square"></i>

                    <a
                        target="_blank"
                        href="https://www.youtube.com/channel/UC-7SIn8xQflUAkrdZTzbtOQ">
                        <i class="fa fa-youtube-square"></i>
                    </a>
                    <a target="_blank" href="https://www.pinterest.co.uk/audacityflooring/pins/">
                        <i class="fa  fa-pinterest-square"></i>
                    </a>

                    <a target="_blank" href="https://www.instagram.com/audacityflooring/">
                        <i class="fa fa-instagram"></i>
                    </a>

                    <a
                        target="_blank"
                        href="https://www.houzz.com/pro/audacityflooring/__public">
                        <i class="fa  fa-houzz"></i>
                    </a>
</div>
								<?php wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id' => 'primary-menu',
						) );
						?>
							</nav>

							<style>

							a#cn-accept-cookie {
								background:#ffffff;
								    box-shadow: 0 0px 0 rgba(255,255,255,0) inset, 0 0px 0px rgba(0,0,0,0);
								    color: #33257c;
								    text-shadow: 0 0px 0 rgba(0,0,0,0);
								    font-weight:600;
								    border-color: rgba(0,0,0,0) rgba(0,0,0,0) rgba(0,0,0,0);
								    font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
								    letter-spacing: 0.5px;
								    display: initial !important;
								    padding: 5px 15px !important;

							}

							a#cn-more-info{
								background:#33257c;
								    box-shadow: 0 0px 0 rgba(255,255,255,0) inset, 0 0px 0px rgba(0,0,0,0);
								    color: #ffffff;
								    text-shadow: 0 0px 0 rgba(0,0,0,0);
								    font-weight:100;
								    border-color: rgba(0,0,0,0) rgba(0,0,0,0) rgba(0,0,0,0);
								    font-family: -apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif;
								    letter-spacing: 0.5px;
								     display: initial !important;
								    padding: 5px 15px !important;

							}

							</style>
			</header>
			<!-- #masthead -->

			<main id="content" class="site-content" role="main">
