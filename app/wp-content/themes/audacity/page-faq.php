<?php get_header()/* Template Name: FAQ */?>

<section class="title">


	<div class="row blue">

		<h1>

			<?php echo the_title(); ?>

		</h1>

	</div>

</section>

<div class="section faq">

<div class="container accordion">
<?php

// check if the flexible content field has rows of data
if( have_rows('ik_faq') ):

// loop through the rows of data
while ( have_rows('ik_faq') ) : the_row();

if( get_row_layout() == 'ik_question_answer' ):?>

<dt>
<a href=""><h3><?php the_sub_field('ik_question');?></h3></a>
</dt>

<dd>
<?php the_sub_field('ik_answer');?>
</dd>

<?php endif;

endwhile;

else :

// no layouts found

endif;

?>
</div>
</div>


<div class=" container accordion installation">

<h2>installation</h2>

<?php

// check if the flexible content field has rows of data
if( have_rows('ik_faq_copy') ):

// loop through the rows of data
while ( have_rows('ik_faq_copy') ) : the_row();

if( get_row_layout() == 'ik_question_answer_copy' ):?>

<dt>
<a href=""><h3><?php the_sub_field('ik_question_copy');?></h3></a>
</dt>

<dd>
<?php the_sub_field('ik_answer_copy');?>
</dd>

<?php endif;

endwhile;

else :

// no layouts found

endif;

?>
</div>


<div class="container accordion maintenance">

<h2>Maintenance</h2>

<?php

// check if the flexible content field has rows of data
if( have_rows('ik_faq_copy2') ):

// loop through the rows of data
while ( have_rows('ik_faq_copy2') ) : the_row();

if( get_row_layout() == 'ik_question_answer_copy2' ):?>

<dt>
<a href=""><h3><?php the_sub_field('ik_question_copy2');?></h3></a>
</dt>

<dd>
<?php the_sub_field('ik_answer_copy2');?>
</dd>

<?php endif;

endwhile;

else :

// no layouts found

endif;

?>
</div>


</div>
<?php //echo do_shortcode('[get_link_section]') ?>
<div id="new_contact" style="padding:60px 0px" class="row">

<style>
form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:0px 0px 0px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER<br/>
	 <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.</h2> -->
   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>

            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>
		
<?php get_footer(); ?>
