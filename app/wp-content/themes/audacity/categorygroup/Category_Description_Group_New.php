<?php

/*
Plugin Name: WP_List_Table Class Example
Plugin URI: http://sitepoint.com
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Agbonghama Collins
Author URI:  http://w3guy.com
*/
class CategoryDescriptionNew_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'New Category Description',
			'New Category Description',
			'manage_options',
			'category_description_group_form',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		$this->process_post_submit();
        $category_description_detail = $this->get_categorydescdetail();
        wp_enqueue_media();
		?>
		 <style>
		 	.label-info {
			    background-color: #5bc0de;
			}
		 </style>
         <script type="text/javascript">
          jQuery(document).ready( function($) {
                jQuery('#description_type').on('change', function() {
                    if(this.value == 0){
                        jQuery("#divCategoryList").show();
                    }else{
                        jQuery("#divCategoryList").hide();
                    }
                })
          });
         </script>
		<div class="wrap">
			<?php if(isset($_GET["action"])){
				if($_GET["action"]=="edit"){
					echo '<h2>Edit Category Description</h2>';
				}else{
					echo '<h2>Edit Category Description</h2>';
				}
			}else{
				echo '<h2>New Category Description</h2>';
			}
			?>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-1">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<input type="hidden" name="category_description_id" value="<?php echo $category_description_detail["ID"];?>" />
								<input type="hidden" name="issubmit" value="1" />
                                <div class="form-field term-description-wrap">
									<label for="description_type">Description Type</label><br />
                                    <select id="description_type" name="description_type" style="width:100%">
                                        <option value="0" <?php if($category_description_detail["description_type"]==0){echo "selected";} ?>>Category</option>
                                        <option value="1" <?php if($category_description_detail["description_type"]==1){echo "selected";} ?>>Collection</option>
                                    </select>
                                </div>
                                <div class="form-field term-description-wrap" id="divCategoryList" <?php if($category_description_detail["description_type"]!=0){echo "style='display:none'";} ?> >
									<label for="category_description">Category Name</label><br />
                                    <?php
                                    $selecteditem = $category_description_detail["category_id"];
                                    $categories = get_categories( array(
                                        'orderby' => 'name',
                                        'order'   => 'ASC'
                                    ) );
                                    ?>
                                    <select name="category_id" style="width:100%">
                                    <?php
                                        if(count($categories)>0){
                                            echo '<option value="-1">Select</option>';
                                            foreach ( $categories as $item ) {
                                                $isSelected = "";
                                                if($selecteditem==$item->term_id){
                                                    $isSelected = "selected";
                                                }
                                                echo '<option value="'.$item->term_id.'" '.$isSelected.'>'.$item->cat_name.'</option>';
                                            }
                                        }
                                    ?>
                                </select>									
								</div>
								<div class="form-field term-description-wrap">
									<label for="category_description">Category Description</label><br />
                                    <?php wp_editor( $category_description_detail["category_description"], "category_description" ); ?>									
								</div>
                                <div class="form-field term-description-wrap">
									<label for="category_description_link_title">Category Description Button Title</label><br />
                                    <input name="category_description_link_title" id="category_description_link_title" type="text" value="<?php echo $category_description_detail["category_description_link_title"];?>" size="40" aria-required="true">
                                </div>
                                <div class="form-field term-description-wrap">
									<label for="category_description_link">Category Description Link</label><br />
                                    <input name="category_description_link" id="category_description_link" type="text" value="<?php echo $category_description_detail["category_description_link"];?>" size="40" aria-required="true">
                                </div>
                                <div class="form-field term-description-wrap">
									<label for="is_hidden">Is Hidden</label><br />
                                    <?php
                                        $isChecked = "";
                                        if($category_description_detail["is_hidden"]==1){
                                            $isChecked = "checked";
                                        }
                                    ?>
                                    <input name="is_hidden" id="is_hidden" type="checkbox" <?php echo $isChecked; ?>>
                                </div>
                                <?php $postcode_group = apply_filters( 'taxonomy_get_postcode_group','taxonomy_get_postcode_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Postcode Group</label>  <br />                                  			
                                    <select name="taxonomy_postcode_group[]" multiple style="width:100%">
                                        <?php 
                                        $postcode = unserialize($category_description_detail["postcode_group"]);
                                        if(count($postcode_group)>0){
                                            foreach ( $postcode_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $postcode as $selecteditem ) {
                                                    if($selecteditem==$item->ID){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item->ID.'" '.$isSelected.'>'.$item->postcode_group_name.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>                                                                            
                                </div>    
	                            <?php $country_group =  apply_filters( 'taxonomy_get_country_group','taxonomy_get_country_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Country</label>
                                    <select name="taxonomy_country_group[]" multiple style="width:100%">
                                    <?php
                                        $country = unserialize($category_description_detail["country_group"]);
                                        if(count($country_group)>0){
                                            foreach ( $country_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $country as $selecteditem ) {
                                                    if($selecteditem==$item["code"]){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item["code"].'" '.$isSelected.'>'.$item["name"].'</option>';						
                                            }
                                        }
                                    ?>
                                    </select>                                      
                                </div>
								<p class="submit">
									<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Category Description">
								</p>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>		
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Category Description List',
			'default' => 5,
			'option'  => 'ads_per_page'
		];

		add_screen_option( $option, $args );

		
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	public function get_categorydescdetail() {
		$category_description_detail = array();
		$category_description_detail["ID"]=0;
		$category_description_detail["category_id"]="";
        $category_description_detail["category_description"]="";
        $category_description_detail["category_description_link"]="";
        $category_description_detail["category_description_link_title"]="";
        $category_description_detail["description_type"]=0;
        $category_description_detail["is_hidden"]=0;
        $category_description_detail["postcode_group"]="";
        $category_description_detail["country_group"]="";

		if(isset($_GET["action"])){
			if($_GET["action"]=="edit"){
				global $wpdb;
				$query = "select * from wp_category_description_group where ID =".$_GET["ID"];
				$result = $wpdb->get_results($query);
				$lcount = 0;
				if(count($result)>0){
					foreach ( $result as $item ) {
						$category_description_detail["ID"] = $item->ID;
                        $category_description_detail["category_id"] = $item->category_id;
                        $category_description_detail["category_description"] = $item->category_description;
                        $category_description_detail["category_description_link"] = $item->category_description_link;
                        $category_description_detail["category_description_link_title"] = $item->category_description_link_title;
                        $category_description_detail["description_type"] = $item->description_type;
                        $category_description_detail["is_hidden"] = $item->is_hidden;
                        $category_description_detail["postcode_group"] = $item->postcode_group;
                        $category_description_detail["country_group"] = $item->country_group;
						break;
					}
				}
				
			}
		}
		return $category_description_detail;
	}
	public function process_post_submit() {
		if(isset($_POST["issubmit"])){
			$category_id = $_POST["category_id"];
            $category_description = $_POST["category_description"];
            $category_description_link = $_POST["category_description_link"];
            $category_description_link_title= $_POST["category_description_link_title"];
            $is_hidden= $_POST["is_hidden"];
            if(empty($is_hidden)){
                $is_hidden = 0;
            }else{
                $is_hidden = 1;   
            }
            $postcode_group = serialize($_POST["taxonomy_postcode_group"]);
            $country_group = serialize($_POST["taxonomy_country_group"]);
            $category_description_id = $_POST["category_description_id"];
            $description_type = $_POST["description_type"];
			global $wpdb;
			$query = "";
			if($category_description_id == 0){
				$query = "INSERT INTO wp_category_description_group(category_id, category_description, category_description_link, category_description_link_title, is_hidden, postcode_group, country_group, description_type, is_active) VALUES(".$category_id.",'".$category_description."','".$category_description_link."','".$category_description_link_title."',".$is_hidden.",'".$postcode_group."','".$country_group."',".$description_type.",1)";				
			}else{
				$query = "update wp_category_description_group set category_id=".$category_id.", category_description='".$category_description."', category_description_link='".$category_description_link."', category_description_link_title = '".$category_description_link_title."', is_hidden = ".$is_hidden.", postcode_group='".$postcode_group."', country_group='".$country_group."', description_type=".$description_type." where ID =".$category_description_id;
			}
            $wpdb->get_results($query);
			echo '<script type="text/javascript">';
			echo 'window.location = "?page=category_description_group_listing";';
			echo '</script>';
			exit;
		}
	}

}
