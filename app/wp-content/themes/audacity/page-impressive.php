<?php /* Template Name: Impressive */

get_header();

?>

<script>
jQuery(function ($) {

  $(document).ready(function () {

var fullscreen = document.getElementById('fullscreen');
var fullscreenscratch = document.getElementById('fullscreenscratch');
var fullscreenstain = document.getElementById('fullscreenstain');

// $("video").each(function () {

	// fullscreen.addEventListener('click',function(){

fullscreen.addEventListener('click',function(){
	// console.log ('it is working'); 
	if(cigarette.requestFullscreen){
		cigarette.requestFullscreen();
	} 
	else if (cigarette.webkitRequestFullscreen){
		cigarette.webkitRequestFullscreen();
	}
	else if (cigarette.mozRequestFullScreen){
		cigarette.mozRequestFullScreen();
	}
	else if (cigarette.msRequestFullscreen){
		cigarette.msRequestFullscreen();
	}   

});


fullscreenscratch.addEventListener('click',function(){
	// console.log ('it is working'); 
	if(scratch.requestFullscreen){
		scratch.requestFullscreen();
	} 
	else if (scratch.webkitRequestFullscreen){
		scratch.webkitRequestFullscreen();
	}
	else if (scratch.mozRequestFullScreen){
		scratch.mozRequestFullScreen();
	}
	else if (scratch.msRequestFullscreen){
		scratch.msRequestFullscreen();
	}   

});

fullscreenstain.addEventListener('click',function(){
	// console.log ('it is working'); 
	if(stain.requestFullscreen){
		stain.requestFullscreen();
	} 
	else if (stain.webkitRequestFullscreen){
		stain.webkitRequestFullscreen();
	}
	else if (stain.mozRequestFullScreen){
		stain.mozRequestFullScreen();
	}
	else if (stain.msRequestFullscreen){
		stain.msRequestFullscreen();
	}   

});



if (document.addEventListener)
{
document.addEventListener('webkitfullscreenchange', exitHandler, false);
document.addEventListener('mozfullscreenchange', exitHandler, false);
document.addEventListener('fullscreenchange', exitHandler, false);
document.addEventListener('MSFullscreenChange', exitHandler, false);
}

function exitHandler()
{
if (document.webkitIsFullScreen === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
else if (document.mozFullScreen === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
else if (document.msFullscreenElement === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
}     


// $('.slick-active').parent().click(function () {
//     // $(this).find("video")[0].play()
//     $(this).find(".outer-video").show()
//     $('.lightbox-background').fadeIn('slow')
// });

$('img.trigger.cigarette').click(function () {
	 $(".outer-video.cigarette").fadeIn('slow')
	// $('.lightbox-background').fadeIn('slow')
	// $('body').css('position', 'fixed')
	// $('body').css('bottom', '0px')
	$("video#cigarette")[0].play()
	 $("video#cigarette").css('display', 'block')
});

$('img.trigger.stain').click(function () {
	$(".outer-video.stain").fadeIn('slow')
	// $('.lightbox-background').fadeIn('slow')
	// $('body').css('position', 'fixed')
	// $('body').css('bottom', '0px')
	$("video#stain")[0].play()
	$("video#stain").css('display', 'block')
});

$('img.trigger.scratch').click(function () {
	$(".outer-video.scratch").fadeIn('slow')
	// $('.lightbox-background').fadeIn('slow')
	// $('body').css('position', 'fixed')
	// $('body').css('bottom', '0px')
	$("video#scratch")[0].play()
	$("video#scratch").css('display', 'block')
});

$('.lightbox-background').click(function () {
	this.pause();
	this.currentTime = 0;
	$(this).fadeOut('slow');
	$('body').css('position', 'initial')
	$('body').css('bottom', 'initial')
	$(".outer-video").fadeOut('slow')
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 1000);
});

$('.fa-window-close').click(function () {
	// this.pause();
	// this.currentTime = 0;
	// $(this).fadeOut('slow');
	$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
	$('.lightbox-background').fadeOut('slow')
	$('video').fadeOut('slow')
	$('body').css('position', 'initial')
	$('body').css('bottom', 'initial')
	$(".outer-video").fadeOut('slow')
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 1000);
});

});
});
</script>

<section class="title">


        <div class="row blue">

                <h1>

                        <?php echo the_title(); ?>

                </h1>

        </div>

</section>

<section class="intro">

        <div class="row">
                <table>
                        <tr>
                                <td>
                                        <!-- <div class="col-md-6"> -->
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/impressive.jpg">

                                        <!-- </div> -->
                                </td>

                                <td>
                                        <table>
                                                <tr>
                                                        <td id="parallax-id-1">
                                                                <!-- <div class="col-md-6"> -->
                                                                <div class="inner">
                                                                        <h2 class="black">
                                                                                                                                   <?php the_field('water_plank_06'); ?>
                                                        <?php the_field('impressive_intro_heading'); ?>

                                                                        </h2>
                                                                        <p>
                                                                        <?php the_field('impressive_intro_content'); ?>

                                                                        </p>
                                                                </div>

                                                        </td>
                                                        <td id="parallax-id-2" class="icon" valign="middle">
                                                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/02.png">
                                                        </td>
                                                </tr>
                                        </table>
                                        <!-- </div> -->
                                </td>
                                <!-- <td>

</td> -->
                        </tr>
                </table>
        </div>
</section>

<div id="parallax-id-2-5">

        <section class="full blue">
                <div class="container">
                        <div class="row">
                                <div class="col-md-offset-3 col-md-3"></div>
                                <div class="col-md-6">
                                        <h2>
                                        <?php the_field('impressive_break_heading'); ?>
                                        </h2>
                                        <p>
                                        <?php the_field('impressive_break_content'); ?>

                                        </p>
                                </div>

                        </div>
                        <br/>
                        <br/>
                        <div class="row grid">
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/03.svg">
                                        <p>Pet-friendly</p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/06.svg">
                                        <p>Stain resistant</p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/07.svg">
                                        <p>Scratch resistant</p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/last.svg">
                                        <p>Impact resistant</p>

                                </div>
                        </div>
                </div>
        </section>
</div>

<div id="parallax-id-3">

        <section class="construction">
                <div class="container">
                        <div class="row">
                                <div class="col-md-6 everyroom">
                                        <h2> 
                                        <?php the_field('impressive_bottom_heading_01'); ?>
                                                
                                        </h2>
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/04.svg">

                                        <p>
                                        <?php the_field('impressive_bottom_01_content'); ?>

                                        </p>
                                </div>

                                <div class="col-md-6 quiet">
                                        <h2> 
                                        <?php the_field('impressive_bottom_02_heading'); ?>
                                                
                                        </h2>
                                        <div class="row">
                                                <div class="col-md-6">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/19.svg">

                                                </div>
                                                <div class="col-md-6">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/05.svg">

                                                </div>
                                        </div>
                                        <p>
                                        <?php the_field('impressive_bottom_2_content'); ?>

                                        </p>
                                </div>
                        </div>
                </div>
        </section>
</div>

<div id="parallax-id-4">

        <section class="video-slide">
                <div class="container">
                        <div class="row">
                                <div class="col-md-6">

                                        <div class="video-slider">
                                                <div>
                                                        <img id="fullscreen" class="trigger cigarette" src="<?php bloginfo('template_directory'); ?>/assets/img/cigarette.jpg">
                                                        <!-- <div class="outer-video">
                                                                <video loop id="cigarette" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/cigarette.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                                <div>
                                                        <img id="fullscreenscratch" class="trigger scratch" src="<?php bloginfo('template_directory'); ?>/assets/img/scratch.jpg">
                                                        <!-- <div class="outer-video">

                                                                <video loop id="screen" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/scratch.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                                <div>
                                                        <img id="fullscreenstain" class="trigger stain" src="<?php bloginfo('template_directory'); ?>/assets/img/stain.jpg">

                                                        <!-- <div class="outer-video">

                                                                <video loop id="screen" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/stain.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                        </div>

                                </div>

                                <div class="col-md-6 align">
                                <div class="row ">

                                        <h2>
                                        <?php the_field('impressive_video_heading'); ?>

                                        </h2>

                                                <p>
                                                <?php the_field('impressive_video_content'); ?>

                                                </p>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>
</div>

<div class="outer-video cigarette">
<i class="fa fa-window-close"></i>

        <video loop id="cigarette" width="100%" height="auto" controls>

                <source src="<?php bloginfo('template_directory'); ?>/assets/videos/cigarette.mp4" type="video/mp4">

        </video>

      
</div>

<div class="outer-video scratch">
        <i class="fa fa-window-close"></i>

          <video loop id="scratch" width="100%" height="auto" controls>

                <source src="<?php bloginfo('template_directory'); ?>/assets/videos/scratch.mp4" type="video/mp4">

        </video>


</div>

<div class="outer-video stain">
        <i class="fa fa-window-close"></i>


        <video loop id="stain" width="100%" height="auto" controls>

                <source src="<?php bloginfo('template_directory'); ?>/assets/videos/stain.mp4" type="video/mp4">

        </video>
</div>

<div class="lightbox-background">
</div>
<?php //echo do_shortcode('[get_link_section]') ?>
<div id="new_contact" style="padding:60px 0px" class="row">

<style>

body.website-6 .everyroom{
display:none;
}


body.website-6 .quiet{
margin:0 auto;
}



form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:0px 0px 0px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER<br/>
     <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.</h2> -->

   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>

            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>
<?php get_footer(); ?>