function ipLookUp () {
  jQuery.ajax('https://ipapi.co/json')
  .then(
      function success(response) {
          console.log('User\'s Location Data is ', response);
          console.log('User\'s Country', response.country);
var parts = location.pathname.split('/');
          var CountryName = response.country_name;


            if (CountryName == 'United Kingdom') {
              if(window.location.href.indexOf('uk') > -1) {
         jQuery('.right-side').remove();
      }
      else {

        var countrySelected = Cookies.get('audacitySelected');
        if (countrySelected !== "countrySelected") {
          jQuery('body > .right-side').addClass('redirecting');
          jQuery('body > .right-side').prepend('<p>You are being redirected to our ' + CountryName +' Website...</p>');
      jQuery('#country_selector_modal').css('opacity', '0');
        window.location = "https://audacityflooring.local/uk"
        }
        else {
          jQuery('.right-side').remove();
        }
      }



            }

            else if (CountryName == 'Australia') {
              if(window.location.href.indexOf('au') > -1) {
         jQuery('.right-side').remove();
      }
      else {

        var countrySelected = Cookies.get('audacitySelected');
        if (countrySelected !== "countrySelected") {
          jQuery('body > .right-side').addClass('redirecting');
            jQuery('body > .right-side').prepend('<p>You are being redirected to our ' + CountryName +' Website...</p>');
            jQuery('#country_selector_modal').css('opacity', '0');
        window.location = "https://audacityflooring.local/au"
        }
        else {
          jQuery('.right-side').remove();
        }
      }


            }



            else if (CountryName == 'Canada')  {
              if(window.location.href.indexOf('ca') > -1) {
         jQuery('.right-side').remove();
      }
      else {

        var countrySelected = Cookies.get('audacitySelected');
        if (countrySelected !== "countrySelected") {
          jQuery('body > .right-side').addClass('redirecting');
            jQuery('body > .right-side').prepend('<p>You are being redirected to our ' + CountryName +' Website...</p>');
          jQuery('#country_selector_modal').css('opacity', '0');
        window.location = "https://audacityflooring.local/ca"
        }
        else {
          jQuery('.right-side').remove();
        }
        }


      }

      else if (CountryName == 'United States') {

      var CountryName = 'The United States Of America';
jQuery('.right-side').remove();

      }

      else {
          jQuery('.right-side').remove();
      }

  },

      function fail(data, status) {
          console.log('Request failed.  Returned status of',
                      status);
      }
  );
}

jQuery('body').prepend('<div class="currentSiteLocation" style="display: none">'+ jQuery('.country_selector_seems_text').text() +'</div>');
var currentCountry = Cookies.get('currentCountry');
var currentSiteCountry = jQuery('.currentSiteLocation').text();
          if ( currentCountry !== currentSiteCountry ) {
            ipLookUp();
          }
          else {
            jQuery('.right-side').remove();
          Cookies.remove('audacitySelected');
          }



jQuery(function ($) {



if(jQuery(window).width() < 767 ){

jQuery("a.cta-mobile").click(function() {
  $('.contact-popup').fadeIn('fast');

});

jQuery("i.popup-close").click(function() {
  $('.contact-popup').fadeOut('fast');

});

if(window.outerWidth < 768) {

$(".header_drop").prependTo('.overlay.overlay--menu');
$(".cta-mobile").append('<p>We love helping out. Just ask !</p>');

}

};


$('.dms-select > option:nth-child(1)').remove();

$('#country_selector_modal').on($.modal.AFTER_CLOSE, function(event, modal) {

var countryExit = Cookies.get('countryExit');

  if (countryExit !== "countryExitOK") {
  Cookies.set('countryExit', 'countryExitOK', { expires: 1 });
  }

});


  $('#country_selector_modal').on($.modal.BEFORE_OPEN, function(event, modal) {

    var CountryName = $('.country_selector_seems_text').text();

    var parts = location.pathname.split('/');
    $(document).on('change','#country-dropdown',function(){
      var countrySelected = Cookies.get('audacitySelected');
      if (countrySelected !== "countrySelected") {
      Cookies.set('audacitySelected', 'countrySelected', { expires: 7 });
      }
      var countryExit = Cookies.get('countryExit');

        if (countryExit !== "countryExitOK") {
        Cookies.set('countryExit', 'countryExitOK', { expires: 1 });
        }

   });

   $(document).on('change','.dms-select',function(){
     var countrySelected = Cookies.get('audacitySelected');
     if (countrySelected !== "countrySelected") {
     Cookies.set('audacitySelected', 'countrySelected', { expires: 7 });
     }
     var countryExit = Cookies.get('countryExit');

       if (countryExit !== "countryExitOK") {
       Cookies.set('countryExit', 'countryExitOK', { expires: 1 });
       }

  });



var countryExit = Cookies.get('countryExit');

if (countryExit === "countryExitOK") {
  $('.jquery-modal').remove();
}
var countryName = $('.country_selector_seems_text').text();
var countryName2 = $('.country_selector_seems_text').text();

if ( countryName != 'United Kingdom' ) {
  if (countryName != 'Australia') {
    if ( countryName != 'Canada' ) {
      if ( countryName != 'United States' ) {
        var countryName2 = 'International';
      }
      else if (countryName == 'United States') {
        var countryName = 'The United States of America';
        var countryName2 = 'The United States of America';
      }
      else if (countryName == 'United Kingdom') {
        var countryName = 'The United Kingdom';
        var countryName2 = 'The United Kingdom';
      }
    }
  }
}

var theCountryList = ['United States of America', 'United Kingdom'];

var countryNameThePrefix = function(optText){
  return theCountryList.indexOf(optText) > -1 ? "The " : "";
}

$('.country_selector_modal_text').prepend('<p>Seems like you are in '+ countryNameThePrefix(countryName) + countryName +'!</p>');
$('.country_selector_modal_buttons').append('<div class="stay-wrap"><div class="stay"><a href="#close-modal" rel="modal:close" >Stay on ' + countryNameThePrefix(countryName2) + countryName2 + ' page</a></div></div><div class="reason">This helps us deliver a better service, showing you which collections are available.</div>');
    $('.country_selector_modal_buttons').prepend('<div id="country-dropdown-wrap"><select id="country-dropdown"><option selected disabled>Select Your Area</option>' + $('.dms-select').html() + '</select></div>');
    $('.country_selector_modal_buttons #country-dropdown > option:last-child').removeAttr('selected');
    $('.country_selector_modal_buttons #country-dropdown > option:first-child').attr('selected', 'selected');



    $('#country_selector_modal select').prepend('<option value="https://audacityflooring.local/international">International</option>');
    $('.dms-container select').prepend('<option value="https://audacityflooring.local/international">International</option>');


    $("#country-dropdown").change(function() {
      var option = $(this).find('option:selected');
        window.location.href = option.val();
      var optionText = $(this).find('option:selected').text();
      Cookies.set('currentCountry', optionText, { expires: 7 });
    });

    $(".dms-select").change(function() {

      var optionText = $(this).find('option:selected').text();
      Cookies.set('currentCountry', optionText, { expires: 7 });
      window.location.href = option.val();
    });

  });

  $('<option class="active" value="' + $('.logo > a').attr('href') + '" selected="">'+ $('.logo > a > img').attr('title') +'</option>').appendTo('.dms-select');
  // var theCountryList = ['United States of America', 'United Kingdom'];
  //    $("option", '.dms-select').each(function() {
  //        var optText = this.text;
  //        if(theCountryList.indexOf(optText) > -1){
  //         this.text = "The " + this.text;
  //        }
  //    });
    $(".cta").click(function() {
        $('html,body').animate({
            scrollTop: $("#gform_wrapper_2").offset().top - 270},
            'slow');
    });

    $(".cta-home").click(function() {
        $('html,body').animate({
            scrollTop: $("http://audacity.local/#section3").offset().top - 180},
            'slow');
    });



    if ($('#video-modal').hasClass('current')) {
        ('header').css('background', 'red')
    };

    $('input#input_2_9_3').attr('readonly', 'readonly');
    $('input#input_2_9_5').attr('readonly', 'readonly');

    $(".warranty").click(function () {
        window.open('http://thibaultrolando.com/Audacity/wp-content/uploads/2018/01/Audacity-Residential-Warranty.pdf');
        window.open('http://thibaultrolando.com/Audacity/wp-content/uploads/2018/01/Audacity-Commercial-Warranty.pdf');
    });


    (function ($) {
        var allPanels = $('.accordion > dd').hide();
        $('.accordion > dt > a').click(function () {
            allPanels.slideUp();
            $(this).parent().next().slideDown();
            return false;
        });
        $('.accordion > dt > a').eq(0).click()
    })(jQuery);


    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $('#masthead').addClass("sticky");
        } else {
            $('#masthead').removeClass("sticky");
        }
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrolltop').fadeIn();
        } else {
            $('.scrolltop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scrolltop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 800);
        return false;
    });


    $(window).load(function () {


        // interval passed to reveal
        window.sr = ScrollReveal({
            duration: 500
        });
        sr.reveal('ul.products article', 300);
        sr.reveal('section.tab-info .item', 300);
        sr.reveal('.accordion ul li', 300);
        sr.reveal('.section.support .col-md-6', 300);
        sr.reveal('.page-template-page-water .construction tr', 300);
        sr.reveal('.page-template-page-impressive .full .col-lg-3', 300);
        sr.reveal('.page-template-page-impressive .construction img', 300);
        sr.reveal('.page-template-page-impressive .intro td.icon img', 300);
        sr.reveal('.page-template-page-installation .full .col-lg-3', 300);
        sr.reveal('.page-template-page-installation .construction img', 300);
        sr.reveal('.page-template-page-installation .intro td.icon img', 300);

    });

     $(document).ready(function () {




//         var fullscreen = document.getElementById('fullscreen');
//         var fullscreenscratch = document.getElementById('fullscreenscratch');
//         var fullscreenstain = document.getElementById('fullscreenstain');

//         // $("video").each(function () {

//             // fullscreen.addEventListener('click',function(){

//         fullscreen.addEventListener('click',function(){
//             // console.log ('it is working');
//             if(cigarette.requestFullscreen){
//                 cigarette.requestFullscreen();
//             }
//             else if (cigarette.webkitRequestFullscreen){
//                 cigarette.webkitRequestFullscreen();
//             }
//             else if (cigarette.mozRequestFullScreen){
//                 cigarette.mozRequestFullScreen();
//             }
//             else if (cigarette.msRequestFullscreen){
//                 cigarette.msRequestFullscreen();
//             }

//         });


//         fullscreenscratch.addEventListener('click',function(){
//             // console.log ('it is working');
//             if(scratch.requestFullscreen){
//                 scratch.requestFullscreen();
//             }
//             else if (scratch.webkitRequestFullscreen){
//                 scratch.webkitRequestFullscreen();
//             }
//             else if (scratch.mozRequestFullScreen){
//                 scratch.mozRequestFullScreen();
//             }
//             else if (scratch.msRequestFullscreen){
//                 scratch.msRequestFullscreen();
//             }

//         });

//         fullscreenstain.addEventListener('click',function(){
//             // console.log ('it is working');
//             if(stain.requestFullscreen){
//                 stain.requestFullscreen();
//             }
//             else if (stain.webkitRequestFullscreen){
//                 stain.webkitRequestFullscreen();
//             }
//             else if (stain.mozRequestFullScreen){
//                 stain.mozRequestFullScreen();
//             }
//             else if (stain.msRequestFullscreen){
//                 stain.msRequestFullscreen();
//             }

//         });



// if (document.addEventListener)
// {
//     document.addEventListener('webkitfullscreenchange', exitHandler, false);
//     document.addEventListener('mozfullscreenchange', exitHandler, false);
//     document.addEventListener('fullscreenchange', exitHandler, false);
//     document.addEventListener('MSFullscreenChange', exitHandler, false);
// }

// function exitHandler()
// {
//     if (document.webkitIsFullScreen === false)
//     {
//         console.log('test')
//     $("video").each(function () {
//                 this.pause();this.currentTime = 0;
//             })
//     }
//     else if (document.mozFullScreen === false)
//     {
//         console.log('test')
//     $("video").each(function () {
//                 this.pause();this.currentTime = 0;
//             })
//     }
//     else if (document.msFullscreenElement === false)
//     {
//         console.log('test')
//     $("video").each(function () {
//                 this.pause();this.currentTime = 0;
//             })
//     }
// }


//         // $('.slick-active').parent().click(function () {
//         //     // $(this).find("video")[0].play()
//         //     $(this).find(".outer-video").show()
//         //     $('.lightbox-background').fadeIn('slow')
//         // });

//         $('img.trigger.cigarette').click(function () {
//              $(".outer-video.cigarette").fadeIn('slow')
//             // $('.lightbox-background').fadeIn('slow')
//             // $('body').css('position', 'fixed')
//             // $('body').css('bottom', '0px')
//             $("video#cigarette")[0].play()
//              $("video#cigarette").css('display', 'block')
//         });

//         $('img.trigger.stain').click(function () {
//             $(".outer-video.stain").fadeIn('slow')
//             // $('.lightbox-background').fadeIn('slow')
//             // $('body').css('position', 'fixed')
//             // $('body').css('bottom', '0px')
//             $("video#stain")[0].play()
//             $("video#stain").css('display', 'block')
//         });

//         $('img.trigger.scratch').click(function () {
//             $(".outer-video.scratch").fadeIn('slow')
//             // $('.lightbox-background').fadeIn('slow')
//             // $('body').css('position', 'fixed')
//             // $('body').css('bottom', '0px')
//             $("video#scratch")[0].play()
//             $("video#scratch").css('display', 'block')
//         });

//         $('.lightbox-background').click(function () {
//             this.pause();
//             this.currentTime = 0;
//             $(this).fadeOut('slow');
//             $('body').css('position', 'initial')
//             $('body').css('bottom', 'initial')
//             $(".outer-video").fadeOut('slow')
//             $("html, body").animate({
//                 scrollTop: $(document).height()
//             }, 1000);
//         });

//         $('.fa-window-close').click(function () {
//             // this.pause();
//             // this.currentTime = 0;
//             // $(this).fadeOut('slow');
//             $("video").each(function () {
//                 this.pause();this.currentTime = 0;
//             })
//             $('.lightbox-background').fadeOut('slow')
//             $('video').fadeOut('slow')
//             $('body').css('position', 'initial')
//             $('body').css('bottom', 'initial')
//             $(".outer-video").fadeOut('slow')
//             $("html, body").animate({
//                 scrollTop: $(document).height()
//             }, 1000);
//         });



        // $('video').click(function () {
        //     $(this).fadeOut('slow')
        //     this.pause();
        //     this.currentTime = 0;
        //     $('.lightbox-background').fadeOut('slow')
        //     $('body').css('position', 'initial')
        //     $('body').css('bottom', 'initial')
        //     $(".outer-video").fadeOut('slow')
        //     $("html, body").animate({
        //         scrollTop: $(document).height()
        //     }, 1000);


        // });


        $(function () {
            $('li.menu-item a').click(function () {
                $('nav.primary-navigation').css('display', 'none');
            });
        });


        if ($(window).width() > 990) {
            $(function () {

                jQuery('#parallax-id-1 .inner-center').heliumParallax({

                    minVal: ['-150px'],
                    maxVal: ['150px']
                });
                jQuery('#parallax-id-1 .inner').heliumParallax({

                    minVal: ['-150px'],
                    maxVal: ['50px']
                });

                jQuery('#parallax-id-2').heliumParallax({

                    minVal: ['-150px'],
                    maxVal: ['150px']
                });

                jQuery('#parallax-id-2-5').heliumParallax({

                    minVal: ['-150px'],
                    maxVal: ['150px']
                });

                jQuery('#parallax-id-2-5 .full').heliumParallax({

                    minVal: ['80px'],
                    maxVal: ['-50px']
                });



                jQuery('#parallax-id-3 .construction').heliumParallax({

                    minVal: ['0px'],
                    maxVal: ['0px']
                });
            });
        }

        if ($(window).width() < 990) {
            $(function () {

                jQuery('#parallax-id-1 .inner-center').heliumParallax({

                    minVal: ['0px'],
                    maxVal: ['-100px']
                });
                jQuery('#parallax-id-1 .inner').heliumParallax({


                    minVal: ['0px'],
                    maxVal: ['-100px']

                });

                jQuery('#parallax-id-2').heliumParallax({

                    minVal: ['0px'],
                    maxVal: ['-100px']
                });
            });
        }


        // initialize paroller.js
        $("body").paroller();

        //EQUAL HEIGHT

        //* HAMBUGER MENU *//

        $('.fa-bars').click(function () {
            $('nav.primary-navigation').slideToggle();
        });

        $('.fa-times').click(function () {
            $('nav.primary-navigation').slideToggle();
        });

        //NEW scrollOverflow

        if ($(window).width() > 960) {

            $(function () {
                $.scrollify({
                    section: ".example-classname",
                    sectionName: ".example-classname",
                    //  interstitialSection : ".support",
                    easing: "easeOutExpo",
                    scrollSpeed: 1100,
                    offset: 0,
                    scrollbars: true,
                    standardScrollElements: ".support",
                    setHeights: true,
                    overflowScroll: true,
                    updateHash: true,
                    touchScroll: true,
                    before: function () {},
                    after: function () {},
                    afterResize: function () {},
                    afterRender: function () {}
                });

                // $.scrollify({
                //   section : ".example-classname",
                // });
            });
        }

        //END EQUAL HEIGHT

        // $('#fullpage').fullpage({
        //     sectionsColor: ['#ffffff', '#ffffff', '#ffffff'],


        // });

        const $container = $('.container-elastic');
        const $slide = $('.slide');
        const $text = $('.slide__text');
        const numSlides = 4;
        const initialAnimDur = 5705;
        const animDelay = 1000;
        let initialAnim = true;
        let clickAnim = false;

        $(document).on('click', '.slide__bg-dark', function () {




            if (initialAnim || clickAnim) return;
            let _this = $(this).parent();
            let target = +_this.attr('data-target');
            clickAnim = true;

            _this.css({
                'transform': 'translate3d(-100%, 0, 0)',
                'transition': '750ms',
                'cursor': 'default'
            })

            _this.find('.slide__img-wrapper').css({
                'transform': 'translate3d(0, 0, 0) scale(.95, .95)',
                'transition': '2000ms'
            })

            for (let i = target, length = $slide.length; i < length; i++) {
                $('.slide--' + (i + 1)).css({
                    'transform': 'translate3d(0, 0, 0)',
                    'transition': '750ms'
                })
            }

            for (let i = target, length = $slide.length; i > 1; i--) {
                $('.slide--' + (i - 1)).css({
                    'transform': 'translate3d(-125%, 0, 0)',
                    'transition': '750ms'
                })
            }

            setTimeout(function () {
                $slide.not(_this).find('.slide__bg-dark').css({
                    'opacity': '0'
                })
            }, 750)


            _this.find('.slide__text').css({
                'transform': 'translate3d(150px, -40%, 0)',
                'opacity': '1',
                'transition': '2000ms',
                '-webkit-transition': '2000ms'
            })
        });


        $(document).on('mouseleave', '.slide', function () {
            if (initialAnim || clickAnim) return;
            let _this = $(this);
            let target = +_this.attr('data-target');

            for (let i = 1, length = $slide.length; i <= length; i++) {
                $('.slide--' + i).css({
                    'transform': 'translate3d(-' + (100 / numSlides) * (numSlides - (i - 1)) + '%, 0, 0)',
                    'transition': '1000ms'
                })
            }

            $slide.find('.slide__img-wrapper').css({
                'transform': 'translate3d(-200px, 0, 0) scale(1, 1)',
                'transition': '750ms'
            })

            $slide.find('.slide__bg-dark').css({
                'opacity': '0'
            })

            $text.css({
                'transform': 'translate3d(0, -50%, 0) rotate(0.01deg)',
                'opacity': '0',
                'transition': '200ms',
                '-webkit-transition': '200ms'
            })
        });

        $(document).on('click', '.slide__close', function () {

            setTimeout(function () {
                clickAnim = false;
            }, 1000);


            for (let i = 1, length = $slide.length; i <= length; i++) {
                $('.slide--' + i).css({
                    'transform': 'translate3d(-' + (100 / numSlides) * (numSlides - (i - 1)) + '%, 0, 0)',
                    'transition': '1000ms',
                    'cursor': 'pointer'
                })
            }

            $text.css({
                'transform': 'translate3d(150px, -40%, 0)',
                'opacity': '0',
                'transition': '200ms',
                '-webkit-transition': '200ms'
            })

            setTimeout(function () {
                $text.css({
                    'transform': 'translate3d(0, -50%, 0)'
                })
            }, 200)
        })

        setTimeout(function () {
            $container.addClass('active');
        }, animDelay);

        setTimeout(function () {
            initialAnim = false;
        }, initialAnimDur + animDelay);

    });

    //SLICK SLIDER

    // $( ".collections-slider " ).each(function( index ) {
    //     $(this).find('img').wrap('<article></article>')
    //     });

    $('.collections-slider').slick({
        centerMode: true,
        slidesToShow: 1,
        centerPadding: '0px',
        arrows: true,
        responsive: [{
                breakpoint: 1500,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 1440,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 1270,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },

            {
                breakpoint: 1040,
                settings: {
                    centerMode: true,
                    centerPadding: '0px',
                    slidesToShow: 1
                }
            },


            {
                breakpoint: 900,
                settings: {
                    centerMode: true,
                    centerPadding: '80px',
                    slidesToShow: 1
                }
            },


            {
                breakpoint: 768,
                settings: {
                    centerMode: true,
                    centerPadding: '40px',
                    slidesToShow: 1
                }
            }
        ]
    });

    $('.collections-slider-page').slick({
        autoplay: false,
        infinite: true,
        arrows: true,
        infinite: true
    });

    //   $('.single-slider').slick({
    //     autoplay: true,
    //     infinite: true,
    //     arrows: true,
    //     infinite: true
    //   });

    $('.carousel').carousel({
        interval: 5000
    })

    $('.video-slider').slick({});



    //   $('.slick-current').click(function () {
    //   });

    // modal

    $('#video-modal').on($.modal.OPEN, function (event, modal) {
        var vid = document.getElementById("homeVid");
        vid.pause();

        jQuery('#video-modal').find(".mejs-button")[0].click();

    });

    $('#video-modal').on($.modal.BEFORE_CLOSE, function (event, modal) {
        jQuery('#video-modal').find("button[title='pause']").click();
        var vid = document.getElementById("homeVid");
        vid.play();
    });

});
