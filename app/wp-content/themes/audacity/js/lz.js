(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.Tween5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#656F7A").s().p("Ag/g4IAcAFIAhiUIBXAeIhtE/Ig8Ayg");
	this.shape.setTransform(4.1,-18.5,1,1,0,0,0,4.1,-18.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.4,-20,17,40.1);


(lib.Tween4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FBA0A1").s().p("AAKAxQgbgGgIgdQAAgFgCgDQgEgDgEAGIgRAaQgCAEgFAAQgEgBAAgIQABgNAJgnQACgIAHgIQAOgQAXAGQAFAAAeATIAeATQAKAFgHALIgZArIgLACQgHAAgIgCg");
	this.shape.setTransform(0.3,-0.1,1.613,1.613,5.7);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.5,-8.5,21.1,17);


(lib.tick = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#656F7A").ss(2).p("AhtAFIBEBEICYiY");
	this.shape.setTransform(0,0.5,1.336,1.336);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-15.7,-11.1,31.5,22.7);


(lib.shoe = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#656F7A").s().p("Ag/g4IAcAFIAhiUIBXAeIhtE/Ig8Ayg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-8.4,-20,17,40.1);


(lib.head2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#656F7A").p("AgTAAQAAgHAGgGQAGgFAHAAQAIAAAGAFQAGAGAAAHQAAAIgGAGQgGAGgIAAQgHAAgGgGQgGgFAAgJg");
	this.shape.setTransform(-7.6,1.2,1.407,1.407);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#656F7A").ss(1,1,1).p("AgwAQIBJgfIAYAQ");
	this.shape_1.setTransform(-0.1,-1.6);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#656F7A").p("AgUAAQAAgIAGgGQAGgGAIAAQAJAAAGAGQAGAGAAAIQAAAJgGAGQgGAGgJAAQgIAAgGgGQgGgGAAgJg");
	this.shape_2.setTransform(-15.3,-0.3,1.407,1.407);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FE699A").s().p("AAlBgQgNgSABgTQAAgQAMgNIAJgMQAEgHgDgGQgHgOgWAKQgOAHgdgHQgngIgogaQgmggARgjQAGgOAUgBQAJgBAJACQAVAFATAPQAOAGAMABQAPABASgHQA6gWAZAVQAPAMACATQABAQgHAPQgRAgAWARQANALABASQACASgNANIgkAiQgJAGgIAAQgQAAgOgVg");
	this.shape_3.setTransform(0,-1.4,1.407,1.407);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("#FBA0A1").s().p("AgMAmIABgHIAYhJIAABQQgDADgNACIgDAAQgGAAAAgFg");
	this.shape_4.setTransform(-12.7,1.9,1.407,1.407);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#FBA0A1").s().p("AhFBcQgJAAgHgHQgGgGAAgKIAFiJQAAgJAHgGQAGgHAJAAICGgBQAJABAGAGQAGAGAAAJIABCKQAAAKgGAGQgHAHgJAAg");
	this.shape_5.setTransform(1.2,5,1.407,1.407);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-19.2,-17.7,37.8,35.6);


(lib.hand2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FBA0A1").s().p("AALBQQgrgOgIgwQABgHgEgGQgFgFgHAIIggAnQgFAHgHgCQgHgCACgMQADgVAWg9QAFgNAMgMQAYgXAlANQAGABAvAjIAtAjQAPAJgNASIguBCIgJAAQgQAAgRgFg");

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.5,-8.5,21.1,17);


(lib.forearm2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#656F7A").ss(1.5,1,1).p("AijhUIFHCp");
	this.shape.setTransform(2.7,-15.2,1.267,1.267);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("#656F7A").s().p("AipiyIFTEyIgyAzg");
	this.shape_1.setTransform(-1.9,3.4,1.267,1.267);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_1},{t:this.shape}]}).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-23.4,-26.9,48,53);


(lib.state4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#9BC3FB").ss(2).p("AmYjoIAAHHIM7AA");
	this.shape.setTransform(-0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-23.7,84.9,47.5);


(lib.state3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#9BC3FB").ss(2).p("AmiDfIM7AAIAAnH");
	this.shape.setTransform(0.5,0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-23.7,84.9,47.5);


(lib.state2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#9BC3FB").ss(2).p("AmijeIM7AAIAAHH");
	this.shape.setTransform(0.5,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-23.7,84.9,47.5);


(lib.state1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#9BC3FB").ss(2).p("AmYDpIAAnHIM7AA");
	this.shape.setTransform(-0.5,-0.5);

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-42.4,-23.7,84.9,47.5);


(lib.Tween3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.hand2("synched",0);
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-10.5,-8.5,21.1,17);


// stage content:
(lib.LZ_website_animation_working_a_V2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// tick
	this.instance = new lib.tick("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(96.8,98.9,1,1,0,0,0,0,0.1);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1).to({regY:0.5,y:99.3},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:1},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({alpha:0.889},0).wait(1).to({alpha:0.778},0).wait(1).to({alpha:0.667},0).wait(1).to({alpha:0.556},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.333},0).wait(1).to({alpha:0.222},0).wait(1).to({alpha:0.111},0).wait(1).to({alpha:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1).to({startPosition:0},0).wait(1));

	// legs
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#9BC3FB").s().p("Al7AnQg1gMgigpQgjgrAAg4QAAhAAuguQAtgtBBAAQAoAAAhASIABAAIMFGUIhCBzg");
	this.shape.setTransform(234.7,298.7,1.336,1.336);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f().s("#9BC3FB").ss(2).p("ACKhZIkEAAIBWC4");
	this.shape_1.setTransform(152.6,230.9,1.336,1.336);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f().s("#656F7A").ss(2.3).p("AFcC5IoYAAIiflxIIYAAg");
	this.shape_2.setTransform(194.1,250.9,1.336,1.336);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("#FFFFFF").s().p("Ai8C5IiflxIIYAAICfFxg");
	this.shape_3.setTransform(194.1,250.9,1.336,1.336);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f().s("#656F7A").ss(1.5,1).p("AiwAAIFhAA");
	this.shape_4.setTransform(241.6,280.5,1.336,1.336);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("#656F7A").s().p("AhkAPIgRgZIAvgrIC8AwIhZA7g");
	this.shape_5.setTransform(257.2,329.9,1.336,1.336);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("#9BC3FB").s().p("Am5EOIJfpaIABAAQAkgpA2gJQA4gKAwAcQA5AgARA/QASA/ggA5QgTAigiAVIAAABIrkHeg");
	this.shape_6.setTransform(300.5,292.5,1.336,1.336);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("#656F7A").s().p("AAeBmIhnjLIA3gcIAkBgIASgGIAmCpg");
	this.shape_7.setTransform(242.2,349.4,1.336,1.336);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_7},{t:this.shape_6},{t:this.shape_5},{t:this.shape_4},{t:this.shape_3},{t:this.shape_2},{t:this.shape_1},{t:this.shape}]}).wait(312));

	// hand
	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FBA0A1").s().p("AAKAxQgbgGgIgdQAAgFgCgDQgEgDgEAGIgRAaQgCAEgFAAQgEgBAAgIQABgNAJgnQACgIAHgIQAOgQAXAGQAFAAAeATIAeATQAKAFgHALIgZArIgLACQgHAAgIgCg");
	this.shape_8.setTransform(243,236.8,1.613,1.613,5.7);

	this.instance_1 = new lib.Tween3("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(242.8,236.9);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween4("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(238.6,250.5,1,1,-15);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_8}]}).to({state:[{t:this.instance_1}]},50).to({state:[{t:this.instance_2}]},15).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},5).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_2}]},4).to({state:[{t:this.instance_2}]},44).to({state:[{t:this.instance_1}]},19).wait(129));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(50).to({_off:false},0).to({_off:true,rotation:-15,x:238.6,y:250.5},15).wait(99).to({_off:false,rotation:0,x:242.8,y:236.9},19).wait(129));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(50).to({_off:false},15).wait(4).to({startPosition:0},0).to({rotation:-30,x:237.9,y:253},4).wait(2).to({startPosition:0},0).to({rotation:-15,x:238.6,y:250.5},4).wait(2).to({startPosition:0},0).to({rotation:-30,x:237.9,y:253},4).wait(2).to({startPosition:0},0).to({rotation:-15,x:238.6,y:250.5},4).wait(5).to({startPosition:0},0).to({rotation:-30,x:237.9,y:253},4).wait(2).to({startPosition:0},0).to({rotation:-15,x:238.6,y:250.5},4).wait(4).to({startPosition:0},0).to({rotation:-30,x:237.9,y:253},4).wait(2).to({startPosition:0},0).to({rotation:-15,x:238.6,y:250.5},4).wait(44).to({startPosition:0},0).to({_off:true,rotation:0,x:242.8,y:236.9},19).wait(129));

	// forearm
	this.instance_3 = new lib.forearm2("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(270,262.8,1,1,0,0,0,0.4,-0.4);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(50).to({startPosition:0},0).to({regY:-0.3,rotation:-15,x:271.6,y:268.5},15).wait(99).to({startPosition:0},0).to({regY:-0.4,rotation:0,x:270,y:262.8},19).wait(129));

	// head
	this.instance_4 = new lib.head2("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(254.5,161.4,1,1,0,0,0,-0.1,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(41).to({startPosition:0},0).to({regY:0.1,rotation:-11.3,x:251.8,y:162.8},13).wait(54).to({startPosition:0},0).to({rotation:11.6,x:253.8,y:160.7},16).wait(48).to({rotation:11.6},0).to({regY:0,rotation:0,x:254.5,y:161.4},12).wait(128));

	// guy background
	this.shape_9 = new cjs.Shape();
	this.shape_9.graphics.f("#FBA0A1").s().p("Ag/ApIAAhSIB/AAIAABSg");
	this.shape_9.setTransform(259.8,175.4,1.407,2.384);

	this.shape_10 = new cjs.Shape();
	this.shape_10.graphics.f("#6AEBDA").s().p("AgoFsQgUAAgPgPQgOgOAAgVIAApzQAAgUAOgPQAPgPAUAAIBQAAQAVAAAOAPQAPAPAAAUIAAJzQAAAVgPAOQgOAPgVAAg");
	this.shape_10.setTransform(306.3,255.3,1.336,1.336);

	this.shape_11 = new cjs.Shape();
	this.shape_11.graphics.f().s("#656F7A").ss(1.5,1,1).p("AAAD+IAAn7");
	this.shape_11.setTransform(293.3,233.4);

	this.shape_12 = new cjs.Shape();
	this.shape_12.graphics.f().s("#656F7A").ss(1.5,1).p("AAAhCIAACF");
	this.shape_12.setTransform(218.9,217.3,1.336,1.336);

	this.shape_13 = new cjs.Shape();
	this.shape_13.graphics.f("#6AEBDA").s().p("AmcG8QgUAAgPgPQgOgOAAgVIAAsTQAAgVAOgOQAPgPAUAAIM5AAQAUAAAPAPQAOAOAAAVIAAMTQAAAVgOAOQgPAPgUAAg");
	this.shape_13.setTransform(256.5,244.6,1.336,1.336);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_13},{t:this.shape_12},{t:this.shape_11},{t:this.shape_10},{t:this.shape_9}]}).wait(312));

	// computer screen
	this.instance_5 = new lib.state1("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(221.1,143.3);

	this.instance_6 = new lib.state2("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(220.1,143.3);

	this.instance_7 = new lib.state3("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(220.1,143.3);

	this.instance_8 = new lib.state4("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(221.1,142.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_5}]}).to({state:[{t:this.instance_5}]},136).to({state:[{t:this.instance_6}]},10).to({state:[{t:this.instance_7}]},9).to({state:[{t:this.instance_8}]},9).to({state:[{t:this.instance_5}]},9).wait(139));

	// tapping shoe
	this.shape_14 = new cjs.Shape();
	this.shape_14.graphics.f("#656F7A").s().p("Ag/g4IAcAFIAhiUIBXAeIhtE/Ig8Ayg");
	this.shape_14.setTransform(422.5,411.7);

	this.instance_9 = new lib.shoe("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(303.5,342.6);
	this.instance_9._off = true;

	this.instance_10 = new lib.Tween5("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(313.1,342.6,1,1,-24);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_14}]}).to({state:[{t:this.instance_9}]},16).to({state:[{t:this.instance_10}]},16).to({state:[{t:this.instance_9}]},14).to({state:[{t:this.instance_9}]},29).to({state:[{t:this.instance_10}]},16).to({state:[{t:this.instance_9}]},14).to({state:[{t:this.instance_9}]},31).to({state:[{t:this.instance_10}]},16).to({state:[{t:this.instance_9}]},14).to({state:[{t:this.instance_9}]},53).to({state:[{t:this.instance_10}]},16).to({state:[{t:this.instance_9}]},14).wait(63));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(16).to({_off:false},0).to({_off:true,rotation:-24,x:313.1},16).to({_off:false,rotation:0,x:303.5},14).wait(29).to({startPosition:0},0).to({_off:true,rotation:-24,x:313.1},16).to({_off:false,rotation:0,x:303.5},14).wait(31).to({startPosition:0},0).to({_off:true,rotation:-24,x:313.1},16).to({_off:false,rotation:0,x:303.5},14).wait(53).to({startPosition:0},0).to({_off:true,rotation:-24,x:313.1},16).to({_off:false,rotation:0,x:303.5},14).wait(63));
	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(16).to({_off:false},16).to({_off:true,rotation:0,x:303.5},14).wait(29).to({_off:false,rotation:-24,x:313.1},16).to({_off:true,rotation:0,x:303.5},14).wait(31).to({_off:false,rotation:-24,x:313.1},16).to({_off:true,rotation:0,x:303.5},14).wait(53).to({_off:false,rotation:-24,x:313.1},16).to({_off:true,rotation:0,x:303.5},14).wait(63));

	// background
	this.shape_15 = new cjs.Shape();
	this.shape_15.graphics.f().s("#9BC3FB").ss(2).p("ADyDLInjAAIAAmVIHjAAg");
	this.shape_15.setTransform(182.2,123.2,1.336,1.336);

	this.shape_16 = new cjs.Shape();
	this.shape_16.graphics.f().s("#656F7A").ss(2).p("AMXAAI4tAA");
	this.shape_16.setTransform(262.7,339.1,1.336,1.336);

	this.shape_17 = new cjs.Shape();
	this.shape_17.graphics.f().s("#656F7A").ss(2).p("AMXAAI4tAA");
	this.shape_17.setTransform(246.9,327.6,1.336,1.336);

	this.shape_18 = new cjs.Shape();
	this.shape_18.graphics.f().s("#9BC3FB").ss(2).p("AAeAeIg7g7");
	this.shape_18.setTransform(316.5,72.6,1.336,1.336);

	this.shape_19 = new cjs.Shape();
	this.shape_19.graphics.f().s("#9BC3FB").ss(2).p("AAegdIg7A7");
	this.shape_19.setTransform(316.5,72.6,1.336,1.336);

	this.shape_20 = new cjs.Shape();
	this.shape_20.graphics.f().s("#6AEBDA").ss(2).p("AAWAAQAAAJgGAGQgHAGgJAAQgIAAgGgGQgGgGAAgJQAAgIAGgGQAGgHAIAAQAJAAAHAHQAGAGAAAIg");
	this.shape_20.setTransform(175.8,73.1,1.336,1.336);

	this.shape_21 = new cjs.Shape();
	this.shape_21.graphics.f().s("#6AEBDA").ss(2).p("AAVAAQAAAJgGAGQgHAGgIAAQgIAAgGgGQgHgGAAgJQAAgIAHgGQAGgHAIAAQAIAAAHAHQAGAGAAAIg");
	this.shape_21.setTransform(164.3,73.1,1.336,1.336);

	this.shape_22 = new cjs.Shape();
	this.shape_22.graphics.f().s("#6AEBDA").ss(2).p("AAWAAQAAAJgHAGQgGAGgJAAQgIAAgGgGQgHgGAAgJQAAgIAHgGQAGgHAIAAQAJAAAGAHQAHAGAAAIg");
	this.shape_22.setTransform(152.8,73.1,1.336,1.336);

	this.shape_23 = new cjs.Shape();
	this.shape_23.graphics.f().s("#656F7A").ss(2).p("AMPPhI4dAAIAA/BIYdAAg");
	this.shape_23.setTransform(234.1,185,1.336,1.336);

	this.shape_24 = new cjs.Shape();
	this.shape_24.graphics.f().s("#9BC3FB").ss(2).p("AjwAAIHhAA");
	this.shape_24.setTransform(359.5,174.9,1.336,1.336);

	this.shape_25 = new cjs.Shape();
	this.shape_25.graphics.f().s("#656F7A").ss(2).p("ADwAAInfAA");
	this.shape_25.setTransform(76.2,165.5,1.336,1.336);

	this.shape_26 = new cjs.Shape();
	this.shape_26.graphics.f().s("#656F7A").ss(2).p("ADwAAInfAA");
	this.shape_26.setTransform(76.2,156.1,1.336,1.336);

	this.shape_27 = new cjs.Shape();
	this.shape_27.graphics.f().s("#656F7A").ss(2).p("AFAAAIp/AA");
	this.shape_27.setTransform(65.5,137.4,1.336,1.336);

	this.shape_28 = new cjs.Shape();
	this.shape_28.graphics.f().s("#9BC3FB").ss(2).p("AiDAAQAAA3AnAmQAnAnA1AAQA2AAAngnQAngnAAg2QAAg2gngnQgngmg2AAQg1AAgnAmQgnAnAAA2g");
	this.shape_28.setTransform(89.8,105.1,1.336,1.336);

	this.shape_29 = new cjs.Shape();
	this.shape_29.graphics.f("#FFFFFF").s().p("AhdBdQgmgmAAg3QAAg2AmgnQAngmA2AAQA2AAAnAmQAnAnAAA2QAAA2gnAnQgnAng2AAQg2AAgngng");
	this.shape_29.setTransform(89.8,105.1,1.336,1.336);

	this.shape_30 = new cjs.Shape();
	this.shape_30.graphics.f("#CDF9F3").s().p("AhbBcQgmgnAAg1QAAg1AmgmQAmgmA1AAQA2AAAmAmQAmAmAAA1QAAA1gmAnQgmAmg2AAQg1AAgmgmg");
	this.shape_30.setTransform(17.4,137.4,1.336,1.336);

	this.shape_31 = new cjs.Shape();
	this.shape_31.graphics.f("#CDF9F3").s().p("AgpAqQgSgRAAgZQAAgYASgSQARgRAYAAQAZAAARARQASASAAAYQAAAZgSARQgRASgZAAQgYAAgRgSg");
	this.shape_31.setTransform(52.1,116,1.336,1.336);

	this.shape_32 = new cjs.Shape();
	this.shape_32.graphics.f().s("#9BC3FB").ss(2).p("AAAGBQifAAhwhxQhxhxAAifQAAieBxhxQBwhxCfAAQCfAABxBxQBxBxAACe");
	this.shape_32.setTransform(103.2,52.4,1.336,1.336);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.shape_32},{t:this.shape_31},{t:this.shape_30},{t:this.shape_29},{t:this.shape_28},{t:this.shape_27},{t:this.shape_26},{t:this.shape_25},{t:this.shape_24},{t:this.shape_23},{t:this.shape_22},{t:this.shape_21},{t:this.shape_20},{t:this.shape_19},{t:this.shape_18},{t:this.shape_17},{t:this.shape_16},{t:this.shape_15}]}).wait(312));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(196,184,392.7,366.8);
// library properties:
lib.properties = {
	id: '3E67C8724EC9435E949B02122DB73507',
	width: 392,
	height: 368,
	fps: 24,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['3E67C8724EC9435E949B02122DB73507'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, AdobeAn = AdobeAn||{});
var createjs, AdobeAn;