<?php /* Template Name: Water */

get_header();

?>

<section class="title">


        <div class="row blue">

                <h1>

                        <?php echo the_title(); ?>

                </h1>

        </div>

</section>

        <section class="intro">

                <div class="row">
                        <table>
                                <tr>
                                        <td>
                                                <!-- <div class="col-md-6"> -->
                                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/Audacity-Water-resistant-72h.jpg">

                                                <!-- </div> -->
                                        </td>

                                        <td id="parallax-id-1">
                                        <!-- <div class="col-md-6"> -->
                                                <div class="inner">
                                                        <h2 class="black">
                                                        <!-- WATER RESISTANT -->
                                                        <?php the_field('water_intro_heading'); ?>

                                                        </h2>
                                                        <p>
                                                        <!-- With Audacity we've solved the main issue of natural,
                                                                <br/> wood-based flooring - moisture. The flooring stands up
                                                                <br/> to spills and keeps the moisture out long enough for you
                                                                <br/> to be able to clean it up. -->
                                                                <?php the_field('water_intro_content'); ?>

                                                                </p>
                                                </div>
                                                <!-- </div> -->
                                        </td>
                                </tr>
                        </table>
                </div>
        </section>

<div id="parallax-id-2-5">
        <section class="full purple">
                <div class="row">

                        <div class="col-md-12">
                                <h2>
                                <?php the_field('water_break_heading'); ?>
                                        <!-- <br/>WATER RESISTANT? -->
                                        </h2>
                                <p>
                                <?php the_field('water_break_content'); ?>

                                <!-- Audacity features an extra stable high-density core that is protected against moisture
                                        <br/> by a specially developed, multi-layer sealer, that we apply to the edges of a board.
                                        <br/> We also use a click system with additional contact points that allow for an extra
                                        tight
                                        <br/> and strong engagement, keeping water from penetrating the edges. -->
                                </p>
                        </div>
                </div>
        </section>
</div>

<div id="parallax-id-3">

<section class="construction">

        <h2 class="black">
                <!-- CONSTRUCTION -->
                <?php the_field('water_plank_heading'); ?>

        </h2>

        <div class="row">
                <div class="col-md-6">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/plank.jpg">

                </div>
                <div class="col-md-6">
                        <table>
                                <tbody>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/16.svg">
                                                </td>
                                                <td>
                                                        <h4> 
                                                        <?php the_field('water_plank_01'); ?>
                                                        </h4>


                                                        <p>
                                                        <?php the_field('water_plank_01_content'); ?>
                                                        </p>
                                                </td>
                                        </tr>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/18.svg">
                                                </td>
                                                <td>

                                                        <h4>
                                                        <?php the_field('water_plank_02'); ?>

                                                        </h4>


                                                        <p>
                                                        <?php the_field('water_plank_02_content'); ?>

                                                        </p>
                                                </td>

                                        </tr>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/10.svg">
                                                </td>
                                                <td>

                                                        <h4>
                                                        <?php the_field('water_plank_03'); ?>

                                                        </h4>


                                                        <p>
                                                        <?php the_field('water_plank_03_content'); ?>

                                                        </p>
                                                </td>

                                        </tr>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/11.svg">
                                                </td>
                                                <td>

                                                        <h4>
                                                        <?php the_field('water_plank_04'); ?>
                                                                </h4>


                                                        <p>
                                                        <?php the_field('water_plank_04_content'); ?>

                                                        </p>
                                                </td>

                                        </tr>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/12.svg">
                                                </td>
                                                <td>

                                                        <h4>
                                                        <?php the_field('water_plank_05'); ?>
                                                        </h4>


                                                        <p>
                                                        <?php the_field('water_plank_05_content'); ?>

                                                        </p>
                                                </td>

                                        </tr>
                                        <tr>
                                                <td valign="middle">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/05.svg">
                                                </td>
                                                <td>

                                                        <h4>
                                                        <?php the_field('water_plank_06'); ?>

                                                        </h4>


                                                        <p>
                                                        <?php the_field('water_plank_06_content'); ?>

                                                        </p>
                                                </td>

                                        </tr>
                                </tbody>
                        </table>
                </div>
        </div>
</section>
</div>
<?php //echo do_shortcode('[get_link_section]') ?>
<div id="new_contact" style="padding:60px 0px" class="row">

<style>
form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:0px 0px 0px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER<br/>
    <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.</h2> -->

   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>


            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>


<?php get_footer(); ?>