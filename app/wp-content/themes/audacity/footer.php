<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legranddigital
 */
//$allowedFooter = apply_filters( 'get_allowed_footer','get_allowed_footer' );
?>


<div id="contact-other" class="container-fluid cta-trigger contact-popup" style="background:#43bbbe;padding:50px 0px">

  <i style="color:white;float:right;text-align:right;position: relative; top:20px;font-size:30px" class="fa fa-times popup-close"></i>
    <?php// echo do_shortcode('[get_link_section]') ?>

  <h2 style="text-align:center;color:white;line-height:35px">Looking for a local retailer?<br/>
     <?php if( get_field('store_cta','option') ): ?>

   <a style="
       background: #0a213c;
    color: white;
    padding: 5px 30px;
    margin-top:15px;
    color: white;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store"><?php the_field('store_cta','option');?></a>

<?php endif; ?>
Have some questions?<br/>
Get in touch !<br/>
</h2>

<?php echo do_shortcode( '[gravityform id=2 title=false description=false ajax=true ]
' ); ?>

  </div>

    <footer>
        <a href="#" class="scrolltop">
            <i class="fa fa-angle-up"></i>
        </a>
        <div class="container-fluid">

            <div class="row">
                  <div class="col-lg-6"> <p>
<?php the_field('footer-left','option'); ?>
</p>
    </div>
    <div class="col-lg-2 empty">
</div>
    <div class="col-lg-4">
<!--         <img src="<?php bloginfo('template_directory'); ?>/assets/img/armstong-logo.png">
 -->        


       <a target="_blank" href="<?php the_field('link_img','option'); ?>
">
        <img src="<?php the_field('footer_img','option'); ?>">
</a>

<?php if( get_field('image_footer','option') ): ?>

    <img src="<?php the_field('image_footer'); ?>" />

<?php endif; ?>


<p>
        <?php the_field('footer_right','option'); ?>
        </p>
        </div>
                    </div>
            </div>
        </div>
    </footer>

<a href="javascript:void(0)" class="cta-mobile"> 

<!-- <img src="https://www.firmfitfloor.com/wp-content/uploads/2018/05/cta-crop-2.gif" style="
    width: 75%;"> -->
<!--     <span class="text_cta">
Any questions ? <br/>

Contact us !<br/>
</span> -->
<i class="fa fa-envelope"></i>

  </a>

<div class="cta">

Any questions ? <br/>
Contact us !<br/>
<i class="fa fa-envelope"></i>

  </div>

  <style>

  a.cta-mobile{
    display:none;
  }

      .contact-popup{
        display:none;
      }

@media all and (max-width:768px) {

  .layer .button-container-desktop {
    margin-top:0px !important;
  }

  .gform_wrapper textarea.medium{
    height:180px !important;
    line-height: 220px;
  }

  a.cta-mobile i.fa.fa-envelope {
    position: relative;
    background: #43bbbe;
    border-radius: 0px !important;
    padding: 10px 20px !important;
    float: left;
    width: 20%;
    display: inline-block;
    color: white ;
    font-size:40px;
}

a.cta-mobile p {
    margin: 20px 0px;
    width: 80%;
    float: right;
    position: relative;
        color: white ;

}

  a.cta-mobile{
        display: block !important;
    background: #43bbbe !important;
    box-shadow: none !important;
    bottom: 0px !important;
    top: initial !important;
    z-index: 999 !important;
    position: fixed !important;
    width: 100%;
    padding: 0px !important;
    border-radius: 0px !important;
  }

        .contact-popup{
            overflow-x: hidden;
            overflow-y: scroll;
    height: 100% !important;
        top:0px !important;
    position: fixed;
    top: 0px;
    background: #43bbbe;
    z-index: 9999999;
        padding: 50px 20px 50px 20px !important;
}

.popup-inner{
        position: relative !important;
        display: block;
}


    #cookie-notice{
        display: none !important;
    }
  }

  .gform_wrapper{
    display: block !important
  }

  #new_form h2{
    color: black !important;
  }

  #new_form h2 a {
   background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto
  }

.cta{
      color: white;
    text-align: center;
    padding: 15px 30px;
    background: #33257c;
    box-shadow: -2px 7px 17px 1px rgba(0,0,0,0.3);
    position: fixed;
    top: 200px;
    line-height: 24px;
    z-index: 99;
    right: 0;
    cursor: pointer;
}

#cookie-law-info-bar{
    font-size: 12px !important;
    padding:6px 0 !important;
}

  </style>

    <?php wp_footer(); ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-113411176-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-113411176-1');
</script>


    </body>

    </html>
