<?php get_header()/* Template Name: Home */?>

<style>


/*.cta{
    display:none
}
*/
</style>

<div class="section header-video video example-classname" id="section0">
    <?php get_header('home'); ?>

    <div class="container-fluid">
    <h1 class="showmob" data-mobile-font-size="delta">
                The Fearless Water Resistant
                <br/>Laminate Flooring 
            </h1>
        <video id="homeVid" loop muted autoplay style="background-size:100% !important;background-repeat:no-repeat !important;background: url('<?php echo $video['background-video-preview']['url'] ?>' );">
            <source src="<?php bloginfo('template_directory'); ?>/assets/videos/video-short.mp4" type="video/mp4">
        </video>

        <img class="hide" src="<?php bloginfo('template_directory'); ?>/assets/img/mobile-intro.jpg">
        <i class="fa fa-play-circle-o">               
        <a href="#video-modal" rel="modal:open"></a>
</i>
        <div class="layer">
            <!-- <h1 class="desktop" data-mobile-font-size="delta">
                The Fearless Water Resistant
                <br/>Laminate Flooring 
            </h1> -->
            <p class="delta" data-mobile-font-size="epsilon">
         Audacity is the ultimate laminate flooring, designed to be water-resistant,<br/>
extra durable and easy to maintain. Featuring latest flooring designs and<br/>
beautiful Embossed-In-Register surface for an authentic wood feel<br/><br/>
            </p>
            <div class="button-container-desktop center trigger">
                <!-- <a href="#video-modal" rel="modal:open" class="button button-primary ">Play the film</a> -->
                <a href="#video-modal" rel="modal:open" class="button button-primary "><i class="fa fa-play-circle-o"></i></a>

            </div>
        </div>
    </div>
</div>

<div class="section specificities example-classname" id="section1">

    <div class="row blue">

        <h2>
        <?php the_field('section_1_heading'); ?>

      <!-- WHY YOU'LL LOVE AUDACITY -->
        </h2>

        <p class="delta" data-mobile-font-size="epsilon">
         <!-- Audacity is the ultimate laminate flooring, designed to be water-resistant,<br/>
extra durable and easy to maintain. Featuring latest flooring designs and<br/>
beautiful Embossed-In-Register surface for an authentic wood feel -->
<?php the_field('section_1_content'); ?>

            </p>

    </div>
    <div class="accordion">
        <ul>
            <li>
                <h2>
                <?php the_field('section_1_slide_heading'); ?>

                
                <!-- WATER RESISTANT -->
                 </h2>
                <div class="fade-content">
                    <p>
                    <?php the_field('section_1_slide_content'); ?>

                    <!-- With Audacity we've solved the main issue of natural, <br/>
                    wood-based flooring - moisture. The flooring stands up <br/>
                    to spills and keeps the moisture out long enough for you <br/>
                    to be able to clean it up. -->
                    </p>
                    <div class="button-container-desktop center">
                        <a href="<?php echo get_home_url(); ?>/water-resistant" class="button button-primary">Learn more</a>
                    </div>
                </div>

            </li>
            <li>
                <h2>
                <?php the_field('section_2_slide_heading'); ?>

                <!-- IMPRESSIVE
                    <br/>PERFORMANCE -->
                    </h2>
                <div class="fade-content">
                    <p>
                    <?php the_field('section_2_slide_content'); ?>

                   <!-- Audacity is as durable as it is beautiful.<br/>
Unlike with standard vinyl flooring, with Audacity you<br/>
don't have to worry about all the little scares when your kids are messing around.<br/>
The flooring is extra stain, scratch and impact resistant ! -->
                    </p>
                    <div class="button-container-desktop center">
                        <a href="<?php echo get_home_url(); ?>/impressive" class="button button-primary">Learn more</a>
                    </div>
                </div>
            </li>
            <li>

                <h2>
                <?php the_field('section_3_slide_heading'); ?>

                <!-- EASY
                    <br/> LIVING -->
                    </h2>
                <div class="fade-content">
                    <p>
                    <?php the_field('section_3_slide_content'); ?>

                    <!-- Audacity flooring features an extra stain resistant wear<br/>
                    layer, plus it can be wet-mopped. All of which means<br/>
                    it’s really easy to keep your flooring nice and clean! -->
                    </p>
                    <div class="button-container-desktop center">
                        <a href="<?php echo get_home_url(); ?>/easy-living/" class="button button-primary">Learn more</a>
                    </div>
                </div>

            </li>

        </ul>
    </div>

</div>

<div class="section collections example-classname" id="section2">

    <div class="row blue">

        <h2>

            <!-- LAMINATE REINVENTED
            <br/> TO AMAZE -->
            <?php the_field('section_3_title'); ?>

        </h2>
    </div>

    <div class="collections-slider">

 <!-- <article>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/cat1.jpg">
           <div class="inner-title"> <h2>Vintage</h2>
           <a class="button button-primary" href="/category/classic-naturals/">Discover floor</a>
           </div>
        </article>

         <article>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/earthside.jpg">
                      <div class="inner-title">  <h2>Hearthside</h2>
                      <a class="button button-primary" href="category/heatherside/">Discover floor</a>


  </div>                  </article>

        <article>
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/lodge.jpg">
                       <div class="inner-title"> <h2>Lodge</h2>
                       <a class="button button-primary" href="/category/lodge/">Discover floor</a>
                       </div>

        </article>>

        <article>
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/classic-naturals.jpg">
                   <div class="inner-title"> <h2>Classic Naturals</h2>
                   <a class="button button-primary" href="/category/aspiration/">Discover floor</a>

    </div></article>-->
    <?php //echo do_shortcode( '[get_home_category]'); ?>

     <article>
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/all-collections.jpg">
                   <div class="inner-title"> 
<!--                     <h2>Classic Naturals</h2>
 -->                   <a class="button button-primary" href="<?php echo get_home_url(); ?>/collections/">Discover collections</a>

    </div></article>


    </div>

</div>

<div class="section support example-classname normal-scroll" sectionName="support" id="section3">

    <div class="row blue">

        <h2>
            <!-- SUPPORT -->
            <?php the_field('section_4_title'); ?>

        </h2>
    </div>

    <span class="contact">

 <h2 class="contact" style="text-align:center;color:black;clear:both;margin-top:50px;">
 <?php the_field('contact_section','option');?>
</h2>
</span>

<div class="retailer-locator">
                <?php //echo do_shortcode('[get_link_section]') ?>
</div>

    <div class="row">
    <div class="col-lg-6 col-md-12 col-sm-12 first">            <div class="row">
                <div class="col-md-6">

               <?php 

$file = get_field('installation_manual','option');

if( $file ): ?> 
                    <a target="_blank" href="<?php echo $file['url']; ?>">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/installation.jpg">
                    <h3> 
                    <?php the_field('installation_manual'); ?>

                    <!-- Installation manual -->
                    </h3>
</a>

<?php endif; ?>


                </div>

                <div class="col-md-6">
                    <a href="<?php echo get_home_url(); ?>/faq">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/FAQ.jpg">
                    <h3> 
                                <?php the_field('faq_titlte'); ?>

                    <!-- FAQ -->
                    </h3>
</a>
                </div>

                <div class="col-md-6 colmargin">

                <?php 

$file = get_field('maintenance','option');

if( $file ): ?>

                    <a target="_blank" href="<?php echo $file['url']; ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/maintenance.jpg">
                    <h3> 
                                <?php the_field('maintenance_title'); ?>

                    <!-- Maintenance -->
                    </h3>
</a>

<?php endif; ?>


                </div>

                <div class="col-md-6 colmargin">
                <a href="#warranty-modal" rel="modal:open">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/img/warranty.jpg">
                    <h3> 

                                <?php the_field('warranties_title'); ?>

                    <!-- Warranties -->
                    </h3>


                    </a>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12 second">
            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
    </div>

    <?php get_footer('home'); ?>
</div>

<div id="warranty-modal" class="modal">

<div class="row blue">

<h2>
    CHOOSE THE TYPE OF WARRANTY
</h2>
</div>

<?php 

$file = get_field('residential_warranty', 'option');

if( $file ): ?>

<a class="residential" target="_blank" href="<?php echo $file['url']; ?>">
<!-- Residential Warranty  -->
                                <?php the_field('warranty_01'); ?>

<i class="fa fa-chevron-down"></i></a>

<?php endif; ?>


<?php 

$file = get_field('commercial_warranty', 'option');

if( $file ): ?>

<a class="commerical" target="_blank" href="<?php echo $file['url']; ?>">
<!-- Commercial Warranty  -->
                                <?php the_field('warranty_02'); ?>

<i class="fa fa-chevron-down"></i></a>

<?php endif; ?>

</div>

<div id="video-modal" class="modal">
<?php echo do_shortcode('[video width="960" src="https://audacityflooring.com/wp-content/themes/audacity/assets/videos/video-long.mp4"]'); ?>
</div>

<style>


.link_to_store a{
    top:-30px;
}
.link_to_store h2{
    display: none !important;
}

body.home .collections .collections-slider .slick-slide {
    margin: 0px !important;
}

</style>

<?php get_footer(); ?>
