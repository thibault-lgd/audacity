<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legranddigital
 */

?>
	<!DOCTYPE html>
	<html <?php language_attributes(); ?>>

	<head>

		<meta http-equiv="x-ua-compatible" content="IE=edge">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico" type="image/x-icon" />
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link href="/style/vendor/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
		    crossorigin="anonymous">
		<!-- <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"> -->
		<!-- <link href="//fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet"> -->
		<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
		<!-- <script src="wp-content/themes/audacity/js/vendor/scrolloverflow.min.js"></script> -->


		<?php wp_head(); ?>

	</head>

	<body <?php body_class(); ?>>


		<div id="page" class="site">

			<header id="masthead" class="site-header" role="banner">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="logo">
								<a href="<?php echo get_home_url(); ?>">
									<img width="200px" class="desktop" src="<?php bloginfo('template_directory'); ?>/assets/holding/audacity-logo.png" alt=" logo"
									/>
								</a>
							
							</div>
						</div>


			</header>
			<!-- #masthead -->

			<main id="content" class="site-content" role="main">