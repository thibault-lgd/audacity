<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legranddigital
 */
//$allowedFooter = apply_filters( 'get_allowed_footer','get_allowed_footer' );
?>


    <footer>
        <a href="#" class="scrolltop">
            <i class="fa fa-angle-up"></i>
        </a>
        <div class="container-fluid">

            <div class="row">
                  <div class="col-lg-6"> <p>
<?php the_field('footer-left','option'); ?>
</p>
    </div>
    <div class="col-lg-2 empty">
</div>
    <div class="col-lg-4">
<!--         <img src="<?php bloginfo('template_directory'); ?>/assets/img/armstong-logo.png">
 -->        


       <a target="_blank" href="<?php the_field('link_img','option'); ?>
">
        <img src="<?php the_field('footer_img','option'); ?>">
</a>

<?php if( get_field('image_footer','option') ): ?>

    <img src="<?php the_field('image_footer'); ?>" />

<?php endif; ?>


<p>
        <?php the_field('footer_right','option'); ?>
        </p>
        </div>
                    </div>
            </div>
        </div>
    </footer>

<div class="cta">

Any questions ? <br/>
Contact us !<br/>
<i class="fa fa-envelope"></i>

  </div>

  <style>

.cta{
      color: white;
    text-align: center;
    padding: 15px 30px;
    background: #33257c;
    box-shadow: -2px 7px 17px 1px rgba(0,0,0,0.3);
    position: fixed;
    top: 200px;
    line-height: 24px;
    z-index: 99;
    right: 0;
    cursor: pointer;
}

#cookie-law-info-bar{
    font-size: 12px !important;
    padding:6px 0 !important;
}

  </style>

    <?php wp_footer(); ?>

    <!-- Google Analytics -->
    <!-- <script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-105840324-1', 'auto');
ga('send', 'pageview');
</script> -->
    <!-- End Google Analytics -->

    </body>

    </html>
