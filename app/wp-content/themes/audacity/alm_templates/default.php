
<?php
    $landing_page = get_sub_field('featured_post');
	$featured_post = get_sub_field('featured_post');
	
    ?>

<?php
   $landing_page = get_field('landing_page'); ?>	
   

						<article <?php if (! has_post_thumbnail() ) { echo ' class="no-img"'; } ?>
						class="vertical-center <?php $terms = get_the_terms( $post->ID , 'featured' ); 
                    foreach ( $terms as $term ) {
                        $term_link = get_term_link( $term, 'featured' );
                        if( is_wp_error( $term_link ) )
                        continue;
                    echo '' . $term->name . '';
                    } 
				?>">
				   <a href="<?php the_permalink(); ?>">

<?php
   	if ( has_post_thumbnail() ) {
   		the_post_thumbnail('full');
   	}
	?>
	
	<img class="full" src="<?php echo $landing_page['big_featured_image']['url']; ?>" alt="<?php echo $landing_page['big_featured_image']['alt']; ?>" />

	
	<div class="info-wrapper">
	

	<h3  data-mobile-font-size="delta"><?php the_title(); ?></h3>
	<?php the_excerpt(); ?>
	<?php the_category(); ?>
</div>

</a>


</article>
<?php ; ?>


<?php ?>



