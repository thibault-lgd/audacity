<?php

/*
Plugin Name: WP_List_Table Class Example
Plugin URI: http://sitepoint.com
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Agbonghama Collins
Author URI:  http://w3guy.com
*/

if ( ! class_exists( 'WP_List_Table' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class Postcode_Group_List extends WP_List_Table {

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'Postcode Group', 'sp' ), //singular name of the listed records
			'plural'   => __( 'Postcode Groups', 'sp' ), //plural name of the listed records
			'ajax'     => false //does this table support ajax?
		] );

	}


	/**
	 * Retrieve customers data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_customers( $per_page = 5, $page_number = 1 ) {

		global $wpdb;
		
		$sql = "SELECT * FROM wp_postcodegroup ";
				
		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$do_search .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$do_search .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}else{
			$do_search .= ' ORDER BY postcode_group_name desc ';
		}
		
		$sql .= $do_search;
		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

		$result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}


	/**
	 * Delete a customer record.
	 *
	 * @param int $id customer ID
	 */
	public static function delete_customer( $id ,$status ) {
		global $wpdb;
		if($status == "inactive"){
			$query = "update wp_postcodegroup set is_active=0 where ID =".$id;
			$wpdb->get_results($query);
		}else if($status == "active"){
			$query = "update wp_postcodegroup set is_active=1 where ID =".$id;
			$wpdb->get_results($query);
		}else if($status == "delete"){
			$query = "delete from wp_postcodegroup where ID =".$id;
			$wpdb->get_results($query);
		}
	}


	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;
		$countQuery = "SELECT count(*) FROM wp_postcodegroup";
		return $wpdb->get_var( $countQuery );
	}


	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No postcode avaliable.', 'sp' );
	}


	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
	public function column_default( $item, $column_name ) {
		switch ( $column_name ) {
			case 'ID':
				return $item[ $column_name ];
			case 'postcode_group_name':
				return $this->column_name($item);
			case 'is_active':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
		);
	}


	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) {

		$delete_nonce = wp_create_nonce( 'sp_delete_ads' );
		$trash_nonce = wp_create_nonce( 'sp_pending_ads' );

		$title = '<strong>' . $item['postcode_group_name'] . '</strong>';

		
		$actions=array(
			'edit'=>sprintf('<a href="?page=postcode_group_form&action=%s&ID=%s">Edit</a>', 'edit', $item["ID"])			
        );
		if($item["is_active"] == 1){
			$actions['pending']= sprintf('<a href="?page=postcode_group_listing&action=%s&ID=%s">In Active</a>', 'inactive', $item["ID"]);
		}else{
			$actions['pending']= sprintf('<a href="?page=postcode_group_listing&action=%s&ID=%s">Active</a>', 'active', $item["ID"]);
		}
		$actions['delete']= sprintf('<a href="?page=postcode_group_listing&action=%s&ID=%s">Delete</a>', 'delete', $item["ID"]);
		return $title . $this->row_actions( $actions );
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'ID'    => __( 'ID', 'sp' ),
			'postcode_group_name'    => __( 'Postcode Group Name', 'sp' ),
			'is_active'    => __( 'Is Active', 'sp' )
		];

		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'ID' => array( 'post_id', true ),
			'postcode_group_name' => array( 'postcode_group_name', true )
		);

		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete',
			'bulk-inactive' => 'In Active',
			'bulk-active' => 'Active'
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {

		$this->_column_headers = $this->get_column_info();

		
		/** Process bulk action */
		$this->process_bulk_action();

		# >>>> Pagination
		$per_page     = $this->get_items_per_page( 'ads_per_page', 20 );
		$current_page = $this->get_pagenum();
		$total_items  = self::record_count();
		$this->set_pagination_args( array (
				'total_items' => $total_items,
				'per_page'    => $per_page,
				'total_pages' => ceil( $total_items / $per_page )
		) );
		$last_post = $current_page * $per_page;
		$first_post = $last_post - $per_page + 1;
		$last_post > $total_items AND $last_post = $total_items;
		
		// Setup the range of keys/indizes that contain
		// the posts on the currently displayed page(d).
		// Flip keys with values as the range outputs the range in the values.
		$range = array_flip( range( $first_post - 1, $last_post - 1, 1 ) );
		
		// Filter out the posts we're not displaying on the current page.
		//$posts_array = array_intersect_key( $posts, $range );
		# <<<< Pagination
		
		$this->items = self::get_customers( $per_page, $current_page );
	}

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			
				self::delete_customer( absint( $_GET['ID']),"delete" );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}else if ( 'inactive' === $this->current_action() ) {

			
				self::delete_customer( absint($_GET['ID']),$this->current_action() );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}else if ( 'active' === $this->current_action() ) {

			
				self::delete_customer( absint($_GET['ID']),$this->current_action() );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_customer( $id ,"delete");

			}

			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		        // add_query_arg() return the current url
		        //wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}else if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-inactive' )
					|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-inactive' )
					) {
			$delete_ids = esc_sql( $_POST['bulk-delete'] );
			
			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_customer( $id ,"inactive");
			
			}
			
			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
			// add_query_arg() return the current url
			//wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}else if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-active' )
					|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-active' )
					) {
			$delete_ids = esc_sql( $_POST['bulk-delete'] );
			
			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_customer( $id ,"active");
			
			}
			
			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
			// add_query_arg() return the current url
			//wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}
	}
}


class PostCodeGroup_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'Postcode Group Name',
			'Postcode Group List',
			'manage_options',
			'postcode_group_listing',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		?>
		<div class="wrap">
			<h2>Postcode Group Listing</h2>

			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-1">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<?php
								$this->customers_obj->prepare_items();
								$this->customers_obj->display(); ?>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Postcode Group List',
			'default' => 5,
			'option'  => 'ads_per_page'
		];

		add_screen_option( $option, $args );

		$this->customers_obj = new Postcode_Group_List();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}
