<?php /* Template Name: Archives */

get_header();

// $termSearch =  array();
// $allowedCategory = apply_filters( 'get_allowed_category','category' );
// if(!empty($allowedCategory)){
//     $termSearch["include"]=$allowedCategory;
// }


?>
<!-- 
<section class="title">


	<div class="row blue">

		<h1>

		<?php foreach((get_the_category()) as $category) { echo $category->cat_name . ' '; } ?>
		</h1>

	</div>

</section>

<section class="collections container-fluid">

	<div class="row">
		<table>
			<tr>
				<td>
					<img src="<?php bloginfo('template_directory'); ?>/assets/img/placeholder.png">

				</td>
				<td>
					<div class="inner-center">

						<h2 class="black"> Laminate reinvented to amaze</h2>
						<p>
							<br/> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							<br/> Proin consectetur velit in neque placerat sollicitudin.
							<br/> Pellentesque sed malesuada massa, ut bibendum ante.
						</p>
					</div>
				</td>
			</tr>
		</table>
	</div>

</section> -->

    <section id="cat_desc" style="clear:both">
        <div class="row">
            <div class="col-md-3">
            </div>
            <div class="col-md-6 col-sm-12">
                
<p>
        <?php the_field('cta_description','option'); ?>

    </p>

                <a class="cat_desc_cta" target="_blank" href="<?php the_field('cta_link','option'); ?>">   <?php the_field('cta_text','option'); ?>
</a>

                <p>
                    <?php 
                 //   echo $catDes;?>
                </p>
          <!--       <?php if(count($allowedCategoryDescription)>0){
                        if(!empty($allowedCategoryDescription[0]->category_description_link)){
                            $link = $allowedCategoryDescription[0]->category_description_link;
                            if(!empty($allowedCategoryDescription[0]->category_description_link_title)){
                                echo '<a class="cat_desc_cta" target="_blank" href="'.$link.'">'.$allowedCategoryDescription[0]->category_description_link_title.'</a>';
                            }else{
                                echo '<a class="cat_desc_cta" target="_blank" href="'.$link.'">shop audacity</a>';
                            }
                        }
                    }
                ?> -->
            </div>
            <div class="col-md-3">
            </div>
        </div>
    </section>

<section id="grid">

<div class="wrap">
<div class="tag-filter">
<ul class="categories">
<li><a href="<?php echo esc_url( home_url( '/collections' ) ); ?>">Show All</a></li>
<?php wp_list_categories('title_li='); ?>
</ul>


<!-- 
<select name="event-dropdown" id="cat" onchange='document.location.href=this.options[this.selectedIndex].value;'> 
<option value=""><?php echo esc_attr(__('Select Category')); ?></option>  -->

<?php 
// $option = '<option value="' . get_option('home') . '/blog/">All Categories</option>'; // change category to your custom page slug
// $categories = get_categories(); 
// foreach ($categories as $category) {
// $option .= '<option value="'.get_option('home').'/category/'.$category->slug.'">';
// $option .= $category->cat_name;
// $option .= ' ('.$category->category_count.')';
// $option .= '</option>';
// }
// echo $option;
?>
</select>


</div>

        
</div>


<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post',
'post_status'=>'publish', 'posts_per_page'=>-1)); ?>

		<?php if ( $wpb_all_query->have_posts() ) : ?>

		<ul class="products">

			<!-- the loop -->
			 <?php //while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); //?> 

             <?php
global $post;
$category = get_the_category($post->ID);
$category = $category[0]->cat_ID;
$myposts = get_posts(array('numberposts' => 5, 'offset' => 0, 'category__in' => array($category), 'post_status'=>'publish', 'order'=>'ASC' ));
foreach($myposts as $post) :
setup_postdata($post);
?>
			<?php
	$landing_page = get_sub_field('featured_post');
	$featured_post = get_sub_field('featured_post');

	?>

				<?php
   $landing_page = get_field('landing_page'); ?>

					<article class="col-md-3" <?php if (! has_post_thumbnail() ) ?>>
						<a href="<?php the_permalink(); ?>">

							<?php  if ( has_post_thumbnail() ) {
		  					 the_post_thumbnail('full');}?>
							
							<h2><?php echo the_title(); ?></h2>

						</a>

					</article>
                    <?php endforeach; ?>
                    <?php wp_reset_query(); ?>
					<!-- end of the loop -->

		</ul>

		<?php wp_reset_postdata(); ?>


		<?php else : ?>
		<p>
			<?php _e( 'Sorry, no posts matched your criteria.' ); ?>
		</p>
		<?php endif; ?>


    </section>
<?php //echo do_shortcode('[get_link_section]') ?>
<div id="new_contact" style="padding:60px 0px" class="row">

<style>
form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}

#cat_desc{
	margin-top:25px;
	display:block;
	position: relative;
}

#cat_desc .col-md-6.col-sm-12{
	padding:25px;
	border:1px solid #33257c;
}

a.cat_desc_cta{
	background: #43bbbe;
	color: white;
	padding:10px 20px;
	    margin: 0 auto;
    position: relative;
    display: block;
    text-align: center;
    width: fit-content;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:0px 0px 0px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER <br/>
 <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.</h2> -->

   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>

            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>



    <?php get_footer('collections'); ?>
