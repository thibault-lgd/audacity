<?php /* Template Name: Impressivenotworking */

get_header();

?>



<section class="title">


        <div class="row blue">

                <h1>

                        <?php echo the_title(); ?>

                </h1>

        </div>

</section>

<section class="intro">

        <div class="row">
                <table>
                        <tr>
                                <td>
                                        <!-- <div class="col-md-6"> -->
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/impressive.jpg">

                                        <!-- </div> -->
                                </td>

                                <td>
                                        <table>
                                                <tr>
                                                        <td id="parallax-id-1">
                                                                <!-- <div class="col-md-6"> -->
                                                                <div class="inner">
                                                                        <h2 class="black">
                                                                        <?php //the_field('impressive_intro_heading'); ?>
                                                                                <!-- LARGE AREA
                                                                                <br/> INSTALLATION -->
                                                                        </h2>
                                                                        <p>
                                                                                <!-- Thanks to Audacity's extra stable core, the flooring
                                                                                <br/> can be installed over areas of up to 4000sf
                                                                                / 400sqm
                                                                                <br/> with no transitions even in between rooms
                                                                                and areas.
                                                                                <br/> That's 300% larger compared with standard
                                                                                laminate
                                                                                <br/> or vinyl flooring.  -->

                                                                      <?php the_field('impressive_intro_content'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                                                </p>
                                                                </div>

                                                        </td>
                                                        <td id="parallax-id-2" class="icon" valign="middle">
                                                                <img src="<?php bloginfo('template_directory'); ?>/assets/icons/02.png">
                                                        </td>
                                                </tr>
                                        </table>
                                        <!-- </div> -->
                                </td>
                                <!-- <td>

</td> -->
                        </tr>
                </table>
        </div>
</section>

<div id="parallax-id-2-5">

        <section class="full blue">
                <div class="container">
                        <div class="row">
                                <div class="col-md-offset-3 col-md-3"></div>
                                <div class="col-md-6">
                                        <h2>
                                                <!-- KIDS & PET-FRIENDLY -->
                                                <?php the_field('impressive_break_heading'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                </h2>
                                        <p>
                                        <?php the_field('impressive_break_content'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                <!-- Audacity is as durable as it is beautiful. Unlike with standard
                                                <br/> vinyl flooring, with Audacity you don't have to worry about
                                                <br/> all the little scares when your kids are messing around.
                                                <br/> The flooring is extra stain, scratch and impact resistant -->
                                                </p>
                                </div>

                        </div>
                        <br/>
                        <br/>
                        <div class="row grid">
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/03.svg">
                                        <p>
                                        <?php the_field('impressive_icon_01_'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>
                                        </p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/06.svg">
                                        <p>
                                        <?php the_field('impressive_icon_02'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>
                                        </p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/07.svg">
                                        <p>
                                        <?php the_field('impressive_icon_03'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>
                                        </p>

                                </div>
                                <div class="col-lg-3 col-md-6 col-sm-6">
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/last.svg">
                                        <p>
                                        <?php the_field('impressive_icon_04'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>
                                        </p>

                                </div>
                        </div>
                </div>
        </section>
</div>

<div id="parallax-id-3">

        <section class="construction">
                <div class="container">
                        <div class="row">
                                <div class="col-md-6">
                                        <h2> 
                                        <!-- For Every Room  -->
                                        <?php the_field('impressive_title_01_bottom'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>


                                        </h2>
                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/04.svg">

                                        <p>
                                        <!-- Thanks to Audacity's dimensional stability and
                                                <br/> water-resistance, the floor is suitable for every room of
                                                <br/> your home, including bathrooms and laundry rooms -->
                                                <?php the_field('impressive_content_01_bottom'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                </p>
                                </div>

                                <div class="col-md-6">
                                        <h2> 
                                        <?php the_field('impressive_title_02_bottom'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>
 
                                        </h2>
                                        <div class="row">
                                                <div class="col-md-6">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/19.svg">

                                                </div>
                                                <div class="col-md-6">
                                                        <img src="<?php bloginfo('template_directory'); ?>/assets/icons/05.svg">

                                                </div>
                                        </div>
                                        <p>
                                        <?php the_field('impressive_content_02_bottom'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                </p>
                                </div>
                        </div>
                </div>
        </section>
</div>

<div id="parallax-id-4">

        <section class="video-slide">
                <div class="container">
                        <div class="row">
                                <div class="col-md-6">

                                        <div class="video-slider">
                                                <div>
                                                        <img id="fullscreen" class="trigger cigarette" src="<?php bloginfo('template_directory'); ?>/assets/img/cigarette.jpg">
                                                        <!-- <div class="outer-video">
                                                                <video loop id="cigarette" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/cigarette.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                                <div>
                                                        <img id="fullscreenscratch" class="trigger scratch" src="<?php bloginfo('template_directory'); ?>/assets/img/scratch.jpg">
                                                        <!-- <div class="outer-video">

                                                                <video loop id="screen" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/scratch.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                                <div>
                                                        <img id="fullscreenstain" class="trigger stain" src="<?php bloginfo('template_directory'); ?>/assets/img/stain.jpg">

                                                        <!-- <div class="outer-video">

                                                                <video loop id="screen" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/stain.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                        </div>

                                </div>

                                <div class="col-md-6 align">
                                <div class="row ">

                                        <h2>
                                        <?php the_field('impressive_video_heading'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                        </h2>

                                                <p> 
                                                <?php the_field('impressive_video_content'); ?>                                                                                                                                                        <?php the_field('impressive_intro_heading'); ?>

                                                        </p>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>
</div>



<?php get_footer(); ?>