<?php

/*
Plugin Name: WP_List_Table Class Example
Plugin URI: http://sitepoint.com
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Agbonghama Collins
Author URI:  http://w3guy.com
*/
class FooterGroupNew_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'New Footer Group',
			'New Footer Group',
			'manage_options',
			'footer_group_form',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		$this->process_post_submit();
        $footer_detail = $this->get_footerdetail();
        wp_enqueue_media();
		?>
		 <style>
		 	.label-info {
			    background-color: #5bc0de;
			}
		 </style>
         <script type="text/javascript">
         jQuery(document).ready( function($) {

                jQuery('input#myprefix_media_manager').click(function(e) {

                    e.preventDefault();
                    var image_frame;
                    if(image_frame){
                        image_frame.open();
                    }
                    // Define image_frame as wp.media object
                    image_frame = wp.media({
                        title: 'Select Media',
                        multiple : false,
                        library : {
                            type : 'image',
                        }
                    });

                    image_frame.on('select', function() {
                        var attachment = image_frame.state().get('selection').first().toJSON();
                        jQuery('#footer_image').val(attachment.url);
                        jQuery("#footer_image_img").attr("src",attachment.url);
                    });
                    image_frame.open();
                });

            });
         </script>
		<div class="wrap">
			<?php if(isset($_GET["action"])){
				if($_GET["action"]=="edit"){
					echo '<h2>Edit Footer Group</h2>';
				}else{
					echo '<h2>Edit Footer Group</h2>';
				}
			}else{
				echo '<h2>New Footer Group</h2>';
			}
			?>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-1">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<input type="hidden" name="footer_group_id" value="<?php echo $footer_detail["ID"];?>" />
								<input type="hidden" name="issubmit" value="1" />
								<div class="form-field term-description-wrap">
									<label for="footer_text_1">Footer Text 1</label><br />
                                    <?php wp_editor( $footer_detail["footer_text_1"], "footer_text_1" ); ?>									
								</div>
                                <div class="form-field term-description-wrap">
									<label for="footer_text_2">Footer Text 2</label><br />
									<?php wp_editor( $footer_detail["footer_text_2"], "footer_text_2" ); ?>
								</div> 
                                <div class="form-field term-description-wrap">
									<label for="footer_image_img">Footer Image</label><br />
                                    <?php                                
                                    $image = '<img id="footer_image_img" style="width:200px;height:auto" src="'.$footer_detail["footer_image"].'" />';                                
                                    ?>
                                    <?php echo $image; ?>
                                    <input type="hidden" name="footer_image" id="footer_image" value="<?php echo $footer_detail["footer_image"]; ?>" class="regular-text" />
                                    <input type='button' class="button-primary" value="<?php esc_attr_e( 'Select a image', 'mytextdomain' ); ?>" id="myprefix_media_manager"/>                              
                                </div>
                                <div class="form-field term-description-wrap">
									<label for="footer_image_link">Footer Image Link</label><br />
                                    <input name="footer_image_link" id="footer_image_link" type="text" value="<?php echo $footer_detail["footer_image_link"];?>" size="40" aria-required="true">
                                </div>
                                <?php $postcode_group = apply_filters( 'taxonomy_get_postcode_group','taxonomy_get_postcode_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Postcode Group</label>  <br />                                  			
                                    <select name="taxonomy_postcode_group[]" multiple style="width:100%">
                                        <?php 
                                        $postcode = unserialize($footer_detail["postcode_group"]);
                                        if(count($postcode_group)>0){
                                            foreach ( $postcode_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $postcode as $selecteditem ) {
                                                    if($selecteditem==$item->ID){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item->ID.'" '.$isSelected.'>'.$item->postcode_group_name.'</option>';
                                            }
                                        }
                                        ?>
                                    </select>                                                                            
                                </div>    
	                            <?php $country_group =  apply_filters( 'taxonomy_get_country_group','taxonomy_get_country_group' ); ?>
                                <div class="form-field term-description-wrap">
                                    <label for="taxonomy_post_parent">Country</label>
                                    <select name="taxonomy_country_group[]" multiple style="width:100%">
                                    <?php
                                        $country = unserialize($footer_detail["country_group"]);
                                        if(count($country_group)>0){
                                            foreach ( $country_group as $item ) {
                                                $isSelected = "";
                                                foreach ( $country as $selecteditem ) {
                                                    if($selecteditem==$item["code"]){
                                                        $isSelected = "selected";
                                                    }
                                                }
                                                echo '<option value="'.$item["code"].'" '.$isSelected.'>'.$item["name"].'</option>';						
                                            }
                                        }
                                    ?>
                                    </select>                                      
                                </div>
								<p class="submit">
									<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Footer Group">
								</p>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>		
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Footer Group List',
			'default' => 5,
			'option'  => 'ads_per_page'
		];

		add_screen_option( $option, $args );

		$this->customers_obj = new Postcode_Group_List();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	public function get_footerdetail() {
		$footer_detail = array();
		$footer_detail["ID"]=0;
		$footer_detail["footer_text_1"]="";
        $footer_detail["footer_text_2"]="";
        $footer_detail["footer_image"]="";
        $footer_detail["footer_image_link"]="";
        $footer_detail["postcode_group"]="";
        $footer_detail["country_group"]="";

		if(isset($_GET["action"])){
			if($_GET["action"]=="edit"){
				global $wpdb;
				$query = "select * from wp_footergroup where ID =".$_GET["ID"];
				$result = $wpdb->get_results($query);
				$lcount = 0;
				if(count($result)>0){
					foreach ( $result as $item ) {
						$footer_detail["ID"] = $item->ID;
                        $footer_detail["footer_text_1"] = $item->footer_text_1;
                        $footer_detail["footer_text_2"] = $item->footer_text_2;
                        $footer_detail["footer_image"] = $item->footer_image;
                        $footer_detail["footer_image_link"] = $item->footer_image_link;
                        $footer_detail["postcode_group"] = $item->postcode_group;
                        $footer_detail["country_group"] = $item->country_group;
						break;
					}
				}
				
			}
		}
		return $footer_detail;
	}
	public function process_post_submit() {
		if(isset($_POST["issubmit"])){
			$footer_text_1 = $_POST["footer_text_1"];
            $footer_text_2 = $_POST["footer_text_2"];
            $footer_image = $_POST["footer_image"];
            $footer_image_link= $_POST["footer_image_link"];
            $postcode_group = serialize($_POST["taxonomy_postcode_group"]);
            $country_group = serialize($_POST["taxonomy_country_group"]);
			$footer_group_id = $_POST["footer_group_id"];
			global $wpdb;
			$query = "";
			if($footer_group_id == 0){
				$query = "INSERT INTO wp_footergroup(footer_text_1,footer_text_2,footer_image,footer_image_link,postcode_group,country_group,is_active) VALUES('".$footer_text_1."','".$footer_text_2."','".$footer_image_link."','".$footer_image."','".$postcode_group."','".$country_group."',1)";				
			}else{
				$query = "update wp_footergroup set footer_text_1='".$footer_text_1."', footer_text_2='".$footer_text_2."', footer_image='".$footer_image."', footer_image_link = '".$footer_image_link."', postcode_group='".$postcode_group."', country_group='".$country_group."' where ID =".$footer_group_id;
			}
			$wpdb->get_results($query);
			echo '<script type="text/javascript">';
			echo 'window.location = "?page=footer_group_listing";';
			echo '</script>';
			exit;
		}
	}

}
