<?php

/*
Plugin Name: WP_List_Table Class Example
Plugin URI: http://sitepoint.com
Description: Demo on how WP_List_Table Class works
Version: 1.0
Author: Agbonghama Collins
Author URI:  http://w3guy.com
*/
class PostCodeGroupNew_Plugin {

	// class instance
	static $instance;

	// customer WP_List_Table object
	public $customers_obj;

	// class constructor
	public function __construct() {
		add_filter( 'set-screen-option', [ __CLASS__, 'set_screen' ], 10, 3 );
		add_action( 'admin_menu', [ $this, 'plugin_menu' ] );
	}


	public static function set_screen( $status, $option, $value ) {
		return $value;
	}

	public function plugin_menu() {

		$hook = add_menu_page(
			'New Postcode Group',
			'New Postcode Group',
			'manage_options',
			'postcode_group_form',
			[ $this, 'plugin_settings_page' ]
		);

		add_action( "load-$hook", [ $this, 'screen_option' ] );

	}


	/**
	 * Plugin settings page
	 */
	public function plugin_settings_page() {
		$this->process_post_submit();
		$postcode_detail = $this->get_postcode();
		?>
		 <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/postcodegroup/bootstrap-tagsinput.css">
		 <style>
		 	.label-info {
			    background-color: #5bc0de;
			}
		 </style>
		<div class="wrap">
			<?php if(isset($_GET["action"])){
				if($_GET["action"]=="edit"){
					echo '<h2>Edit Postcode Group</h2>';
				}else{
					echo '<h2>Edit Postcode Group</h2>';
				}
			}else{
				echo '<h2>New Postcode Group</h2>';
			}
			?>
			<div id="poststuff">
				<div id="post-body" class="metabox-holder columns-1">
					<div id="post-body-content">
						<div class="meta-box-sortables ui-sortable">
							<form method="post">
								<input type="hidden" name="postcode_group_id" value="<?php echo $postcode_detail["ID"];?>" />
								<input type="hidden" name="issubmit" value="1" />
								<div class="form-field form-required term-name-wrap">
									<label for="postcode_group_name">Name</label><br />
									<input name="postcode_group_name" id="postcode_group_name" type="text" value="<?php echo $postcode_detail["postcode_group_name"];?>" size="40" aria-required="true">
									<p>The name is how it appears on category selection.</p>
								</div>
								<div class="form-field term-description-wrap">
									<label for="postcode_group_list">Postcodes</label><br />
									
<textarea rows="10" cols="1" name="postcode_group_list" id="postcode_group_list"><?php echo $postcode_detail["postcode_group_list"];?></textarea>									
									<p>Comma seperated list of postcode in .</p>
								</div>
								<p class="submit">
									<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Postcode Group">
								</p>
							</form>
						</div>
					</div>
				</div>
				<br class="clear">
			</div>
		</div>
		<script src="<?php echo get_template_directory_uri();?>/postcodegroup/bootstrap-tagsinput.min.js"></script>
	<?php
	}

	/**
	 * Screen options
	 */
	public function screen_option() {

		$option = 'per_page';
		$args   = [
			'label'   => 'Postcode Group List',
			'default' => 5,
			'option'  => 'ads_per_page'
		];

		add_screen_option( $option, $args );

		$this->customers_obj = new Postcode_Group_List();
	}


	/** Singleton instance */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	public function get_postcode() {
		$postcode_detail = array();
		$postcode_detail["ID"]=0;
		$postcode_detail["postcode_group_name"]="";
		$postcode_detail["postcode_group_list"]="";
		if(isset($_GET["action"])){
			if($_GET["action"]=="edit"){
				global $wpdb;
				$query = "select * from wp_postcodegroup where ID =".$_GET["ID"];
				$result = $wpdb->get_results($query);
				$lcount = 0;
				if(count($result)>0){
					foreach ( $result as $item ) {
						$postcode_detail["ID"] = $item->ID;
						$postcode_detail["postcode_group_name"] = $item->postcode_group_name;
						$postcode_detail["postcode_group_list"] = $item->postcode_list;
						break;
					}
				}
				
			}
		}
		return $postcode_detail;
	}
	public function process_post_submit() {
		if(isset($_POST["issubmit"])){
			$postcode_group_name = $_POST["postcode_group_name"];
			$postcode_group_list = $_POST["postcode_group_list"];
			$postcode_group_id = $_POST["postcode_group_id"];
			global $wpdb;
			$query = "";
			if($postcode_group_id == 0){
				$query = "INSERT INTO wp_postcodegroup(postcode_group_name,postcode_list,is_active) VALUES('".$postcode_group_name."','".$postcode_group_list."',1)";				
			}else{
				$query = "update wp_postcodegroup set postcode_group_name='".$postcode_group_name."', postcode_list='".$postcode_group_list."' where ID =".$postcode_group_id;
			}
			$wpdb->get_results($query);
			echo '<script type="text/javascript">';
			echo 'window.location = "?page=postcode_group_listing";';
			echo '</script>';
			exit;
		}
	}

}
