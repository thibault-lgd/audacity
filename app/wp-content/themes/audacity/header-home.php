<?php /** * The header for our theme * * This is the template that displays all
of the <head> section and everything up until <div id="content"> * * @link
https://developer.wordpress.org/themes/basics/template-files/#template-partials
* * @package legranddigital */ ?>


<style>

@media only screen and (min-device-width : 375px) and (max-device-width : 667px) {

.mobiletohide{
    display: none;
}

.multi_dropdown {
    float: none;
    margin-right: 20px;
    top: 5px;
    position: relative;
    padding: 10px 0px;
    display: block;
    text-align: center;
}

#country_selector_modal.fade{
    max-width: 100%;
}

.dms-container{
    padding-bottom: 20px;
}

}

</style>

            <div class="container-fluid mobiletohide" style="background:#43bbbe;padding:5px 0px">
                <div class="row">
                    <div class="container">
                    <div class="col-md-12 right">
                            <div class="outer_dropdown topbar-dropdown">
                        <span class="multi_dropdown" style="color: white">Select your country</span>
                        <?php echo do_shortcode('[dms]'); ?>
                    </div>
            </div>
            </div>
</div>
            </div>

<header id="masthead" class="site-header" role="banner">
    <div class="container">
        <div class="row">
            <div class="col-xl-2 col-lg-3 col-md-3">
                <div class="logo">
                    <a href="<?php echo get_home_url(); ?>">
                        <img
                            width="200px"
                            class="desktop"
                            src="<?php bloginfo('template_directory'); ?>/assets/logo.png"
                            alt=" logo"/>
                    </a>
                    <!-- <a href="<?php //echo get_home_url(); ?>">
									<img width="200px" class="desktop" src="<?php// bloginfo('template_directory'); ?>/assets/img/armstrong-logo.jpg" alt=" logo"
									/>
								</a> -->
                </div>
            </div>

            <div class="col-xl-8 col-lg-8 col-md-7 tohide">
                <h1 class="desktop-title" data-mobile-font-size="delta">
                    The Fearless Water Resistant Laminate Flooring
                </h1>

            </div>
            <div class="social">
                <div class="row">
                    <a
                        target="_blank"
                        href="https://www.facebook.com/Audacity-Flooring-2018669681713086/">
                        <i class="fa  fa-facebook-square"></i>
                    </a>

                    <a target="_blank" href="https://www.linkedin.com/company/audacity-flooring/">
                        <i class="fa fa-linkedin-square"></i>

                        <a
                            target="_blank"
                            href="https://www.youtube.com/channel/UC-7SIn8xQflUAkrdZTzbtOQ">
                            <i class="fa fa-youtube-square"></i>
                        </a>

                        <i class="fa fa-bars"></i>

                    </div>
                    <div class="row">

                        <a target="_blank" href="https://www.pinterest.co.uk/audacityflooring/pins/">
                            <i class="fa  fa-pinterest-square"></i>
                        </a>

                        <a target="_blank" href="https://www.instagram.com/audacityflooring/">
                            <i class="fa fa-instagram"></i>
                        </a>

                        <a
                            target="_blank"
                            href="https://www.houzz.co.uk/pro/sherry-devera/audacity-flooring?irs=US">
                            <i class="fa  fa-houzz"></i>
                        </a>
                    </div>
                </div>
            </div>

        </div>
        <nav id="site-navigation" class="primary-navigation">
            <div class="container-menu-social">
                <a
                    target="_blank"
                    href="https://www.facebook.com/Audacity-Flooring-2018669681713086/">
                    <i class="fa  fa-facebook-square"></i>
                </a>

                <a target="_blank" href="https://www.linkedin.com/company/audacity-flooring/">
                    <i class="fa fa-linkedin-square"></i>

                    <a
                        target="_blank"
                        href="https://www.youtube.com/channel/UC-7SIn8xQflUAkrdZTzbtOQ">
                        <i class="fa fa-youtube-square"></i>
                    </a>
                    <a target="_blank" href="https://www.pinterest.co.uk/audacityflooring/pins/">
                        <i class="fa  fa-pinterest-square"></i>
                    </a>

                    <a target="_blank" href="https://www.instagram.com/audacityflooring/">
                        <i class="fa fa-instagram"></i>
                    </a>

                    <a
                        target="_blank"
                        href="https://www.houzz.com/pro/audacityflooring/__public">
                        <i class="fa  fa-houzz"></i>
                    </a>
                </div>
                <i style="color: white" class="fa fa-times"></i>

                <?php wp_nav_menu( array( 'theme_location' => 'menu-1', 'menu_id' =>
                'primary-menu', ) ); ?>
            </nav>
        </header>