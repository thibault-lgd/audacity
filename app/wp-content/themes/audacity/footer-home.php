<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package legranddigital
 */

?>
<footer>
<a href="#" class="scrolltop">
<i class="fa fa-angle-up"></i>
</a>
<div class="container-fluid">

<div class="row">
<div class="col-lg-6"> <p>
<?php the_field('footer-left','option'); ?>
</p>
    </div>
    <div class="col-lg-2 empty">
</div>
    <div class="col-lg-4">

       <a target="_blank" href="<?php the_field('link_img','option'); ?>
">
        <img src="<?php the_field('footer_img','option'); ?>">
</a>

<?php if( get_field('image_footer','option') ): ?>

    <img src="<?php the_field('image_footer'); ?>" />

<?php endif; ?>


<p>
        <?php the_field('footer_right','option'); ?>
        </p>
        
        </div>
</div>
</div>
</footer>

<a href="javascript:void(0)" class="cta-mobile"> 

<!-- <img src="https://www.firmfitfloor.com/wp-content/uploads/2018/05/cta-crop-2.gif" style="
    width: 75%;"> -->
<!--     <span class="text_cta">
Any questions ? <br/>

Contact us !<br/>
</span> -->
<i class="fa fa-envelope"></i>

  </a>

  <style>
    a.cta-mobile{
    display:none;
  }

      .contact-popup{
        display:none;
      }

@media all and (max-width:768px) {


  a.cta-mobile i.fa.fa-envelope {
    position: relative;
    background: #43bbbe;
    border-radius: 0px !important;
    padding: 10px 20px !important;
    float: left;
    width: 20%;
    display: inline-block;
}

a.cta-mobile p {
    margin: 20px 0px;
    width: 80%;
    float: right;
    position: relative;
}

  a.cta-mobile{
        display: block !important;
    background: #43bbbe !important;
    box-shadow: none !important;
    bottom: 0px !important;
    top: initial !important;
    z-index: 999 !important;
    position: fixed !important;
    width: 100%;
    padding: 0px !important;
    border-radius: 0px !important;
  }

        .contact-popup{
            overflow-x: hidden;
            overflow-y: scroll;
    height: 100% !important;
        top:0px !important;
    position: fixed;
    top: 0px;
    background: #43bbbe;
    z-index: 9999999;
        padding: 0px 0px 50px 0px !important;
}

.popup-inner{
        position: relative !important;
        display: block;
}


    #cookie-notice{
        display: none !important;
    }
  }

  </style>
