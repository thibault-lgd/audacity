<?php /* Template Name: Installation */

get_header();

?>
<script>
jQuery(function ($) {

  $(document).ready(function () {

var fullscreen = document.getElementById('fullscreen');
var fullscreenscratch = document.getElementById('fullscreenscratch');
var fullscreenstain = document.getElementById('fullscreenstain');

// $("video").each(function () {

	// fullscreen.addEventListener('click',function(){

fullscreen.addEventListener('click',function(){
	// console.log ('it is working'); 
	if(cigarette.requestFullscreen){
		cigarette.requestFullscreen();
	} 
	else if (cigarette.webkitRequestFullscreen){
		cigarette.webkitRequestFullscreen();
	}
	else if (cigarette.mozRequestFullScreen){
		cigarette.mozRequestFullScreen();
	}
	else if (cigarette.msRequestFullscreen){
		cigarette.msRequestFullscreen();
	}   

});

if (document.addEventListener)
{
document.addEventListener('webkitfullscreenchange', exitHandler, false);
document.addEventListener('mozfullscreenchange', exitHandler, false);
document.addEventListener('fullscreenchange', exitHandler, false);
document.addEventListener('MSFullscreenChange', exitHandler, false);
}

function exitHandler()
{
if (document.webkitIsFullScreen === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
else if (document.mozFullScreen === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
else if (document.msFullscreenElement === false)
{
console.log('test')
$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
}
}     


// $('.slick-active').parent().click(function () {
//     // $(this).find("video")[0].play()
//     $(this).find(".outer-video").show()
//     $('.lightbox-background').fadeIn('slow')
// });

$('img.trigger.cigarette').click(function () {
	 $(".outer-video.cigarette").fadeIn('slow')
	// $('.lightbox-background').fadeIn('slow')
	// $('body').css('position', 'fixed')
	// $('body').css('bottom', '0px')
	$("video#cigarette")[0].play()
	 $("video#cigarette").css('display', 'block')
});


$('img.trigger.scratch').click(function () {
	$(".outer-video.scratch").fadeIn('slow')
	// $('.lightbox-background').fadeIn('slow')
	// $('body').css('position', 'fixed')
	// $('body').css('bottom', '0px')
	$("video#scratch")[0].play()
	$("video#scratch").css('display', 'block')
});

$('.lightbox-background').click(function () {
	this.pause();
	this.currentTime = 0;
	$(this).fadeOut('slow');
	$('body').css('position', 'initial')
	$('body').css('bottom', 'initial')
	$(".outer-video").fadeOut('slow')
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 1000);
});

$('.fa-window-close').click(function () {
	// this.pause();
	// this.currentTime = 0;
	// $(this).fadeOut('slow');
	$("video").each(function () {
		this.pause();this.currentTime = 0;
	})
	$('.lightbox-background').fadeOut('slow')
	$('video').fadeOut('slow')
	$('body').css('position', 'initial')
	$('body').css('bottom', 'initial')
	$(".outer-video").fadeOut('slow')
	$("html, body").animate({
		scrollTop: $(document).height()
	}, 1000);
});

});
});
</script>

<section class="title">


	<div class="row blue">

		<h1>

			<?php echo the_title(); ?>

		</h1>

	</div>

</section>

<section class="intro">

	<div class="row">
		<table>
			<tr>
				<td>
					<!-- <div class="col-md-6"> -->
					<img src="<?php bloginfo('template_directory'); ?>/assets/img/easy-to-install.jpg">

					<!-- </div> -->
				</td>

				<td id="parallax-id-1">
					<!-- <div class="col-md-6"> -->
					<div class="inner">
						<table>
							<tr>
								<td>
									<h2 class="black">

										<?php the_field('installation-intro-title'); ?>
									</h2>
									<p>
										<?php the_field('installation_intro_content'); ?>
									</p>
									<span>
										<?php 

$file = get_field('installation_manual', 'option');

if( $file ): ?>
										<strong>
											<a target="_blank" href="<?php echo $file['url']; ?>">See Installation manual</a>
										</strong>

										<?php endif; ?>

									</span>
								</td>
								<td class="icon" align="middle">
									<img src="<?php bloginfo('template_directory'); ?>/assets/icons/09.svg">

								</td>
							</tr>
						</table>
					</div>
					<!-- </div> -->
				</td>
			</tr>
		</table>
</section>




<section class="video-slide" style="padding:0px">
                <div class="container-fluid">
                        <div class="row">
                                <div class="col-md-12">

                                        <div class="video-slider">
                                                <div>
                                                        <img style="width:100%" id="fullscreen" class="trigger cigarette" src="../wp-content/themes/audacity/assets/images/cover-installation.jpg">
                                                        <!-- <div class="outer-video">
                                                                <video loop id="cigarette" width="100%" height="auto">

                                                                        <source src="<?php bloginfo('template_directory'); ?>/assets/videos/cigarette.mp4" type="video/mp4">

                                                                </video>
                                                        </div> -->
                                                </div>
                                    

                                </div>

                             
                        </div>
                </div>
        </section>
        
<div id="parallax-id-2-5">

	<section class="full purple">
		<div class="row">
			<div class="col-md-offset-3 col-md-3"></div>
			<div class="col-md-6">
				<h2>
					<?php the_field('installation_break_heading'); ?>
				</h2>
				<p>
					<?php the_field('installation_break_content'); ?>
				</p>
				<br/>
				<br/>
				<img src="<?php bloginfo('template_directory'); ?>/assets/icons/01.svg">
				<br/>
				<br/>
				<?php 

$file = get_field('maintenance', 'option');

if( $file ): ?>

				<a target="_blank" href="<?php echo $file['url']; ?>">See maintenance guide</a>

				<?php endif; ?>


			</div>
			<div class="col-md-offset-3 col-md-3"></div>
		</div>
	</section>
</div>


<section class="video-loop" style="position:fixed!important;">


	<div class="container-fluid">



<div class="outer-video cigarette">
<!-- <i class="fa fa-window-close"></i>
 -->
        <video style="display: none" loop id="cigarette" width="100%" height="auto" controls>

<source src="https://thibaultrolando.com/audacity/wp-content/uploads/2018/08/Aduacity-install1-final-Aug.14th2018-1.mp4" type="video/mp4" />

        </video>

      
</div>

		
	</div>
	</section>
<div id="parallax-id-3">

	<section class="construction">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<table>
						<tr>
							<td>
								<h2>
									<?php the_field('installation_bottom_heading'); ?>
								</h2>
								<p>
									<?php the_field('installation_bottom_content'); ?>

								</p>
								<br/>
								<br/>
								<a href="#warranty-modal" rel="modal:open">
									See warranties</a>
							</td>
							<td valign="middle">
							<div class="row icons">
							<div class="col-md-6">
								<img src="<?php bloginfo('template_directory'); ?>/assets/icons/fs.jpg">
								</div>
								<div class="col-md-6">
								<img src="<?php bloginfo('template_directory'); ?>/assets/icons/pvc-free-audacity.png">
								</div>
<!-- 								<div class="col-md-3">
								<img src="<?php bloginfo('template_directory'); ?>/assets/icons/14.svg">
								</div>
								<div class="col-md-3">
								<img src="<?php bloginfo('template_directory'); ?>/assets/icons/15.svg">
								</div> -->
							</div>
							</td>
						</tr>
					</table>
				</div>

			</div>
		</div>
	</section>

</div>
<div id="warranty-modal" class="modal">

	<div class="row blue">

		<h2>
			CHOOSE THE TYPE OF WARRANTY
		</h2>
	</div>
	<?php 

$file = get_field('residential_warranty', 'option');

if( $file ): ?>
	<a class="residential" target="_blank" href="<?php echo $file['url']; ?>">Residential Warranty
		<i class="fa fa-chevron-down"></i>
	</a>

					<?php endif; ?>

	<?php 

$file = get_field('commercial_warranty', 'option');

if( $file ): ?>
	<a class="commerical" target="_blank" href="<?php echo $file['url']; ?>">Commercial Warranty
		<i class="fa fa-chevron-down"></i>
	</a>
	<?php endif; ?>

</div>
<?php //echo do_shortcode('[get_link_section]') ?>
<div id="new_contact" style="padding:60px 0px" class="row">

<style>

video#cigarette, .outer-video.cigarette{
/*	display: none !important;
*/}
form#gform_2 {
    text-align: center;
}
.gform_wrapper .top_label input.medium, .gform_wrapper .top_label select.medium{
        width: 100% !important;
}
.gform_wrapper .top_label .gfield_label{
	display: block !important; 
}

li#field_2_6 label{
	display: none !important
}

.ginput_container_radio, label.gfield_label{
	text-align: left;
	margin-top:30px;
}
</style>
<div class="col-lg-3 col-md-12 col-sm-12 second">
&nbsp;
</div>
<div id="new_form" class="col-lg-6 col-md-12 col-sm-12 second" style="margin: 0 auto;padding:0px 0px 0px 0px">
<!-- <h2 style="text-align:center;color:black">FIND AN AUDACITY RETAILER<br/>
	 <?php if( get_field('find_a_store','option') ): ?>

   <a style="
       background: #33257c;
    color: white;
    padding: 10px 20px;
    margin-top:15px;
    margin-bottom:15px;
    display: block;
    width: fit-content;
    margin-left: auto;
    margin-right: auto" class="button_cta"href="<?php echo get_home_url(); ?>/store-locator"><?php the_field('find_a_store','option');?></a>

<?php endif; ?>
OR ASK A QUESTION.</h2> -->

   <h2 style="text-align:center;color:black;clear:both">
 <?php the_field('contact_section','option');?>
</h2>
            <?php echo do_shortcode('[gravityform id=2]') ?>
        </div>
        <div class="col-lg-3 col-md-12 col-sm-12 second">
		&nbsp;
</div>
        </div>


<?php get_footer(); ?>