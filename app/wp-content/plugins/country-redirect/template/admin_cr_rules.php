<?php

use Inc\Base\BaseController;

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class RedirectRule_List extends WP_List_Table
{

	/** Class constructor */
	public function __construct() {

		parent::__construct( [
			'singular' => __( 'CR Rule', 'sp' ), //singular name of the listed records
			'plural'   => __( 'CR Rules', 'sp' ), //plural name of the listed records
			'ajax'     => false //does this table support ajax?
		] );

	}


	/**
	 * Retrieve rules data from the database
	 *
	 * @param int $per_page
	 * @param int $page_number
	 *
	 * @return mixed
	 */
	public static function get_rules( $per_page = 5, $page_number = 1 ) {

		global $wpdb;
		
		$sql = "SELECT * FROM wp_cr_rules ";
		$do_search = "";	
		if ( ! empty( $_REQUEST['orderby'] ) ) {
			$do_search .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$do_search .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';
		}else{
			$do_search .= ' ORDER BY site_id desc ';
		}
		
		$sql .= $do_search;
		$sql .= " LIMIT $per_page";
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

        $result = $wpdb->get_results( $sql, 'ARRAY_A' );

		return $result;
	}


	/**
	 * Delete a customer record.
	 *
	 * @param int $id customer ID
	 */
	public static function delete_rule( $id ,$status ) {
		global $wpdb;
		if($status == "inactive"){
			$query = "update wp_cr_rules set is_active=0 where ID =".$id;
			$wpdb->get_results($query);
		}else if($status == "active"){
			$query = "update wp_cr_rules set is_active=1 where ID =".$id;
			$wpdb->get_results($query);
		}else if($status == "delete"){
			$query = "delete from wp_cr_rules where ID =".$id;
			$wpdb->get_results($query);
		}
	}


	/**
	 * Returns the count of records in the database.
	 *
	 * @return null|string
	 */
	public static function record_count() {
		global $wpdb;
		$countQuery = "SELECT count(*) FROM wp_cr_rules";
		return $wpdb->get_var( $countQuery );
	}


	/** Text displayed when no customer data is available */
	public function no_items() {
		_e( 'No rules avaliable.', 'sp' );
	}


	/**
	 * Render a column when no column specific method exist.
	 *
	 * @param array $item
	 * @param string $column_name
	 *
	 * @return mixed
	 */
    public function column_default( $item, $column_name ) {
        
		switch ( $column_name ) {
			case 'ID':
				return $this->column_name($item);
			case 'country_name':
				return $item[ $column_name ];
			case 'site_name':
				return $item[ $column_name ];
			case 'is_display':
				return $item[ $column_name ];
			case 'is_active':
				return $item[ $column_name ];
			default:
				return print_r( $item, true ); //Show the whole array for troubleshooting purposes
		}
	}

	/**
	 * Render the bulk edit checkbox
	 *
	 * @param array $item
	 *
	 * @return string
	 */
	function column_cb( $item ) {
		return sprintf(
			'<input type="checkbox" name="bulk-delete[]" value="%s" />', $item['ID']
		);
	}


	/**
	 * Method for name column
	 *
	 * @param array $item an array of DB data
	 *
	 * @return string
	 */
	function column_name( $item ) {

		$delete_nonce = wp_create_nonce( 'sp_delete_ads' );
		$trash_nonce = wp_create_nonce( 'sp_pending_ads' );

		$title = '<strong>' . $item['ID'] . '</strong>';

		
		$actions=array(
			'edit'=>sprintf('<a href="?page=cr_rules_form&action=%s&ID=%s">Edit</a>', 'edit', $item["ID"])			
        );
		if($item["is_active"] == 1){
			$actions['pending']= sprintf('<a href="?page=cr_rules&action=%s&ID=%s">In Active</a>', 'inactive', $item["ID"]);
		}else{
			$actions['pending']= sprintf('<a href="?page=cr_rules&action=%s&ID=%s">Active</a>', 'active', $item["ID"]);
		}
		$actions['delete']= sprintf('<a href="?page=cr_rules&action=%s&ID=%s">Delete</a>', 'delete', $item["ID"]);
		return $title . $this->row_actions( $actions );
	}


	/**
	 *  Associative array of columns
	 *
	 * @return array
	 */
	public function get_columns() {
		$columns = [
			'cb'      => '<input type="checkbox" />',
			'ID'    => __( 'ID', 'sp' ),
			'country_name'    => __( 'Country Name', 'sp' ),
			'site_name'    => __( 'Site Name', 'sp' ),
			'is_display'    => __( 'Is Display', 'sp' ),
			'is_active'    => __( 'Is Active', 'sp' )
		];
		return $columns;
	}


	/**
	 * Columns to make sortable.
	 *
	 * @return array
	 */
	public function get_sortable_columns() {
		$sortable_columns = array(
			'ID' => array( 'ID', true ),
			'country_name' => array( 'country_name', true ),
			'site_name' => array( 'site_name', true )
		);
		return $sortable_columns;
	}

	/**
	 * Returns an associative array containing the bulk action
	 *
	 * @return array
	 */
	public function get_bulk_actions() {
		$actions = [
			'bulk-delete' => 'Delete',
			'bulk-inactive' => 'In Active',
			'bulk-active' => 'Active'
		];

		return $actions;
	}


	/**
	 * Handles data query and filter, sorting, and pagination.
	 */
	public function prepare_items() {
		
		$columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
		/** Process bulk action */
		$this->process_bulk_action();

		# >>>> Pagination
		$per_page     = $this->get_items_per_page( 'ads_per_page', 20 );
		$current_page = $this->get_pagenum();
        $total_items  = self::record_count();
        $this->set_pagination_args( array (
				'total_items' => $total_items,
				'per_page'    => $per_page,
				'total_pages' => ceil( $total_items / $per_page )
		) );
		$last_post = $current_page * $per_page;
		$first_post = $last_post - $per_page + 1;
		$last_post > $total_items AND $last_post = $total_items;
		
		// Setup the range of keys/indizes that contain
		// the posts on the currently displayed page(d).
		// Flip keys with values as the range outputs the range in the values.
		$range = array_flip( range( $first_post - 1, $last_post - 1, 1 ) );
		
		// Filter out the posts we're not displaying on the current page.
		//$posts_array = array_intersect_key( $posts, $range );
		# <<<< Pagination
		
		$this->items = self::get_rules( $per_page, $current_page );
	}

	public function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if ( 'delete' === $this->current_action() ) {

			
				self::delete_rule( absint( $_GET['ID']),"delete" );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}else if ( 'inactive' === $this->current_action() ) {

			
				self::delete_rule( absint($_GET['ID']),$this->current_action() );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}else if ( 'active' === $this->current_action() ) {

			
				self::delete_rule( absint($_GET['ID']),$this->current_action() );

		                // esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		                // add_query_arg() return the current url
		                //wp_redirect( esc_url_raw(add_query_arg()) );
                /*echo '<script type="text/javascript">';
                echo 'window.location.reload()';
                echo '</script>';
				exit;*/

		}

		// If the delete bulk action is triggered
		if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-delete' )
		     || ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-delete' )
		) {

			$delete_ids = esc_sql( $_POST['bulk-delete'] );

			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_rule( $id ,"delete");

			}

			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
		        // add_query_arg() return the current url
		        //wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}else if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-inactive' )
					|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-inactive' )
					) {
			$delete_ids = esc_sql( $_POST['bulk-delete'] );
			
			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_rule( $id ,"inactive");
			
			}
			
			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
			// add_query_arg() return the current url
			//wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}else if ( ( isset( $_POST['action'] ) && $_POST['action'] == 'bulk-active' )
					|| ( isset( $_POST['action2'] ) && $_POST['action2'] == 'bulk-active' )
					) {
			$delete_ids = esc_sql( $_POST['bulk-delete'] );
			
			// loop over the array of record IDs and delete them
			foreach ( $delete_ids as $id ) {
				self::delete_rule( $id ,"active");
			
			}
			
			// esc_url_raw() is used to prevent converting ampersand in url to "#038;"
			// add_query_arg() return the current url
			//wp_redirect( esc_url_raw(add_query_arg()) );
			echo '<script type="text/javascript">';
			echo 'window.location.reload()';
			echo '</script>';
			exit;
		}
	}
}

$option = 'per_page';
$args = [
    'label' => ' List',
    'default' => 5,
    'option' => 'ads_per_page'
];

add_screen_option($option, $args);
$rulesList = new RedirectRule_List();
?>
<div class="wrap">
    <h2><?php echo $this->plugin_name . " Listing"; ?></h2>

    <div id="poststuff">
        <div id="post-body" class="metabox-holder columns-1">
            <div id="post-body-content">
                <div class="meta-box-sortables ui-sortable">
                    <form method="post">
                        <?php
                        $rulesList->prepare_items();
                        $rulesList->display(); ?>
                    </form>
                </div>
            </div>
        </div>
        <br class="clear">
    </div>
</div>
        