<?php

//use Inc\Base\BaseController;
if (file_exists(PLUGIN_PATH . '/inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. '/inc/Base/BaseController.php';
}
class CRRulesForm_Plugin
{

	// class instance
     static $instance;

	// customer WP_List_Table object
     public $customers_obj;

     public $basecontroller_obj;

	// class constructor
     public function __construct($baseObj)
     {
          add_filter('set-screen-option', [__class__, 'set_screen'], 10, 3);
          $this->basecontroller_obj = $baseObj;
          $this->plugin_settings_page();
     }


     public static function set_screen($status, $option, $value)
     {
          return $value;
     }


     /**
      * Plugin settings page
      */
     public function plugin_settings_page()
     {

          $this->process_post_submit();
          $cr_rules = $this->get_cr_rules();
          wp_enqueue_media();
          ?>
		 <style>
		 	.label-info {
			    background-color: #5bc0de;
			}
		 </style>
         <script type="text/javascript">
          jQuery(document).ready( function($) {
               jQuery('#country_code').on('change', function() {
                    jQuery("#country_name").val(jQuery("#country_code option:selected").text());
                })
          });
         </script>
		<div class="wrap">
			<?php if (isset($_GET["action"])) {
          if ($_GET["action"] == "edit") {
               echo '<h2>Edit Rules </h2>';
          } else {
               echo '<h2>Edit Rules</h2>';
          }
          } else {
               echo '<h2>New Rules</h2>';
          }
          ?>
               <div id="poststuff">
                    <div id="post-body" class="metabox-holder columns-1">
                         <div id="post-body-content">
                              <div class="meta-box-sortables ui-sortable">
                                   <form method="post">
                                        <input type="hidden" name="cr_rules_id" value="<?php echo $cr_rules["ID"]; ?>" />
                                        <input type="hidden" id="country_name" name="country_name" value="<?php echo $cr_rules["country_name"]; ?>" />
                                        <input type="hidden" name="issubmit" value="1" />  
                                        <div class="form-field term-description-wrap" id="divCountryList">
                                             <label for="country_code">Country</label><br />
                                             <?php
                                             $selecteditem = $cr_rules["country_code"];
                                             $countryList = $this->basecontroller_obj->getAllCountry();
                                             ?>
                                             <select id="country_code" name="country_code" style="width:100%">
                                             <?php
                                             if (count($countryList) > 0) {
                                                  echo '<option value="-1">Select</option>';
                                                  foreach ($countryList as $item) {
                                                       $isSelected = "";
                                                       if ($selecteditem == $item["code"]) {
                                                            $isSelected = "selected";
                                                       }
                                                       echo '<option value="' . $item["code"] . '" ' . $isSelected . '>' . $item["name"] . '</option>';
                                                  }
                                             }
                                             ?>
                                             </select>
                                        </div>
                                        <div class="form-field term-description-wrap" id="divSiteList">
                                             <label for="site_id">Redirect Site Name</label><br />
                                             <?php
                                             $selecteditem = $cr_rules["site_id"];
                                             $subsites = get_sites();

                                             // global $wpdb;

                                             // $table_name = 'wp_cr_rules';

                                             // foreach ( $wpdb->get_col( "DESC " . $table_name, 0 ) as $column_name ) {
                                             //   error_log( $column_name );
                                             //   echo '<div>'.$column_name.'</div>';
                                             // }

                                             ?>
                                             <select name="site_id" style="width:100%">
                                             <?php
                                             if(count($subsites)>0){
                                             echo '<option value="-1">Select</option>';
                                             foreach ( $subsites as $item ) {
                                                  $isSelected = "";
                                                  $subsite_id = get_object_vars($item)["blog_id"];
                                                  $subsite_name = get_blog_details($subsite_id)->blogname;
                                                  if($selecteditem==$subsite_id){
                                                       $isSelected = "selected";
                                                  }
                                                  echo '<option value="'.$subsite_id.'" '.$isSelected.'>'.$subsite_name.'</option>';
                                             }
                                             }
                                             ?>
                                             </select>
                                        </div>
                                        <div class="form-field term-description-wrap">
                                             <label for="postcode_list">Postcodes</label><br />
                                             <textarea rows="10" cols="1" name="postcode_list" id="postcode_list"><?php echo $cr_rules["postcode_list"]; ?></textarea>
                                             <p>Comma seperated list of postcode in .</p>
                                        </div>
                                        <div class="form-field term-description-wrap">
                                             <label for="is_display">Should display in dropdown?</label><br />
                                             <input type="hidden" name="is_display" value="0" />
                                             <input type="checkbox" name="is_display" id="is_display" value="1" data-ischecked="<?php echo ($cr_rules['is_display']==1 ? 'checked' : 'not checked');?>" <?php echo ($cr_rules['is_display']==1 ? 'checked' : '');?> />
                                        </div>
                                        
                                        <p class="submit">
                                             <input type="submit" name="submit" id="submit" class="button button-primary" value="Save">
                                        </p>
                                   </form>
                              </div>
                         </div>
                    </div>
                    <br class="clear">
               </div>
          </div>		
	<?php
     }

     /**
      * Screen options
      */
     public function screen_option()
     {

          $option = 'per_page';
          $args = [
               'label' => 'Rules List',
               'default' => 5,
               'option' => 'ads_per_page'
          ];

          add_screen_option($option, $args);


     }


     /** Singleton instance */
     public static function get_instance()
     {
          if (!isset(self::$instance)) {
               self::$instance = new self();
          }

          return self::$instance;
     }
     public function get_cr_rules()
     {
          $cr_rules = array();
          $cr_rules["ID"] = 0;
          $cr_rules["country_name"] = "";
          $cr_rules["country_code"] = "";
          $cr_rules["site_id"] = "";
          $cr_rules["site_name"] = "";
          $cr_rules["postcode_list"] = "";
          $cr_rules["is_display"] = "";
          
          if (isset($_GET["action"])) {
               if ($_GET["action"] == "edit") {
                    global $wpdb;
                    $query = "select * from wp_cr_rules where ID =" . $_GET["ID"];
                    $result = $wpdb->get_results($query);
                    $lcount = 0;
                    if (count($result) > 0) {
                         foreach ($result as $item) {
                              $cr_rules["ID"] = $item->ID;
                              $cr_rules["country_name"] = $item->country_name;
                              $cr_rules["country_code"] = $item->country_code;
                              $cr_rules["site_id"] = $item->site_id;
                              $cr_rules["site_name"] = $item->site_name;
                              $cr_rules["postcode_list"] = $item->postcode_list;
                              $cr_rules["is_display"] = $item->is_display;
                              
                              break;
                         }
                    }

               }
          }
          return $cr_rules;
     }
     public function process_post_submit()
     {
          if (isset($_POST["issubmit"])) {
               $cr_rules_id = $_POST["cr_rules_id"];
               $country_code = $_POST["country_code"];
               $country_name = $_POST["country_name"];

               $site_id = $_POST["site_id"];
               $site_name = get_blog_details($site_id)->blogname;
               $postcode_list = $_POST["postcode_list"];
               $is_display = $_POST["is_display"] == "1" ? 1 : 0;
               
               global $wpdb;
               $query = "";
               if ($cr_rules_id == 0) {
                    $query = "INSERT INTO wp_cr_rules(country_name, country_code, site_id, site_name, postcode_list, is_display, is_active) VALUES('" . $country_name . "','" . $country_code . "'," . $site_id . ",'" . $site_name . "','" . $postcode_list . "','" . $is_display . "',1)";
               } else {
                    $query = "update wp_cr_rules set country_name='" . $country_name . "', country_code='" . $country_code . "', site_id=" . $site_id . ", site_name = '" . $site_name . "', postcode_list = '" . $postcode_list ."', is_display = '" . $is_display . "' where ID =" . $cr_rules_id;
               }
               $wpdb->get_results($query);
               echo '<script type="text/javascript">';
               echo 'window.location = "?page=cr_rules";';
               echo '</script>';
               exit;
          }
     }

}

$obj = new CRRulesForm_Plugin(new BaseController);
