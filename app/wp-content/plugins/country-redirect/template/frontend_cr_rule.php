
<div id="mulitSiteSelector" class="jquery-modal blocker current" >
     <div id="cr_country_selector_modal" class="cr_country_selector_modal cr-country-selector-modal fade cr_country_selector_modal_modal modal" tabindex="-1" role="dialog" style="display: inline-block;">
            <div class="cr-country-selector-modal-dialog modal-normal" role="document">
                <div class="cr-country-selector-modal-content">                       
                    <div class="cr-country-selector-modal-body" style="color: #57aeb0; background-color: ;">
                         <div class="cr_country_selector_modal_text">
                          <input type="hidden" id="CountryNameFromAPI_Pop" value="<?php echo $this->CountryNameFromAPI; ?>" />
                          <input type="hidden" id="UserRegionFromAPI_Pop" value="<?php echo $this->CountryName; ?>" />
                              <p>Seems like you are in 
                                <?php
                                if (strpos($this->CountryNameFromAPI, 'United States') !== false || strpos($this->CountryNameFromAPI, 'United Kingdom') !== false) {
                                  echo "The " . $this->CountryNameFromAPI; 
                                } else {
                                  echo $this->CountryNameFromAPI;
                                }
                                 ?>!
                              </p>
                              <p class="country_selector_seems_text">India</p>
                              <p class="country_selector_international_text">If not, please select your area.</p>
                         </div>
                         <div class="cr_country_selector_modal_buttons">
                              <div id="country-dropdown-wrap">
                                   <select id="country-dropdown">
                                        <option selected="selected" disabled="">Select Your Area</option>
                                        <?php
                                        global $wpdb;
                                        global $wp;
                                        $currentCountry = $this->CountryName;
                                        $selecteditem = get_current_blog_id();
                                        // $subsites = get_sites();
                                        // if (count($subsites) > 0) {
                                        //      foreach ($subsites as $item) {
                                        //           $isSelected = "";
                                        //           $subsite_id = get_object_vars($item)["blog_id"];
                                        //           $redirect = get_site_url($subsite_id) . "/" . $this->currentPage;
                                        //           $subsite_name = get_blog_details($subsite_id)->blogname;
                                        //           if ($selecteditem == $subsite_id) {
                                        //                $isSelected = "selected";
                                        //           }
                                        //           echo '<option value="'.$redirect.'" ' . $isSelected . '>' . $subsite_name . '</option>';
                                        //      }
                                        // }

                                        $displayquery = "select * from wp_cr_rules where is_display = 1 and is_active=1";
                         
                                        $result = $wpdb->get_results($displayquery);
                                         if (count($result) > 0) {
                                              foreach ($result as $item) {
                                                  $isSelected = "";
                                                  $subsite_id = $item->site_id;
                                                  $redirect = get_site_url($subsite_id) . "/" . $this->currentPage;
                                                  $subsite_name = get_blog_details($subsite_id)->blogname;
                                                  if ($selecteditem == $subsite_id) {
                                                       $isSelected = "selected";
                                                       $currentCountry = $subsite_name;
                                                  }
                                                  if (strpos($subsite_name, 'United States') !== false || strpos($subsite_name, 'United Kingdom') !== false) {
                                                      $subsite_name = "The " . $subsite_name; 
                                                    } else {
                                                      $subsite_name = $subsite_name;
                                                    }
                                                  echo '<option value="'.$redirect.'" ' . $isSelected . '>' . $subsite_name . '</option>';
                                              }
                                        }

                                        ?>
                                   </select>
                              </div>
                              <div class="stay-wrap">
                                   <div class="stay">
                                        <a href="javascript:void(0)" onclick="closeDialog()" rel="modal:close">Stay on 
                                          <?php 
                                          if (strpos($currentCountry, 'United States') !== false || strpos($currentCountry, 'United Kingdom') !== false) {
                                                      echo "The " . $currentCountry; 
                                                    } else {
                                                      echo $currentCountry;
                                                    }
                                          ?> page
                                        </a>
                                   </div>
                              </div>
                              <div class="reason">This helps us deliver a better service, showing you which collections are available.</div>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                    <button type="button" class="cr_country_selector_modal_close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
            </div>
        <a href="javascript:void(0)" onclick="closeDialog()" rel="modal:close" class="close-modal ">Close</a>
     </div>
</div>
