<?php

/**
 * @package CountryRedirect
 */
   /*
   Plugin Name: Country Redirect
   Plugin URI: http://example.com
   description: Plugin that can set the redirect rule for multisite when coming from different location
   Version: 1.0.0
   Author: Mr. Thibault
   Author URI: http://thibaultrolando.com
   License: GPL2
   Text Domain:country-redirect
 */

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

Copyright 2005-2015 Automattic, Inc.
 */

if (!function_exists('add_action')) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
    require_once dirname(__FILE__) . '/vendor/autoload.php';
}

define('PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url(__FILE__));
define('PLUGIN', plugin_basename(__FILE__));

if (file_exists(dirname(__FILE__) . '/vendor/autoload.php')) {
     require_once dirname(__FILE__) . '/vendor/autoload.php';
 }

 if (file_exists(dirname(__FILE__) . '/inc/Base/CRPluginManager.php')) {
     require_once dirname(__FILE__) . '/inc/Base/CRPluginManager.php';
 }

function crplugin_activate(){
    CRPluginManager::activate();
}
register_activation_hook(__FILE__, array( 'CRPluginManager', 'activate' ));

function crplugin_deactivate(){
    CRPluginManager::deactivate();
}
//register_deactivation_hook(__FILE__, 'crplugin_deactivate');
register_activation_hook(__FILE__, array( 'CRPluginManager', 'deactivate' ));

if (file_exists(PLUGIN_PATH . '/inc/init.php')) {
     require_once PLUGIN_PATH . '/inc/init.php';

 }

if (class_exists('Init')) {
    Init::register_service();
}

/*

use Inc\CRPluginManager;
use Inc\Admin\AdminPages;

if (!class_exists('CRPlugin')) {
    class CRPlugin
    {
        public $plugin;
        function __construct()
        {
            $this->plugin = plugin_basename(__FILE__);
            add_action('init', array($this, 'custom_post_type'));
        }
        function register()
        {
            

            add_filter("plugin_action_links_$this->plugin", array($this, 'settings_link'));
            //Front-end
            add_action('wp_enqueue_scripts', array($this, 'enqueue'));

        }
        function settings_link($link)
        {
            $setting_link = '<a href="admin.php?page=country_redirect">Settings</a>';
            array_push($link, $setting_link);
            return $link;
        }
        
        
        function activate()
        {
            //$this->custom_post_type();
            //flush_rewrite_rules();
        }
        function deactivate()
        {
            //flush_rewrite_rules();
        }
        function custom_post_type()
        {
            register_post_type('book', ['public' => 'true', 'label' => 'Book']);
        }
        
        function enqueue()
        {
            wp_enqueue_style('cppluginstyle', plugins_url('/assets/style.css', __FILE__));
            wp_enqueue_style('cppluginstyle', plugins_url('/assets/myscript.js', __FILE__));
        }
        //methods
    }

    $crPlugin = new CRPlugin();
    $crPlugin->register();
}
register_activation_hook(__FILE__, array('CRPluginManager', 'activate'));
register_deactivation_hook(__FILE__, array('CRPluginManager', 'deactivate'));*/