<?php 
/**
 * Trigger this file on plugin uninstall
 * @package CountryRedirect
 */
if ( !define( 'WP_UNISTALL_PLUGIN' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}
//Clear Database stored data
$books = get_posts(array('post_type'=>'book', 'numberposts'=> -1));
foreach($books as $book){
    wp_delete_post($book->ID,false);
}
//global $wpdb;
//$wpdb->query()