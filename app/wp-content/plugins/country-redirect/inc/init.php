<?php

/**
 * @package CountryRedirect
 */
//namespace Inc;

// echo "test init";
if (file_exists(PLUGIN_PATH . 'inc/Pages/Admin.php')) {
     require_once PLUGIN_PATH. 'inc/Pages/Admin.php';
}

if (file_exists(PLUGIN_PATH . 'inc/Pages/FrontEnd.php')) {
     require_once PLUGIN_PATH. 'inc/Pages/FrontEnd.php';
}
if (file_exists(PLUGIN_PATH . 'inc/Base/SettingsLink.php')) {
     require_once PLUGIN_PATH. 'inc/Base/SettingsLink.php';
}

final class Init
{
    private static function instantiate($class)
    {
        return new $class;
    }
    public static function get_services()
    {
        return array(
            Admin::class,
            FrontEnd::class,
            SettingsLink::class
        );
    }
    public static function register_service()
    {
        foreach (self::get_services() as $class) {
            $service = self::instantiate($class);
            if (method_exists($service, 'register')) {
                $service->register();
            }
        }
    }
}