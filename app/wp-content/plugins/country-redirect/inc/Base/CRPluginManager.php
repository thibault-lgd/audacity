<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\Base;

class CRPluginManager
{
    public static function activate()
    {
     global $wpdb;
          $sql ="CREATE TABLE IF NOT EXISTS `wp_cr_rules` (
               `ID` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
               `country_name` varchar(255) NOT NULL DEFAULT '',
               `country_code` varchar(20) NOT NULL DEFAULT '',
               `site_id` bigint(20) NOT NULL,
               `site_name` varchar(255) NOT NULL DEFAULT '',
               `postcode_list` longtext NOT NULL,
               `is_display` tinyint(1) NOT NULL DEFAULT '0',
               `is_active` int(11) NOT NULL,
               PRIMARY KEY (`ID`)
          ) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8";
        $wpdb->get_results($sql);
        

    }
    public static function deactivate()
    {
        

    }
}