<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\Base;
//use \Inc\Base\BaseController;
if (file_exists(PLUGIN_PATH . 'inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. 'inc/Base/BaseController.php';
}

class SettingsLink extends BaseController
{

    public function register()
    {
        add_filter("plugin_action_links_$this->plugin", array($this, 'settings_link'));

    }
    function settings_link($link)
    {
        $setting_link = '<a href="admin.php?page=cr_rules">Settings</a>';
        array_push($link, $setting_link);
        return $link;
    }
}