<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\API\Callbacks;
//use Inc\Base\BaseController;
if (file_exists(PLUGIN_PATH . 'inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. 'inc/Base/BaseController.php';
}

class FrontEndCallbacks extends BaseController
{
    public function frontEndDashboard(){
        return require_once("$this->plugin_path/template/front_end_cr_rules.php");
    }
}