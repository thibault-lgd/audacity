<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\API\Callbacks;
//use Inc\Base\BaseController;
if (file_exists(PLUGIN_PATH . 'inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. 'inc/Base/BaseController.php';
}

class AdminCallbacks extends BaseController
{
    public function adminDashboard(){
        return require_once("$this->plugin_path/template/admin_cr_rules.php");
    }
    public function rulesForm(){
        return require_once("$this->plugin_path/template/admin_cr_rules_form.php");
    }
}