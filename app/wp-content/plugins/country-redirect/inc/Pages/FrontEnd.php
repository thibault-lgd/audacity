<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\Pages;

/*use \Inc\Base\BaseController;
use \Inc\API\SettingApi;
use \Inc\API\Callbacks\FrontEndCallbacks;*/
if (file_exists(PLUGIN_PATH . 'inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. 'inc/Base/BaseController.php';
}
if (file_exists(PLUGIN_PATH . 'inc/API/SettingApi.php')) {
     require_once PLUGIN_PATH. 'inc/API/SettingApi.php';
}
if (file_exists(PLUGIN_PATH . 'inc/API/Callbacks/FrontEndCallbacks.php')) {
     require_once PLUGIN_PATH. 'inc/API/Callbacks/FrontEndCallbacks.php';
}


class FrontEnd extends BaseController
{
     public $settings;
     public $callbacks;
     public $currentPage;
     public $redirectSiteId;
     public $CountryName;

     public $pages = array();
     public $subpages = array();
     public $siteList = array();
     public $pPath;
     public function register()
     {
          $this->settings = new SettingApi();
          $this->callbacks = new FrontEndCallbacks();
          add_action('init', array($this, 'register_my_session'));
          add_action('wp_head', array($this, 'front_end_enqueue'), 10);
          add_action('wp', array($this, 'wpse69369_special_thingy'),20);
     }
     function register_my_session()
     {
          if (!session_id()) {
               session_start();
          }
     }
     function wpse69369_special_thingy()
     {
          // echo "loaded";

          // if(!isset($_SESSION['IsRedirected'])){
          if(empty($_SESSION['UserCountry'])){
               $user_ip = getenv('REMOTE_ADDR');
                // echo $user_ip;
               $loc = file_get_contents('https://ipapi.co/'.$user_ip.'/json/?key=e673a0c6785d8f8dbb1348c6ee6cc78f4dbdfcfd');
               // echo $loc;
               $obj = json_decode($loc);
               if(!empty($obj->postal) && !empty($obj->country)){
                    if (!session_id()) {
                         session_start();
                    }
                    $_SESSION['UserPostcode']=$obj->postal;
                    $_SESSION['UserCountry']=$obj->country;
                    $_SESSION['CountryNameFromAPI']=$obj->country_name;
               }
          }

          if(!empty($_SESSION['UserCountry'])){
          	$this->CountryNameFromAPI = $_SESSION['CountryNameFromAPI'];          	
          }
          // echo $_COOKIE['is_skip_redirect'];
          // echo $_COOKIE['temp_skip_redirect'];
          

               if (isset($_SESSION['IsRedirected']) && !empty($_SESSION['UserCountry'])) {
                    
                    if($_SESSION["IsRedirected"]=="1"){
                         unset($_SESSION['IsRedirected']);
                         if (!isset($_COOKIE['is_skip_redirect'])) {
                              $this->CountryName = $subsite_name = get_blog_details($_SESSION["CountryName"])->blogname;
                              add_action('wp_head', array($this, 'front_end_check_site_popup'), 10);
                              $this->pPath = $this->plugin_url;

                              return include("$this->plugin_path/template/frontend_cr_rule.php");
                         }
                    }
               }
         
               if (isset($_SESSION['UserCountry']) && !empty($_SESSION['UserCountry'])) {
                    global $wpdb;
                    global $wp;
                    $isOther = false;
                    $isPostCode = false;
                    $toRedirect = false;
                    $isOnSameSite = false;
                    if (isset($_SESSION['UserPostcode']) && !empty($_SESSION['UserPostcode'])) {
                         
                         $query = "select * from wp_cr_rules where country_code = '" . $_SESSION['UserCountry'] . "' and postcode_list like '%".$_SESSION["UserPostcode"]."%' and is_active=1";
                         $result = $wpdb->get_results($query);
                         if (count($result) > 0) {
                              foreach ($result as $item) {
                                   $postcode_list = explode(",", $item->postcode_list);
                                   foreach($postcode_list as $postcode) {
                                        $postcode = trim($postcode);
                                        if (strpos($postcode, $_SESSION['UserPostcode']) !== false) {
                                             if ($item->site_id != get_current_blog_id()) {
                                                  $this->currentPage = $wp->request;
                                                  $this->redirectSiteId = $item->site_id;
                                                  $this->CountryName = $item->country_name;                                                  
                                                  // echo $this->UserRegionFromAPI;
                                                  // echo "1";
                                                  $toRedirect = true; 
                                                  $isPostCode = true; 
                                                  break;
                                             }else{
                                             	// echo "1";
                                                  $isOnSameSite = true;
                                             }
                                        }
                                   }
                              }
                              
                         }
                         // echo $_SESSION['UserCountry'];
                         // if(!$toRedirect && !$isOnSameSite){
                         //      $query = "select * from wp_cr_rules where country_code = '" . $_SESSION['UserCountry'] . "' and postcode_list='' and is_active=1";
                              
                         //      $result = $wpdb->get_results($query);
                         //      if (count($result) > 0) {
                         //           foreach ($result as $item) {
                         //                if ($item->site_id != get_current_blog_id()) {
                         //                     $this->currentPage = $wp->request;
                         //                     $this->redirectSiteId = $item->site_id;
                         //                     $this->CountryName = $item->country_name;
                         //                     $toRedirect = true; 
                         //                     break;
                         //                }else{
                         //                     $isOnSameSite = true;
                         //                }
                                        
                         //           }
                                   
                         //      }
                         // }

                         // if(!$toRedirect && !$isOnSameSite){
                         //      $query = "select * from wp_cr_rules where country_code = '" . $_SESSION['UserCountry'] . "' and postcode_list NOT like '%".$_SESSION["UserPostcode"]."%' and is_active=1";
                              
                         //      $result = $wpdb->get_results($query);
                         //      if (count($result) > 0) {
                         //           foreach ($result as $item) {
                         //                if ($item->site_id != get_current_blog_id()) {
                         //                     $this->currentPage = $wp->request;
                         //                     $this->redirectSiteId = $item->site_id;
                         //                     $this->CountryName = $item->country_name;
                         //                     $toRedirect = true; 
                         //                     break;
                         //                }else{
                         //                     $isOnSameSite = true;
                         //                }
                                        
                         //           }
                                   
                         //      }
                         // }
                         
                         
                    }
                    if(!$toRedirect && !$isOnSameSite){
                         $query = "select * from wp_cr_rules where country_code = '" . $_SESSION['UserCountry'] . "' and postcode_list='' and is_active=1";
                         
                         $result = $wpdb->get_results($query);
                         if (count($result) > 0) {
                              foreach ($result as $item) {
                                   if ($item->site_id != get_current_blog_id()) {
                                        $this->currentPage = $wp->request;
                                        $this->redirectSiteId = $item->site_id;
                                        $this->CountryName = $item->country_name;
                                        // echo "2";
                                        $toRedirect = true; 
                                        break;
                                   }else{
                                   	// echo "2";
                                        $isOnSameSite = true;
                                   }
                              }                         
                         }
                    }

                    	// echo $toRedirect ? "true": "false";
                    	// echo $isOnSameSite ? "true": "false";
                    if(!$toRedirect && !$isOnSameSite){
                         $query = "select * from wp_cr_rules where country_code = 'other' and is_active=1";
                         
                         $result = $wpdb->get_results($query);
                         if (count($result) > 0) {
                              foreach ($result as $item) {
                                   if ($item->site_id != get_current_blog_id()) {
                                        $this->currentPage = $wp->request;
                                        $this->redirectSiteId = $item->site_id;
                                        $this->CountryName = $item->country_name;
                                        // echo "3";
                                        $toRedirect = true; 
                                        break;
                                   }else{
                                        $isOnSameSite = true;
                                   }                              
                              }                         
                         }
                    }

                    if (isset($_COOKIE['temp_skip_redirect']) && $_COOKIE['temp_skip_redirect']=="1") {
                        $toRedirect = false;
                    } else if(!isset($_COOKIE['is_skip_redirect']) && !$toRedirect && $isOnSameSite && $_SESSION['UserCountry'] == 'US'){
                    	$this->CountryName = $subsite_name = get_blog_details($_SESSION["CountryName"])->blogname;
                              add_action('wp_head', array($this, 'front_end_check_site_popup'), 10);
                              $this->pPath = $this->plugin_url;

                              return include("$this->plugin_path/template/frontend_cr_rule.php");
                    }

                    if($toRedirect & !$isOnSameSite){
                         $_SESSION["IsRedirected"] = "1";
                         $_SESSION["CountryName"] = $this->redirectSiteId;
                         // add_action('wp_head', array($this, 'front_end_check_site'), 10);
                         // echo $this->currentPage;
                         if(!$this->currentPage && $isPostCode){
                              wp_redirect( home_url() ); exit;
                         } else {
                              wp_redirect( get_site_url($this->redirectSiteId) . "/" . $this->currentPage); exit;
                         }
                    }else{
                         $_SESSION["IsRedirected"] = "0";
                    }
               }
          // }

     }
     function front_end_enqueue()
     {
     	           
          // wp_enqueue_script('cppluginstyle-jquery', '//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js');
          wp_enqueue_script('cppluginstyle-myscript', $this->plugin_url . 'assets/myscript.js',array(),"1.0.2");
          $script_params = array(
               'setlocation' => $this->plugin_url . 'assets/setLocation.php',
               'getlocation' => $this->plugin_url . 'assets/getLocation.php',
               'GMAPKEY' => 'AIzaSyAXY7K32Xa6fS_-lppvQqIj2Li7ElxqR5w'
          );
          wp_localize_script('cppluginstyle-myscript', 'scriptParams', $script_params);
     }
     function front_end_check_site()
     {
          wp_enqueue_script('cppluginstyle-checksitescript', $this->plugin_url . 'assets/checksitescript.js');
          $redirect = get_site_url($this->redirectSiteId) . "/" . $this->currentPage;
          global $wp;
          $script_params = array(
               'redirect_site_url' => $redirect,
               'CountryName' => $this->CountryName
          );
          wp_localize_script('cppluginstyle-checksitescript', 'scriptFRParams', $script_params);
     }
     function front_end_check_site_popup()
     {
          wp_enqueue_style('cppluginstyle-style-popup', $this->plugin_url . 'assets/style.css',array(), '1.0.1');
          wp_enqueue_style('cppluginstyle-style-modal', $this->plugin_url . 'assets/jquery.modal.min.css');           
          wp_enqueue_script('cppluginstyle-popupsitescript-1', $this->plugin_url . 'assets/popupsitescript.js',array(),"1.0.1");
          $redirect = get_site_url($this->redirectSiteId) . "/" . $this->currentPage;
          global $wp;
          $script_params = array(
               'redirect_site_url' => $redirect,
               'CountryName' => $this->CountryName
          );
          wp_localize_script('cppluginstyle-popupsitescript-1', 'scriptPopupParams', $script_params);
     }
     public function setPages()
     {
          $this->pages = array(
               array(
                    'page_title' => 'Country Redirect',
                    'menu_title' => 'Country Redirect',
                    'capability' => 'manage_option',
                    'menu_slug' => 'cr_rules',
                    'callback' => array($this->callbacks, 'adminDashboard'),
                    'icon_url' => 'dashicons-admin-multisite',
                    'position' => 110,
               )
          );
     }
     public function setSubPages()
     {
          $this->subpages = array(
               array(
                    'parent_slug' => 'cr_rules',
                    'page_title' => 'Add New Rule',
                    'menu_title' => 'Add New Rule',
                    'capability' => 'manage_option',
                    'menu_slug' => 'cr_rules_form',
                    'callback' => array($this->callbacks, 'rulesForm'),

               )
          );
     }
}