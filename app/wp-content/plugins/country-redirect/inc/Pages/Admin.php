<?php

/**
 * @package CountryRedirect
 */
//namespace Inc\Pages;

/*use \Inc\Base\BaseController;
use \Inc\API\SettingApi;
use \Inc\API\Callbacks\AdminCallbacks;*/

if (file_exists(PLUGIN_PATH . 'inc/Base/BaseController.php')) {
     require_once PLUGIN_PATH. 'inc/Base/BaseController.php';
}
if (file_exists(PLUGIN_PATH . 'inc/API/SettingApi.php')) {
     require_once PLUGIN_PATH. 'inc/API/SettingApi.php';
}
if (file_exists(PLUGIN_PATH . 'inc/API/Callbacks/AdminCallbacks.php')) {
     require_once PLUGIN_PATH. 'inc/API/Callbacks/AdminCallbacks.php';
}

class Admin extends BaseController
{
    public $settings;
    public $callbacks;

    public $pages = array();
    public $subpages = array();
    public function register()
    {
        $this->settings = new SettingApi();
        $this->callbacks = new AdminCallbacks();
        $this->setPages();
        $this->setSubPages();
        $this->settings->addPages($this->pages)->withSubPage('Dashboard')->addSubPages($this->subpages)->register();
        //add_action('admin_enqueue_scripts', array($this, 'admin_enqueue'));

    }

    function admin_enqueue()
    {
        wp_enqueue_style('cppluginstyle', $this->plugin_url . 'assets/style.css');
        wp_enqueue_script('cppluginstyle', $this->plugin_url . 'assets/myscript.js');
    }
    public function setPages()
    {
        $this->pages = array(
            array(
                'page_title' => 'Country Redirect',
                'menu_title' => 'Country Redirect',
                'capability' => 'manage_option',
                'menu_slug' => 'cr_rules',
                'callback' => array($this->callbacks,'adminDashboard'),
                'icon_url' => 'dashicons-admin-multisite',
                'position' => 110,
            )
        );
    }
    public function setSubPages()
    {
        $this->subpages = array(
            array(
                'parent_slug' => 'cr_rules',
                'page_title' => 'Add New Rule',
                'menu_title' => 'Add New Rule',
                'capability' => 'manage_option',
                'menu_slug' => 'cr_rules_form',
                'callback' => array($this->callbacks,'rulesForm'),

            )
        );
    }
}