<?php
if(!empty($_POST['latitude']) && !empty($_POST['longitude'])){
	session_start();
	
		//Send request and receive json data by latitude and longitude
     $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($_POST['latitude']).','.trim($_POST['longitude']).'&sensor=false&key=AIzaSyAXY7K32Xa6fS_-lppvQqIj2Li7ElxqR5w';
     $json = @file_get_contents($url);
     $data = json_decode($json);
     $status = $data->status;
     $_SESSION['UserPostcode']="";
     $_SESSION['UserCountry']="";
     if($status=="OK"){
          for($i=0;$i<count($data->results[0]->address_components);$i++){
               if (in_array("postal_code", $data->results[0]->address_components[$i]->types)){
               $_SESSION['UserPostcode']= $data->results[0]->address_components[$i]->long_name;
                    
               }else if (in_array("country", $data->results[0]->address_components[$i]->types)){
               $_SESSION['UserCountry']= $data->results[0]->address_components[$i]->short_name;
               }
               if($_SESSION['UserPostcode']!="" && $_SESSION['UserCountry']!=""){
                    break;
               }
          }
     }
	$response = array();
	$response["UserPostcode"] = $_SESSION['UserPostcode'];
	$response["UserCountry"] = $_SESSION['UserCountry'];
	echo json_encode($response);
}
?>