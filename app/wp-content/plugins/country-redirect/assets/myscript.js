jQuery(document).ready(function() {
     //findLocation();
     // ipLookUp();
     console.log("Hello....");
     // setCookie("temp_skip_redirect", "0", 1);

     var $ = jQuery;
     var $dmsSelect = $(".dms-select");
     $dmsSelect.change(function() {
          var optionText = $(this)
               .find("option:selected")
               .text();
          Cookies.set("currentCountry", optionText, { expires: 7 });
          setCookie("temp_skip_redirect", "1", 1);
          //            window.location.href = option.val();
          window.location = jQuery("option:selected", this).val();
     });
     $dmsSelect.val($(".site-logo > a").attr("href"));
     if(!$dmsSelect.val()){
          $dmsSelect.val('');
     }
     var theCountryList = ['United States of America', 'United Kingdom'];
     $("option", $dmsSelect).each(function() {
         var optText = this.text;
         if(theCountryList.indexOf(optText) > -1){
          this.text = "The " + this.text;
         }
     });
});
function ipLookUp() {
     jQuery.ajax("https://ipapi.co/json").then(
          function success(response) {
               var UserLatitude = localStorage.getItem("UserLatitude");
               var UserLongitude = localStorage.getItem("UserLongitude");
               if (UserLatitude != response.latitude.toString() && UserLongitude != response.longitude.toString()) {
                    setLocation(response.latitude, response.longitude, response.postal, response.country, true);
               } else {
                    setLocation(response.latitude, response.longitude, response.postal, response.country, false);
               }
          },
          function fail(data, status) {
               console.log("Request failed.  Returned status of", status);
          }
     );
}
function setLocation(latitude, longitude, postcode, country, to_reload) {
     jQuery.ajax({
          type: "POST",
          url: scriptParams.setlocation,
          data: "latitude=" + latitude + "&longitude=" + longitude + "&postcode=" + postcode + "&country=" + country,
          success: function(msg) {
               var response = JSON.parse(msg);
               localStorage.setItem("UserPostcode", response.UserPostcode);
               localStorage.setItem("UserCountry", response.UserCountry);
               localStorage.setItem("UserLatitude", latitude);
               localStorage.setItem("UserLongitude", longitude);
               if (to_reload) {
                    window.location.reload();
               }
          }
     });
}
function getAddress(latitude, longitude) {
     jQuery.ajax({
          type: "POST",
          url: scriptParams.getlocation,
          data: "latitude=" + latitude + "&longitude=" + longitude,
          success: function(msg) {
               var response = JSON.parse(msg);
               localStorage.setItem("UserPostcode", response.UserPostcode);
               localStorage.setItem("UserCountry", response.UserCountry);
               localStorage.setItem("UserLatitude", latitude);
               localStorage.setItem("UserLongitude", longitude);
               window.location.reload();
          }
     });
}
function findLocation() {
     if ("geolocation" in navigator) {
          // check if geolocation is supported/enabled on current browser
          navigator.geolocation.getCurrentPosition(
               function success(position) {
                    // for when getting location is a success
                    var UserLatitude = localStorage.getItem("UserLatitude");
                    var UserLongitude = localStorage.getItem("UserLongitude");
                    if (UserLatitude != position.coords.latitude.toString() && UserLongitude != position.coords.longitude.toString()) {
                         getAddress(position.coords.latitude, position.coords.longitude);
                    } else {
                         var UserCountry = localStorage.getItem("UserCountry");
                         var UserPostcode = localStorage.getItem("UserPostcode");

                         setLocation(position.coords.latitude, position.coords.longitude, UserPostcode, UserCountry, false);
                    }
               },
               function error(error_message) {
                    // for when getting location results in an error
                    console.error("An error has occured while retrieving location", error_message);
                    ipLookUp();
               }
          );
     } else {
          // geolocation is not supported
          // get your location some other way
          console.log("geolocation is not enabled on this browser");
          ipLookUp();
     }
}
function getCountryByLocation(lat, lng) {
     if (localStorage.getItem("UserPostcode") == "null") {
          localStorage.setItem("UserPostcode", "");
     }
     if (localStorage.getItem("UserCountry") == "null" || localStorage.getItem("UserCountry") == null) {
          localStorage.setItem("UserCountry", "");
     }
     var postCode = localStorage.getItem("UserPostcode");
     var country = localStorage.getItem("UserCountry");

     jQuery.ajax({
          type: "POST",
          url: "<?php echo get_template_directory_uri();?>/getLocation.php",
          data: "latitude=" + lat + "&longitude=" + lng + "&postcode=" + postCode + "&country=" + country,
          success: function(msg) {
               var response = JSON.parse(msg);
               var pc = response.UserPostcode;
               var con = response.UserCountry;
               var oldPostCode = localStorage.getItem("UserPostcode");
               localStorage.setItem("UserPostcode", pc);
               if (pc == "null") {
                    pc = "";
               }
               localStorage.setItem("UserPostcode", pc);
               var oldCountry = localStorage.getItem("UserCountry");
               localStorage.setItem("UserCountry", con);
               if (con == "null") {
                    con = "";
               }
               localStorage.setItem("UserCountry", con);
               var IsCheckedOnce = localStorage.getItem("IsCheckedOnce");
               localStorage.setItem("IsCheckedOnce", "1");
               if (!oldPostCode && msg != "") {
                    if (!IsCheckedOnce) {
                         window.location.reload();
                    }
               } else if (!oldCountry && msg != "") {
                    if (!IsCheckedOnce) {
                         window.location.reload();
                    }
               }
          }
     });
}
function setCookie(cname, cvalue, exdays) {
     var d = new Date();
     d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
     var expires = "expires=" + d.toUTCString();
     document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
